class Hangman:

    def __init__(self):

        start_text = "\nPlease pick one of the following options, which determains \nhow many letters the word should include.\n"
        print(start_text)
    
        self.words = ["word1", "word2", "word3"] 
        self.chosen = ""
        self.isGuessRight = False
        self.result = ""

    def startGame(self):
        for idx, word in enumerate(self.words):
            print(str(idx) + ". "  + word)
        
        print("")
        self.chosen = input("What is your choice? ")
        self.result = ["_" for i in range(len(self.words[int(self.chosen)]))]
        print("")

    def gameLoop(self):
        while(not self.isGuessRight):
            print("1. Guess a letter.")
            print("2. Guess a word.")

            x = input()

            if x == "1":
                usersGuessForLetter = input("What would you like to guess? ")

                self.guessLetter(usersGuessForLetter)

            elif x == "2":
                usersGuessForWord = input("What would you like to guess? ")

                self.guessWord(usersGuessForWord)

            else:
                print("Retard. Try again.")

    def guessLetter(self, usersGuessForLetter):
        if usersGuessForLetter in self.words[int(self.chosen)]:
            word = self.words[int(self.chosen)]

            for idx, letter in enumerate(word):
                if usersGuessForLetter == letter:
                    self.result[idx] = usersGuessForLetter
        
            print(self.result)

    def guessWord(self, usersGuess):
        if usersGuessForWord in self.words:
            self.isGuessRight = True
            print("Yeheee!")
        else:
            print("Not quite right")

if __name__ == "__main__":
    playGame = Hangman()
    playGame.startGame()
    playGame.gameLoop()