# -*- coding: utf-8 -*-

print("Du sagde jeg skulle fjolle lidt rundt i python")
print("så jeg har prøvet at smide nogle af de ting du har")
print("gået igennem med mig sammen. Så, jeg synes det kunne være sjovt at")
print("fortælle dig hvad jeg faktisk har gjort uden faktisk at vise dig min")
print("kode :D")
print("Bare læg ud med og give mig 4 tal :)")

var1 = int(raw_input())
var2 = int(raw_input())
var3 = int(raw_input())
var4 = int(raw_input())

elements = [var1, var2, var3, var4]

print(elements)

print("Jeg skal bruge et af de 4 elementer.")

print("Please lad være med at skrive noget andet... Der er gået noget galt, mens jeg arbejdede")
print("med branching, og jeg kan ikke få python til at åbne 'else'... Hjælp mig lige bagefter, please!")

svar = int(raw_input())

if elements.index(svar) == 0:

    print("Her arbejder jeg med 'branching', og du har åbnet den første mulighed - 'if'.")
    print("Siden du valgte det første element, kommer python ikke til")
    print("at arbejde med 4 andre muligheder - 3 'elif'er og et enkelt 'else'.")
    print("Alt efter hvilket element fra arrayet du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde plus.")
    print("Giv mig positive 3 tal.")

    def add(x, y, z):

        a = x + y + z
        return a

    plus1 = int(raw_input())
    plus2 = int(raw_input())
    plus3 = int(raw_input())

    print(add(plus1, plus2, plus3))

    foerste_resultat = plus1 + plus2 + plus3

    print("Jeg har lagt de første 3 tal lagt sammen. Jeg skal have 4 til. Når de er lagt")
    print("sammen, kan vi lægge de to endelige resultater sammen til sidst.")

    def add(x, y, z, p):

        b = x + y + z + p
        return b
    
    plus4 = int(raw_input())
    plus5 = int(raw_input())
    plus6 = int(raw_input())
    plus7 = int(raw_input())

    print(add(plus4, plus5, plus6, plus7))

    andet_resultat = plus4 + plus5 + plus6 + plus7

    print("For at lægge de to værdier sammen, har jeg defineret de første tre")
    print("tal som en variabel, og de 4 næste som en anden variabel. De to variabler, kan")
    print("kan vi nu lægge sammen. Altså bliver det endelige resultat = ")

    print(foerste_resultat + andet_resultat)

    samlet_resultat = foerste_resultat + andet_resultat

    x = samlet_resultat

    for i in range(x):
        print(i)

    print("Jeg har nu loopet over det endelige resultat, såfremt at x = den sidste værdi")
    print("og i = 0 + 1. ")

    print("Da vi her bruger et forloop, og beder Python om at loope i range(x) (hvor")
    print("x er den samlede værdi) vil den samlede værdi ikke være med i vores loop som")
    print("en integer.")
    
elif elements.index(svar) == 1:

    print("Jeg arbejder med 'branching', og du har her 'åbnet' det første")
    print("'elif'.")
    print("Af den grund vil python ikke til at arbejde med den første mulighed (if),")
    print("den sidste mulighed, (else) og de resterende 2 'elif'.")
    print("Alt efter hvilket element du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde minus.")
    print("Giv mig tre positive tal.")

    def sub(x, y, z):

        a = x - y - z
        return a

    minus1 = int(raw_input())
    minus2 = int(raw_input())
    minus3 = int(raw_input())

    print(sub(minus1, minus2, minus3))

    foerste_resultat = minus1 - minus2 - minus3

    print("Jeg har nu trukket de tre tal fra hinanden i den rækkefølge du gav mig dem.") 
    print("Jeg skal have to positive tal til (de bliver lagt sammen) så jeg kan trække de to værdier") 
    print("fra hinanden til sidst.")

    def add(x, y):

        b = x + y
        return b

    minus4 = int(raw_input())
    minus5 = int(raw_input())

    print(add(minus4, minus5))

    andet_resultat = minus4 + minus5

    print("For at trække de to værdier fra hinanden, har jeg defineret de første tre")
    print("tal som en variabel, og de næste 2 som en anden variabel. De to variabler")
    print("kan vi nu trække fra hinanden. Altså bliver det endelige resultat = ")

    print(foerste_resultat - andet_resultat)

    samlet_resultat = foerste_resultat - andet_resultat

    x = samlet_resultat

    for i in range(x, 0):
        print(i)

    print("Jeg har loopet over det endelige resultat, såfremt at x = den sidste værdi")
    print("og i = 0 + 1. ")

    print("Vi bruger her et forloop. Loopet stopper altså, så snart der ikke er flere")
    print("elementer tilbage.")

elif elements.index(svar) == 2:

    print("Jeg arbejder med 'branching', og du har 'åbnet' det andet")
    print("'elif'.")
    print("Af den grund vil python ikke arbejde med den første mulighed (if),")
    print("den sidste mulighed, (else) og de resterende 2 'elif'.")

    print("Alt efter hvilket element du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde division.")
    print("Giv mig to positive tal.")

    def div(x, y):
        
        a = x / y
        return a

    div1 = float(raw_input())
    div2 = float(raw_input())

    print(div(div1, div2))

    foerste_resultat = div1 / div2

    print("Jeg skal have to tal til. Når jeg har fået to resultater, kan vi") 
    print("dividere og disse to med hinanden og få vores endelige resultat.")
    
    def div(x, y):

        b = x / y
        return b

    div3 = float(raw_input())
    div4 = float(raw_input())

    print(div(div3, div4))

    andet_resultat = div3 / div4

    print("Vi dividerer de to tal med hinanden - i den rækkefølge du har givet mig dem - og får,")
    print("at det endelige resultat lyder = ")

    samlet_resultat = foerste_resultat / andet_resultat
    print(samlet_resultat)

    print("Laver vi et par stykker af disse regnestykker, vil vi til sidst kunne")
    print("samle resultaterne i et array for herefter at sortere dem. Vi har et enkelt resultat, nemlig")
    print(samlet_resultat)
    print("så lad os lave 3 til. Giv mig to tal.")

    def div(x, y):

        c = x / y
        return c

    div5 = float(raw_input())
    div6 = float(raw_input())

    print(div(div5, div6))

    regnestykke2 = div5 / div6

    print("To tal til.")

    def div(x, y):

        d = x / y

        return d

    div7 = float(raw_input())
    div8 = float(raw_input())

    print(div(div7, div8))

    regnestykke3 = div7 / div8

    print("To til.")

    def div(x, y):

        e = x / y

        return e

    div9 = float(raw_input())
    div10 = float(raw_input())

    print(div(div9, div10))

    regnestykke4 = div9 / div10

    print("Vi samler alle resultaterne i et array og får = ")

    divArray = [samlet_resultat, regnestykke2, regnestykke3, regnestykke4]
    print(divArray)

    print("Vi sorterer nu elementerne i arrayet og får = ")

    divArray.sort()

    print(divArray)

elif elements.index(svar) == 3:
    
    print("Jeg arbejder med 'branching' og du har 'åbnet' det tredje 'elif'.")
    print("Af den grund vil python ikke arbejde med den første mulighed (if),")
    print("den sidste mulighed, (else) og de resterende 2 'elif'.")
    print("Alt efter hvilket element du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde modulus.")
    print("Giv mig 2 tal til.")

    def modulo(x, y):

        a = x % y
        return a

    modulus1 = int(raw_input())
    modulus2 = int(raw_input())

    print(modulo(modulus1, modulus2))

    foerste_resultat = modulus1 % modulus2

    print("Vi har her taget første tal du gav mig modulus anden tal du gav mig.")
    print("Når vi arbejder med denne metode, finder vi ud af, hvor mange gange det andet")
    print("tal går op i det første. Vi ser bagefter på resten, og dette er vores resultat.")
    print("Jeg skal bruge to tal til.")

    def modulo(x, y):

        b = x % y
        return b
    
    modulus3 = int(raw_input())
    modulus4 = int(raw_input())

    print(modulo(modulus3, modulus4))

    andet_resultat = modulus3 % modulus4

    print("Jeg har defineret de første to tal som en variabel, og de to næste") 
    print("som en anden variabel. Vi bruger nu for tredje gang den samme regnemetode og får, at")
    print("vores endelige resultat lyder = ")

    print(foerste_resultat % andet_resultat)

    samlet_resultat = foerste_resultat % andet_resultat

    x = samlet_resultat

    for i in range(x):
        print(i)

    print("Jeg har loopet over det endelige resultat, såfremt at x = det endelige resultat")
    print("og i = 0 + 1. ")

    print("Da vi her bruger et forloop, og beder Python om at loope i range(x) (hvor")
    print("x er den samlede værdi) vil den samlede værdi ikke være med i vores loop som")
    print("en integer.")
    print("(Er de to resultater modulus hinanden = 0, opstår der ikke et loop.")
    print("Er dette tilfældet, så vil der under det sidste resultat bare stå et '0'.")

else:
    print("Siden du ikke har valgt et element fra arrayet, ved jeg altså ikke")
    print("hvordan jeg kommer videre herfra... Så hardcore er jeg ikke :'(")

    print("(Siden du er flabet, er du endt i 'else' (...))")
