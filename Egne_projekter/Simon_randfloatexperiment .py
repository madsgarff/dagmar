#Project: A program to generate floats with boundaries controlled by function call arguments.

import random
from decimal import Decimal, getcontext

def randfloat(*args):
    '''Generates a random positive float with chosen number of decimals.
    Requires the argument for the maximum of the desired float.
    If called with 2 arguments the first is the minimum.
    If called with 3 arguments the third is the amount of decimals desired.'''
    start = 0
    stop = 0
    dec = 1

    if len(args) == 1:
        stop = args[0]
        
        if stop  < 0:
            raise Exception('The maximum must be greater than the minimum which is set to 0 by default')
        
        elif stop == start:
            return 0.0
            
        else:
            pass
    
    elif len(args) == 2:
        start = args[0]
        stop = args[1]

        if stop < start:
            raise Exception('The maximum must be greater than the minimum')
        

        elif start == stop:
            return float(start)
        
        elif stop - start == 1:
            getcontext().prec = dec + 1 # getcontext decides how many decimals Decimal() operates with
            return float(Decimal(start) + Decimal(random.randint(0, 10 - 1) / 10))
      
        else:
            pass
    
    elif len(args) == 3:
        start = args[0]
        stop = args[1]
        dec = args[2]
        
        if stop < start:
            raise Exception('The maximum must be greater than the minimum')

        elif start == stop:
            return float(start)

        elif stop - start == 1:
            getcontext().prec = dec + 1
            return Decimal(start) + Decimal(random.randint(0, (10 ** dec) - 1) / (10 ** dec))
        
        else:
            pass
            
    else:
        raise Exception('Error in function call: Invalid number of arguments\nAccepted: 1, 2 or 3 arguments -> start, stop, decimals')
    
    getcontext().prec = dec + 1
    baseVal = Decimal(random.randint(start, stop - 1))
    decVal = Decimal(random.randint(0, (10 ** dec) - 1)) / Decimal( 10 ** dec)

    '''Object type test -> '''
    # print('Base: {0}\nDecimal: {1}'.format(baseVal, decVal))
    # print(type(baseVal + decVal))
    
    return float(baseVal) + float(decVal)

print('Call with 1 args:')
print(randfloat(100))
print('Call with 2 args:')
print(randfloat(3, 4))
print('Call with 3 args:')
print(randfloat(0, 10, 6))

''' Crash Test -> '''
# print('Attempt at triggering input error:')
# print(randfloat(0, 3, 5, 7))