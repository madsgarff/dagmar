def fahrenheit_to_celsius(F):

    # Your code goes here: calculate the temperature in Celsius,
    # store in a variable (we called it C), and return it.

    C = 0

    x = (F - 32) * 5/9
    C = x

    return C

print(fahrenheit_to_celsius(77.9))
print(40*"-")
#__________________________________________________________________________

def light_time(distance):

    # Calculate the time taken using t = d/c and return it.

    c = 299792458  # Speed of light [m/s]
    t = 0

    # t = Time, d = distance and c = constant speed.

    t = distance/299792458

    return t

print(light_time(376291900))
print(40*"-")

#__________________________________________________________________________

# Find korrekt output efter udfra følgende informationer;

# a = 2.757
# b = 16.793
# latitude = x

# Eksempel fra opgaven.

# Input latitude: 60.5
# Output body mass: 183.5915

def moose_body_mass(latitude):
    mass = 0

    a = 2.757
    b = 16.793
    
    mass = a * latitude + b

    return mass

print(moose_body_mass(60.5))
print(40*"-")

#__________________________________________________________________________

def compound_interest(amount, rate, years):
    # new_amount = 0

    # Your code goes here!

    # amount = 1000
    # rate = 0.07
    # years = 25

    # new_amount = amount(1 + rate, years)
    new_amount = amount * pow((1 + rate), years)

    return new_amount

print(compound_interest(1000, 0.07, 25))
print(40*"-")
#__________________________________________________________________________

# The wind chill index T_wc quantifies the apparent temperature you feel.
# T_a is the temperature of the air and the v is the wind speed.

# Input: The air temperature T_a in Celcius (°C) and the wind speed v in kilometers per hour.
# Output: The wind chill factor T_wc.

# Input temperature: -25
# Input wind speed: 30
# Output wind chill index: -39.09

# Input = The air temperature T_a in Celcius (°C) and the wind speed v in kilometers per hour.
# Output = The wind chill factor T_wc.

def wind_chill(T_a, v):
    T_wc = 0

    # Your code goes here!

    a = (0.6215 * T_a)
    b = 11.37 * pow(v, 0.16)
    c = 0.3965 * T_a * pow(v, 0.16)
    # d = pow((0.3965 * T_a) * v), 0.16)

    T_wc = 13.12 + a - b + c

    return T_wc

print(wind_chill(-25, 30))
print(40*"-")
#__________________________________________________________________________
