print(40*"-")
# Opgave 10; - Done √

def survive(blood_type, donated_blood): ## Jeg har lavet lidt om på den "start-kode", som jeg fik.

    # Description:
    # Each person has different blood depending on which antibodies and 
    # antigens bind to the surface of their red blood cells. They can be
    # categorized in many different ways, but the two most important 
    # are the ABO group system (with four groups: A, B, AB, O) and the 
    # Rh blood group system (with two groups: +, -). 

    # Hospitals have blood banks that collect blood donations and store 
    # them until they're needed by patients. Every time a patient needs a 
    # blood transfusion, the hospital needs to check that they have some 
    # suitable blood that can be accepted by the patient's body. Given a 
    # patient's blood type and a list of available blood types figure out 
    # if the patient will survive.

    # Input: The person's blood type as a string, and a list of strings 
    # with available blood types.
    # Output: True if the person can be saved, and false otherwise.

    # Example one;
    "Input blood type: B+"
    'Input list of available blood types: ["A-", "B+", "AB+", "O+", "B+", "B-"]'
    "Output survivability: True"

    # Example two
    "Input blood type: AB-"
    'Input list of available blood types: ["O+", "AB+"]'
    "Output survivability: False"

    # Your code goes here!

    if blood_type in donated_blood:
        return True

    else:
        return False

print(survive("B+", ["A-", "B+", "AB+", "O+", "B+", "B-"]))

def survive_second(blood_type, donated_blood): # Jeg har lavet lidt om på den "start-kode", som jeg fik.

    if blood_type in donated_blood:
        return True

    else:
        return False

print(survive_second("AB-", ["O+", "AB+"]))
print(40*"-")
#__________________________________________________________________________

# Opgave 16; - Done √

def rna(dna): # Jeg har lavet lidt om på den "start-kode", som jeg fik.
    
    # Description:

    # DNA (deoxyribonucleic acid) is the molecule that carries the genetic 
    # information of all known living organisms (life on exoplanets might be 
    # totally different!). The genetic information is stored as a sequence of 
    # nucleotides of which there are four: adenine (A), cytosine (C), 
    # guanine (G), and thymine (T). So a DNA sequence can be stored on a 
    # computer as a string of ATCG characters.

    # Certain DNA sequences contain the information required to syntheize 
    # proteins, and the first step in the process is to transcribe the DNA 
    # sequence into a ribonucleic acid (RNA) sequence. The transcription 
    # process is biochemically complex, however the resulting RNA sequence is 
    # simple: it's equal to the DNA sequence with all instances of T replaced 
    # with a U (uracil) and the sequence is reversed.

    # Given the DNA string as input, return the transcribed RNA string. Keep 
    # in mind that these sequences can be very long (the human genome is 
    # 3,088,286,401 characters or base pairs long!).

    # Input: A string of ATCG characters representing a DNA sequence.
    # Output: The RNA sequence corresponding to the input DNA sequence.

    # Example;

    # Input DNA:  "CCTAGGACCAGGTT"
    # Output RNA: "UUGGACCAGGAUCC"

    # Description:

    # Your code goes here!

    rna = ''

    # reversed(object)

    print("The following is a certain dna-string; \n" + str(dna))
    print("")
    dna = dna.replace("T", "U")
    print("We have now replaced all of the T's with U's.")
    print(dna)
    rna = dna[::-1]
    print("")
    print("After now having reversed the dna string, the rna-string is; \n")
    return rna

print(rna("CCTAGGACCAGGTT"))
print(40*"-")
#__________________________________________________________________________

# Opgave 17;

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

def break_caesar_cipher(ciphertext, known_word):
    plaintext = ''

    # Your code goes here!

    for i in (ciphertext):
        for idx, j in enumerate(alphabet):

            if i == alphabet[-1]:
                plaintext += alphabet[2]
                break

            elif i == alphabet[-2]:
                plaintext += alphabet[1]
                break

            elif i == alphabet[-3]:
                plaintext += alphabet[0]
                break

            elif i == " ":
                plaintext += " "
                break

            elif i == alphabet[idx]:
                plaintext += alphabet[idx + 3]

    return plaintext

print(break_caesar_cipher("QEB NRFZH YOLTK CLU GRJMP LSBO QEB IXWV ALD", "fox"))
print(40*"-")
# Opgave 18;

"""Egne_projekter/digits = {
    'black': 0,
    'brown': 1,
    'red': 2,
    'orange': 3,
    'yellow': 4,
    'green': 5,
    'blue': 6,
    'violet': 7,
    'grey': 8,
    'white': 9
    }

    multiplier = {
        'pink': 0.001,
        'silver': 0.01,
        'gold': 0.1,
        'black': 1,
        'brown': 10,
        'red': 100,
        'orange': 10 ** 3,
        'yellow': 10 ** 4,
        'green': 10 ** 5,
        'blue': 10 ** 6,
        'violet': 10 ** 7,
        'grey': 10 ** 8,
        'white': 10 ** 9
    }

    tolerance = {
        'none': 0.2,
        'silver': 0.1,
        'gold': 0.05,
        'brown': 0.01,
        'red': 0.02,
        'green': 0.005,
        'blue': 0.0025,
        'violet': 0.001,
        'grey': 0.0005
    }
"""

""" # def resistance(band_colors):

    #     # # Input resistor band colors: ["green", "blue", "yellow", "gold"]
    #     # # Output nominal resistance: 560000
    #     # # Output minimum resistance: 532000
    #     # # Output maximum resistance: 588000
        
    #     # n_bands = len(band_colors)

    #     # nominal_R = 0
    #     # minimum_R = 0
    #     # maximum_R = 0

    #     # # Your code goes here!



    #     # return nominal_R, minimum_R, maximum_R

    # # print(resistance(["green", "blue", "yellow", "gold"]))
"""

# # Opgave 25;

""" # def rock_temperature(S, a, e): # - Nope (Kan ikke laves? Mangler formlen)
    #     # Magter ikke at skrive den beskrivelse ind, holy shit

    #     # Albedo - evne til at absorbere lys(energi).
    #     # Emissivity = evne til at "modstå" lys(energi).
    #     # S = solar constant
    #     # a = albedo
    #     # e = emissivity
        
    #     # Input: A solar constant "S" in "W/m^2", albedo "0 =< a =< 1", and
    #     # an emissivity "0 =< e =< 1".

    #     # Output: The temperature of the rocky planet in Celsius. 
    #     # Remember to convert from Kelvin (K) to Celcius (C) using;

    #     # C = K - 273.15

    #     # Kan ikke fucking se formularen på hjemmesiden!!!!!
    #     # Den fucker!!!

    #     T = 0

    #     # Your code goes here!

    #     return T

    # print(rock_temperature(1361, 0.306, 0.612))

    # # Forhåbentlig;
    # # Output rock temperature: 14.059374
"""