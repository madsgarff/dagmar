
import random

## Ex.1 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    linjer i filen.
"""
print(40*"-")

with open('text.txt', 'r') as f:
    print(f.read())

# Linjemæssigt er der noget, som ikke stemmer overens med, hvad der står
# i filen "text.txt", terminalen og termialens svar på, hvor mange 
# linjer der er.

f = open("text.txt", "r")
line_count = 0
for line in f:
    if line != "\n":
        line_count += 1

print(line_count)
f.close()

## Ex.2 - begge fungerer (with open og open())
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    ord i filen.
"""
print(40*"-")

f = open("text.txt", "r")

word_count = 0

for i in f:
    word_count += 1

print(word_count)

f.close()

with open("text.txt", "r") as f:
    word_count += 1

print(word_count)

## Ex.3 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    bogstaver i filen.
"""
print(40*"-")

f = open("text.txt", "r")

letter_count1 = 0

for i in f:
    for j in i:
        letter_count1 += 1

print("There are", letter_count1, "letters in the story of Movy Dick.")
f.close()

## Ex.4 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    mellemrum i filen.
""" 
print(40*"-")

f = open("text.txt", "r")

contents = f.read()
counter = contents.count(" ")

print("A space accours", counter, "times in the story of Moby Dick.")

f.close()

## Ex.5 - Done - andet resultat, når "with open" er brugt i opgave 11.
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    vokaler i filen.
"""
print(40*"-")

vowels = ["a", "e", "i", "o", "u"]
found = {}

found["a"] = 0
found["e"] = 0
found["i"] = 0
found["o"] = 0
found["u"] = 0

f = open("text.txt", "r")

for letter in f:
    for vowel in letter:
        if vowel in vowels:
            found[vowel] += 1

print("In total, there are", sum(found.values()), "vowels in the story"
"of Moby Dick")

f.close()
 
## Ex.6 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    linjer i filen, som ikke begynder med 'I'.
""" 
print(40*"-")

f = open("text.txt", "r")

count = 0

for i in f.readlines():
    if i.startswith("I"):
        pass
    else:
        count += 1

print(count, "lines does not start with 'I' in the story of Moby Dick.")

f.close()

## Ex.7 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    ordet 'the' i filen.
""" 
print(40*"-")

f = open("text.txt", "r")

contents = f.read()
counter = contents.count("the")

print("The letters 'the' accours", counter, "times in the story of Moby Dick.")

f.close()

f = open("text.txt", "r")

count = 0
word = "the"
for i in f:
    if word in i.split():
        count += 1

print("The word 'the' accours", count, "times in the story of Moby Dick.")
f.close()

# Forskellen på de to ovenstående er, at den øverste finder hvor mange gange
# "the" står i teksten. Metoden .split() skaber en liste, og finder herefter
# ud af, hvor mange gange "the" står i listen. Af den grund er det kode nummer
# 2 der er korrekt for denne opgave.

## Ex.8 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Print de ord fra 
    filen, som har < 5 bogstaver, dvs. 'yes' skal printes 'whatever' skal ikke. 
""" 
print(40*"-")

f = open("text.txt", "r")

k = str(f.read(500))
v = k.split()
x = []

for i in v:
    if len(i) < 5:
        x.append(i)
print(x)

f.close()

## Ex.9 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
    (med et print for at bekræfte at der faktisk sker noget) antallet af 
    ord, som ender på bogstavet 'e' i filen.
"""
print(40*"-")

f = open("text.txt", "r")

countForE = {'e': 0}

for i in f:
    for j in i:
        if j.endswith('e'):
            countForE['e'] += 1

for key, value in countForE.items():
    print("Words in the story of Moby Dick that ends with", key, "occurs", 
    value, "times.")

f.close()

## Ex.10 - Done
"""
    Lav en funktion, som åbner filen 'text.txt' med 'open'. Print de sidste 10
    linjer af filen.
""" 
print(40*"-")

f = open("text.txt", "r")

lines = f.readlines()
last_10_lines = lines[-10:]

print(last_10_lines)

f.close()

#_________________________________________________________________________

## Ex.11 - Done
"""
    Udvælg 5 af de foregående 10 opgaver. Omskriv funktionerne til at bruge 
    'with open' i stedet for 'open'.
"""
print(40*"-")
# ----------------------------------- Ex11 - 1 ------------------------------

with open("text.txt", "r") as f:

    line_count = 0
    for line in f:
        if line != "\n":
            line_count += 1

print(line_count)
print(40*"-")
# ----------------------------------- Ex11 - 2 ------------------------------
with open("text.txt", "r") as f:
    word_count += 1

print(word_count)

print(40*"-")
# ----------------------------------- Ex11 - 3 ------------------------------
letter_count2 = 0

with open("text.txt", "r") as f:
    for i in f:
        for j in i:
            letter_count2 += 1

print("There are", letter_count2, "letters in the story of Moby Dick.")

print(40*"-")
#------------------------------------ Ex11 - 5 ------------------------------
with open("text.txt", "r") as f:

    for letter in f:
        for vowel in letter:
            if vowel in vowels:
                found[vowel] += 1

    for k, v in sorted(found.items()):
        print(k, "was found", v, "times")

    # Vi finder i denne kode antallet af alle vokaler på to måder.
    # 1) Forloop

    total = 0

    for k in vowels:
        total += found[k]

    print("")
    print("In total, there are", total, "vowels in the story of Moby Dick. "
    "We've used a forloop to solve this problem.")
    print("")

    # 2) Indbygget function "sum" og ".values()"

    total = sum(found.values())

    print("In total, there are", sum(found.values()), "vowels in the story of "
    "Moby dick. This code uses built-in methods.")

print(40*"-")

#------------------------------------ Ex11 - 6 ------------------------------
# Resultatet er anderledes end i den originale opgave 5, da jeg dengang tjekkede
# for "I", og denne gang tjekker jeg for "i". Jeg gør præcist det samme, og får de
# samme resultater begge steder.

with open("text.txt", "r") as f:

    count = 0

    for i in f.readlines():
        if i.startswith("i"):
            pass
        else:
            count += 1

    print(count, "lines does not start with 'i' in the story of Moby Dick.")

## Ex.12 - Done
"""
    Lav en funktion, som opretter en ny fil kaldet 'ex12.txt'. Læs de første 
    10 linjer fra 'text.txt' og skriv dem til 'ex12.txt'. 

    Hint: Åben begge filer, dvs. to fil objekter. Det her er desuden også 
          et eksempel på, at det kan give mening at blande de to syntax'er - 
          'with open'/'open'.
"""
print(40*"-")

f = open("text.txt", "r") # Vi skal huske, at denne fil skal være åben, før
                          # vi kan arbejde med den i forhold til en anden fil.

#print(f.readlines()[:9]) # Her printer vi de første 10 linjer til terminalen.

with open("ex12", "w+") as f1: 
    f1.writelines(f.readlines()[:9])

# Når jeg kører denne, så bliver der lavet en ny txt. fil, som hedder "ex12". 
# Den ligger til venstre sammen med de andre mapper/filer.
# Vi skriver "w+" for at "write and read". Hver gang vi skaber en ny txt. fil,
# skriver vi "w" eller "w+". Findes filen ikke allerede, så skaber python 
# automatisk en ny.

f.close()

## Ex.13 
""" 
    Lav en funktion, som læser de 10 linjer, med 'open with', fra 'ex12.txt'.
    Erstat alle 'i' med 'j' og skriv resultatet til filen. Print indholdet 
    af filen.
"""

print(40*"-")

# with open("ex12", "r") as f1:
#     print(f1.read())

#     for idx, i in enumerate(f1.read()):
#         for ydx, y in enumerate(i):
#             if "i" in y:
#                 i[ydx] = y.replace("i", "j")

#     print(f1.read())

## Ex.14 - Done
"""
    Lav en funktion, som læser en 'random linje' fra filen 'ex12.txt'. Opret 
    en ny fil 'ex14.txt' og skriv den 'random linje' til den nye fil. 
    Print indholdet af filen.
"""
print(40*"-")

random = random.choice(list(open("ex12")))
print(random)

with open("ex14.txt", "w") as f2:
    f2.writelines(random)

## Ex.15
"""
    Lav en funktion, som læser den første linje i 'text.txt' og den første 
    linje i 'ex12.txt'. Sæt herefter de to linjer sammen. Brug herefter 
    enumerate til at loope over stringen. Fjern hvert fjerde bogstav og alle 
    mellemrum. Gem resultat i en ny fil -> 'ex16.txt'.
"""
print(40*"-")

f = open("text.txt", "r")
f1 = open("ex12", "r")

#Heads up; den første linje i ex12 og text.txt er den samme.

with open("ex14.txt", "a+") as f2:
    a = f.readline().rstrip()
    b = f1.readline()

    joinedLines = a + b
    print(joinedLines)

#     x = list(joinedLines)

#     for idx, i in enumerate(joinedLines):
   


## Ex.16
"""
    Lav en funktion, som opretter en ny tekstfil for alle bogstaver i alfabetet,
    dvs. 'a1.txt., 'b2.txt', ... , 'å29.txt'. Brug enumerate til at angive 
    de forskellige tal. 
"""

## Ex.17
"""
    Lav en generel funktion, som tager to input - (self, filnavn, tekst) - 
    og 'append', dvs. tilføj til sidst, teksten til den givne fil. 
"""

## Ex.18
"""
    Lav en funktion, som tager to input - (self, filnavn, bogstav) - 
    og åben filen, tæl antallet af de bogstav, returner dette. 
"""

## Ex.19
"""
    Lav en funktion, som tager to input - (self, filnavn, liste) -
    og åben filen, skriv inputtet fra listen til filen en ad gangen, og
    print filens input. 

    (brug en lille fil, så den ikke skal printe en hel bog)
"""

## Ex.20
"""
    Lav en funktion, som læser indholdet fra en fil og vender det om linje 
    for linje, dvs. 

    1. Hej med dig
    2. Jeg er sej
    3. Whatever mayn 

    ->

    3. Whatever mayn 
    2. Jeg er sej
    1. Hej med dig

    Du kan skrive det til en ny fil eller bare printe det i terminalen. Og igen
    - brug en lille fil. 
"""

## Ex.21
"""
    Lav en funktion, som opretter et nyt dictionary. Tilføj 10 forskellige 
    elementer. Slet 3 af dem. Opdater 3 af dem med nye værdier. 
    Skriv resultatet, dvs. indholdet af dictionariet, til en ny fil kaldet 
    'ex21.txt'. 
"""

## Ex.22
"""
    Lav en funktion, som læser indholdet af 'ex21.txt' ind som et 
    dictionarie. Slet et 'random' 'key/value' pair. Print resultatet, dvs. 
    det nye dict, på en pænt måde. 
"""

## Ex.23
"""
    Lav en funktion, som tager to input - (self, filnavn, bogstaver) - 
    og åben filen. Bogstaver er en liste af bogstaver, som vi ønsker at tælle 
    i forhold til indhold fra filen. Derfor brug 'text.txt.' og tæl alle 
    bogstaverne fra listen, dvs. 

    filnavn = 'text.txt'
    bogstaver = [a,b,c]

    Gem antallet af a'er, b'er og c'er i et dictionary. Print resultatet
    på en pæn måde. Gem resultatet som filen 'ex24.txt'.
"""

## Ex.24
"""
    Lav en funktion, som læser fra filen 'ex21.txt'. Læs indholdet af filen 
    to gange til to forskellige dictionaries. Loop over det ene dictionary 
    og tilføj indholdet til andet. Print resultatet på en pæn måde. 
    Gem resultatet som filen 'ex25.txt'.
"""

## Ex.25
"""
    Lav en funtion, som sletter 'duplicate values' i et dict, dvs. 

    dict = {'a':10, 'b':10, 'c':20}

    bliver til..

    dict = {'a':10, 'c':20}

    Print resultatet på en pæn måde. 
"""
