## Ex.2
"""
+    Lav en funktion, som læser fra en fil, hele filen, og tæller hvert 
+    eneste bogstav. Gem resultatet i et 'dict'. Pretty print når alt er 
+    talt op.
+"""

class exer8:
    
    def __init__(self):
        pass

    def ex1(self):

        """
            Lav en funktion, som læser fra en fil (lav en fil med noget 
            tekst), og gør det både med 'read()', 'readline()' og 
            'readlines(). Print resultaterne. Se forskellen. 
            Forklar forskellen. Forstå forskellen.
        """
        with open("ex12.txt", "r") as f1:

            print(f1.readline())
            print(f1.readline())
            print(f1.readline())
            print(f1.readlines())

        print(40*"-")

        # Den indbyggede funktion .readline() hiver fat i den første linje. 
        # Grunden til, at den næste linje bliver printet, hvis man bruger
        # .readline() igen er, at den første linje bliver "fjernet" - 
        # som Mads sagde, man kan tænke på det som om, at man langsomt 
        # "tømmer teksten". Når den første linje er læst, så eksisterer 
        # den i teorien ikke længere. .readlines() hiver fat i de 
        # resterende linjer og spytter dem ud i et array. Siden vi allerede
        # har brugt .readline() tre gange, så springer .readlines()
        # de første tre over.

        with open("ex12.txt", "r") as f2:

            print(f2.readlines())

        # Vi har åbnet samme tekst som før, men siden vi brugte "with open", 
        # så er filen lukket igen. Vi kan derfor "starte forfra", og HELE 
        # teksten bliver nu spyttet ud i et array. Havde vi læst alle linjer 
        # enkeltvis først - eller brugt funktionen .read(), så ville 
        # ex12.txt ikke have mere at printe.Af den grund ville der blive 
        # printet et tomt array til terminalen.

        print(40*"-")

        with open("ex12.txt", "r") as f3:

            print(f3.read())

        # Hele teksten fra den fil, som vi åbner, bliver printet til 
        # terminalen - dog ikke i et array med /n for at indikere, at der 
        # kommer et linjeskift. Og igen, .read() tømmer stadig filen. Så 
        # hverken .readline() eller .readlines() ville returnere noget 
        # herefter.

    def ex2(self):

        """
            Lav en funktion, som læser fra en fil, hele filen, og tæller 
            hvert eneste bogstav. Gem resultatet i et 'dict'. Pretty print 
            når alt er talt op.
        """

        with open("ex12.txt", "r") as f:

            letterCount = 0
            letterDict1 = {}

            for i in f:
                for j in i:
                    letterCount += 1

            print(letterCount)
            letterDict1["Amount"] = letterCount
            print(letterDict1)

            # letterDict2 = {}

            # for i in f:
            #     for j in i:
            #         letterDict2[j] += 1

            # print(letterDict2)

            # letterDict3 = {}

            # for i in f:
            #     for j in i:
            #         if key in letterDict:
            #             letterDict[key] += 1
            #         else:
            #             letterDict[key] = [value]

            # print(letterDict3)

if __name__ == "__main__":
    runExercises = exer8()
    runExercises.ex1()
    print(40*"-")
    runExercises.ex2()

#with open("ex12.txt", "r") as f:

    # letterCount = 0

    # for i in f:
    #     for j in i:
    #         if j != " ":
    #             letterCount += 1

    #print(letterCount)

    # Ovenstående er antallet af bogstaver i ex12.txt.

    # letterDict = {}

    # letterDict["f"] = 0

    # for i in f:
    #     for j in i:
    #         if j == " ":
    #             letterDict[key] += 1


    # nonLetter = " "

    # spaceDict = {}
    # spaceDict[" "] = 0

    # letterList = []

    # for line in f:
    #     line = line.split()

    #     for word in line:
    #         word = word.split()

    #         for letter in word:
    #             letterList.append(letter)

    # letterList.sort()

    # letterDict = {}

    # for letter in letterList:
    #     letterDict[letter] = letterList.count(letter)

    # for letter in letterDict:
    #     print("{:^8}{:^8d}".format(letter, letterDict[letter]))

    # for i in f:
    #     for j in i:
    #         if j == " ":
    #             spaceDict[j] += 1
    #         else:
    #             letterDict[j] = {1}

    # if this_key in my_dict:
    #     my_dict[this_key].add(this_value)
    # else:
    #      my_dict[this_key] = {this_value}

    # for i in f:
    #     for j in i:
    #         for k in j:
    #             if k == nonLetter:
    #                 spaceDict[k] += 1
    #             else:
    #                 letterDict[k] += 1

    # for i in f:
    #     for j in i:
    #         if j == " ":
    #             spaceDict[j] += 1
            

    # for i in f:
    #     for j in i:
    #         for k in j:
    #             if k != nonLetter:
    #                 letterDict[val] += 1

    # for i in f:
    #     for j in i:
    #         if j != nonLetter:
    #             letterDict[val] += 1

    # for i in f:
    #     (key, value) = line.split()
    #     letterDict[int(key)] = val

    # for i in f:
    #     if i != " ":
    #         letterDict[i] += 1

    # for i in f:
    #     for j in i:
    #         if j not in nonLetter:
    #             letterDict[j] += 1

    # for i in f:
    #     for j in i:
    #         x = j.split(" ")
    #         a = x[0]
    #         b = x[1]
    #         letterDict[a] = b

    # for i in f.readlines():
    #     for j in i:
    #         if j not in nonLetter:
    #             letterDict[j] += 1

    #print(letterDict)
    #print(spaceDict)

## Ex.3
"""
+    Lav en funktion, som læser fra en fil, linje for linje, og tæller hvert
+    eneste bogtav. Gem resultatet i et 'dict'. Pretty print når alt er talt 
+    op.
+"""

## Ex.4
"""
+    Lav en funktion, som læser fra en fil, linje for linje - meeen.. linjerne
+    skal gemme i en liste. Tæl herefter hver eneste bogstav. Gem resultatet 
+    i et 'dict'. Pretty print når alt er talt op. 
+"""