# -*- coding: utf-8 -*-

#import module datetime

import random
from random import randrange

#######################################################
#                                                     #
#           Og vi fortsætter på samme måde.           #
#                                                     #
#######################################################

"""
    Her er en række beskrivelser af nogle funktioner, som skal færdiggøres. 
    Nogle af dem er nemme. Nogle af dem er svære. 

    Pointen er, at vi skal have banket idéen om, at man kan give funktioner 
    variabler, som man kan bruge inde i funktioerne, på plads.

    Hvis der står, at du skal return, så skal du return.
    Hvis der står, at du skal printe, så skal du printe fra funktionen.

    Så det her:

    - Write a function that takes two int variables, multiplies them
    - and returns them.

    - Print out the result of using the function

    Betyder at du skal returnere fra funktionen og printet _UDENFOR_ funktionen.

    Og det her:
    
    - Write a function that sorts a given array in descending order.
    - Print every other element from the array.

    Betyder at du skal printe til terminalen inde fra funktionen.
"""

### DE FØRSTE FEM OPGAVER ER BARE LIGE FOR AT KOMME I GANG OG FOR AT SKRIVE HELE 
### FUNKTIONEN SELV !!!!

### DU MÅ IKKE BRUGE PYTHONS BIBLIOTEK TIL AT LØSE OPGAVER - INGEN 
### MAX, MIN ETC. 

### KUN MATH BIBLIOTEKET ER OK. SÅ FX MATH.SQRT() ER OK.

# Opg. 1 ____________________________________________________________________________________________________________

""" 
   Write a function that takes two int variables, multiplies them
   and returns them.

   Print out the result of using the function.
"""

def multVar(a, b):

    return a * b

print(multVar(2, 3))
print("")

# _______________________________________________________ Opg. 2 _____________________________________________________

"""
    Write a function that takes an int and an array. Multiply 
    every single element of the array and return it.

    Print out the result of using the function.
"""

def multInt1(element, arr):

    x = []

    for i in arr:
        x.append(element * i)

    return x

print(multInt1(5, [1, 2, 3]))
print("")

def multInt2(element, arr):

    for idx, _ in enumerate(arr):
        arr[idx] = arr[idx] * element

    return arr

print(multInt2(2, [10, 11, 12]))
print("")

# _______________________________________________________ Opg. 3 _______________________________________________________
"""
    Write a function that concatenates two strings and return
    the resulting string.

    Print out the result of using the function.
"""

def combineStrings1(str1, str2):

    print(str1 + str2)

combineStrings1("Christ", "mas ")

def combineStrings2(str1, str2):

    return str1 + str2

print(combineStrings2("Win", "ter "))
print("")

# _______________________________________________________ Opg. 4 _____________________________________________________
"""
    Use the previous written function combineStrings(str1, str2) to
    create a string from the two arguments str1 and str2. Pass the 
    resulting string to this function, multCombineString(str, arr), 
    as the first argument. 
    
    The second argument is an array of ints [1,2,3,4,5]. 

    Pass the first and the second argument to the function and multiply 
    every single element in the array and put the result in a new array.

    Return the new array.

    Print out the result of using the function.
"""

def multCombineString(str1, arr):

    x = []

    for i in arr:
        x.append((i * str1))

    return x

x = combineStrings2("Holi", "day ")
print(multCombineString(x, [1, 2, 3, 4, 5]))
print("")

# _______________________________________________________ Opg. 5 _____________________________________________________
"""
    Return the greatest number of the two ints. 
    You are not allowed to use the built-in function max().
    Print out the result of using the function.
"""

def greatest1(a, b):

    x = True

    if a < b:
        x = False

    return x

print(greatest1(10, 20))
print("")

def greatest2(c, d):

    if c > d:
        print(c)

    if c < d:
        print(d)

print(greatest2(10, 15))
print("")

def greatest3(e, f):
 
    if e > f:
        return e

    else:
        return f
    
print(greatest3(2, 3))
print("")

def greatest4(a, b):
    
    return a > b

# _______________________________________________________ Opg. 6 _____________________________________________________

"""
    Returns True if the element can be found in the list. Otherwise
    return false.
    Print the result to the terminal outside of the function.
"""

def isElementInArray(arr, ele):

    for i in arr:
        if ele not in arr:
            return False

    return True 

print(isElementInArray([1, 2, 3], 2))
print("")
# _______________________________________________________ Opg. 7 _____________________________________________________

"""
    Remove the element from the list if it can be found and return it.
    Otherwise return False. 

    Print the result to the terminal outside of the function.
"""

def isElementFound(arr, ele):

    if ele in arr:
        arr.remove(ele)

    else:
        return False

    return ele

testArray = [1, 2, 3]

print(isElementFound(testArray, 3))
print(testArray)

print("")
# _______________________________________________________ Opg. 8 _____________________________________________________

"""
    Print each list from the nested array to the terminal:

    arr = [[1,2,3], [4,5,6], [7,8,9]]

    Like this:

    [1,2,3]
    [4,5,6]
    [7,8,9]
"""

def printNestedList(arr):

    for x in arr:
        print(x)

printNestedList([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

print("")
# _______________________________________________________ Opg. 9 _____________________________________________________

"""
    Print the first item of each of the arrays in the nested array
    to the terminal.

    arr = [[1,2,3], [4,5,6], [7,8,9]]

    Like this:

    1
    4
    7
"""

def printFirstItem1(arr):

    for i in arr:
        print(i[0])

printFirstItem1([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

print("")
# _______________________________________________________ Opg. 10 _____________________________________________________

"""
    Remove the second array of the nested arrays.

    arr = [[1,2,3], [4,5,6], [7,8,9]]

    Print the result to the terminal outside of the function.

    Like this:

    [[1,2,3], [7,8,9]]
"""

def removeEleAndPrintArray(arr):

    del arr[1]

    return arr

print(removeEleAndPrintArray([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))

print("")
# _______________________________________________________ Opg. 11 _____________________________________________________

"""
    Remove each of the first items of the nested arrays.

    arr = [[1,2,3], [4,5,6], [7,8,9]]

    Print the result to the terminal outside of the function.

    Like this:

    [[2,3], [5,6], [8,9]]
"""

def removeFirstEleAndReturnArray(arr):

    for i in arr:
        del i[0]

    return arr

print(removeFirstEleAndReturnArray([[1, 2, 3], [4, 5, 6], [7, 8, 9]]))
print("")

# _______________________________________________________ Opg. 12 _____________________________________________________
"""
    Multi each of the elements of the nested array with an interger
    i.e. the element given.

    Print the result to the terminal outside of the function.
"""

def multiEachElementInArray(arr, ele):

    # for i in arr:
    #     for j in i:
    #         idx = i.index(j)
    #         i[idx] = j * ele

    for i in arr:
        for idx, j in enumerate(i):
            i[idx] = j * ele

    return arr

print(multiEachElementInArray([[1, 2], [3, 4], [5, 6]], 2))
print("")

""" - Beskrivelse
    Vi hopper ind i arrayets første element i ([1, 2]). Herfter kigger vi på
    de to elementer (hvert kaldet for j). Vi definerer en variabel og siger her, 
    at en indexplads j i i, defineres som idx.
    Til sidst fortæller vi python, at på den indexplads i i, skal j ganges med
    ele.
"""

# _______________________________________________________ Opg. 13 _____________________________________________________

"""
    Print this pattern to the terminal.

    #   *
    #   **
    #   ***
    #   ****
"""

def printPattern():

    mango = ""
    for i in range(4):
        for j in range(i + 1):
            mango = mango + "*"
        mango = mango + "\n"   
    print(mango)
    #   mango = "*\n**\n***\n****\n" 

printPattern()

#____________________________________________________________________________________________________________________________________

def simplePrintPattern():

    # for i in range(0, 4):
    #     print("")
    #     for j in range(i + 1):
    #         print("*"),

    for i in range(1, 5):
        print("*" * i)

simplePrintPattern()
print("")

""" - Beskrivelse
    
    Vi kan se, at vi har et mønster, som strækker sig over 4
    linjer - af denne grund vil vi loope 4 gange, så vi skriver range(0, 4).
    print("") står på næste linje fordi - 
    Vi hopper nu ind i det næste forloop for første gang. 
    Her står "for j in range(i + 1). j er vores element inde i "hovedelementet"
    i. Der står range(i + 1), hvilket betyder, at hver gang vi når ind i det andet loop
    så skal vi have "mængden" af "*", som der allerede eksisterer + 1.
    Første gang er der ikke en *, så der bliver lagt en enkelt til.
    Vi hopper nu ud af loopet, og ind i det næste, som er på næste linje. Vi lægger endnu
    en * til og har nu
    *
    * *
    Loopet stopper efter 4 gange - denne funktion ville også fungere fint, hvis vi til 
    start havde skrevet "for i in range(4)".
"""
# _______________________________________________________ Opg. 14 _____________________________________________________

"""
   Print this pattern to the terminal.

      *
      ***
      *****
      *******

     Hint: Print only odd numbers.
"""

def printOddPattern():

    for i in range(7):
        print("")
        for j in range((i * 2) + 1):
            print("*"),

printOddPattern()
print("")

# Opg. 15

"""
   Print this pattern to the terminal.

      A
      B C
      D E F
      G H I J
      K L M N O

      Hint: Look through pythons standard library (yes you can use it)
            and find a function that changes integers into strings.

     (maybe hard)
"""

def printLetterPattern():
    count = 65

    for i in range(1,5):
        mango = ""
        for j in range(i):
            mango = mango + " " + chr(count)
            count += 1

        print(mango)

printLetterPattern()

# Opg. 16

#"""
#    Look through pythons standard library and find a way to print 
#    the current time and date. 

#    Make it look nice before you print it.
#"""

#def printDateTime():

# _______________________________________________________ Opg. 17 _____________________________________________________

"""
    Create a nested array and fill each of the 5 inner arrays with 5's.
    
    Print the result to the terminal outside of the function.

    [[5,5,5,5,5], [5,5,5,5,5], [5,5,5,5,5], [5,5,5,5,5], [5,5,5,5,5]]
"""

def makeNested5s(arg1):

    arr = []

    for i in arg1:
        for idx, j in enumerate(i):
            if j != 5:
                i[idx] = 5
        
        arr.append(i)

    return arr

    # arr = []

    # for i in arg1:
    #     for j in i:
    #         if j != 5:
    #             j = 5

    #         arr.append(j)

    # return arr

x = [[1, 2, 3, 4, 5], [2, 3, 4, 5, 6], [3, 4, 5, 6, 7], [4, 5, 6, 7, 8], [5, 6, 7, 8, 9]]

#print(list(enumerate([1, 2, 3, 4, 5])))
print(makeNested5s(x))
#print(x)

""" - Beskrivelse

    Vi laver til start et helt nyt array. Herefter går vi ind i dette array
    og ser på det første element i, som også er et array. På anden linje
    (ved andet forloop) præsenterer vi to nye variabler - idx og j.
    idx repræsenterer indexpladsen i i, og j repræsenterer elementet på
    indexpladsen idx. Vi bruger enumerate, som laver en counter. 
    Den går altså ind i i og hiver ikke kun fat i j, men også i den idexplads,
    som j står på. 
    if j != 5: betyder ordret "hvis elementet j ikke er lig med 5."
    På den sidste linje i det nestede forloop fortæller vi python, at hvis elementet
    j ikke er lig med 5, så skal det element som står på indexpladsen i j ændres
    til 5. 
    Efter at have tilføjet alle elementerne i til det tomme nye array, returnerer vi dette.
"""

print("")
# Opg. 18 - ish

"""
    Create a nested array and fill each of the 5 inner arrays with 
    random numbers created using pythons build-in library 'random'.

    Hint 1: You can get a random integer between 1-1000 with 
            randomNumber = randrange(1,1000)

    Hint 2: from random import randrange
"""

def makeNestedWithRandom():
    
    arr = []

    for _ in range(5):
        arr.append([random.randint(0,1000)])


    array = []
    count = 0

    while count < 5:
        count += 1
        array.append([random.randint(0,1000)])

    # arr.append([random.randint(0,1000)])
    # arr.append([random.randint(0,1000)])
    # arr.append([random.randint(0,1000)])
    # arr.append([random.randint(0,1000)])
    # arr.append([random.randint(0,1000)])

    #return arr
    return array

print(makeNestedWithRandom())

# Opg. 19

"""
    Create a nested array and fill each of the 5 inner arrays with 
    random numbers created using pythons build-in library 'random'.

    Check if any of the created element is divisible (can be divided)
    by the element that is given as the argument to the function.

    If so return the element otherwise return False.
"""

def mango(ele):

    arr = []

    for _ in range(5):
        arr.append([random.randint(0,1000)])

    for i in arr:
        for j in i:
            if j % ele == 0:
                return ele

    return False

print(mango(97))

# Opg. 20 

#"""
#    Create a nested array and fill each of the 5 inner arrays with 
#    random numbers created using pythons build-in library 'random'.

#    Check if any of the created element is divisible (can be divided)
#    by both the elements that is given as the argument to the function.

#    If so return the smallest of the arguments otherwise return False.
#"""

def checkEleAgainstRandom(arg1, arg2):
   
    arr = []

    for _ in range(5):
        arr.append([random.randint(0,1000)])
    print(arr)

    for i in arr:
        for j in i:
            if (j % arg1 == 0) and (j % arg2 == 0):
                return (arg1, arg2)
    
    return False

print(checkEleAgainstRandom(2, 17))
