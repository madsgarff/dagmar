#######################################################
#                                                     #
#           Og vi fortsætter på samme måde.           #
#                                                     #
#######################################################

"""
    Denne omgang omhandler klasser og lidt omkring dictionaries. 
    Måden klasser bygges op på er rimelig skematiske. De har fx. 
    altid en class Navn: i toppen og en 'constructor funktion' 
    (dvs. __init__ funktionen). Denne funktione kan tage argumenter, 
    men det er ikke altid den gør det.

    Vi forsøger at lave en klasse her, som har rimelig begrænset 
    funktionalitet, men har den struktur, som den skal have.
"""

### DE FØRSTE FEM OPGAVER ER BARE LIGE FOR AT KOMME I GANG OG FOR AT SKRIVE HELE 
### FUNKTIONEN SELV !!!!

### DU MÅ IKKE BRUGE PYTHONS BIBLIOTEK TIL AT LØSE OPGAVER - INGEN 
### MAX, MIN ETC. 

### KUN MATH BIBLIOTEKET ER OK. SÅ FX MATH.SQRT() ER OK.

# Opg. 1 - Done

""" 
    Du skal lave en klasse, som vi gjorde det, da vi sad sammen.
    Du bestemmer selv navnet på klassen. Klassen skal indeholde en 
    'constructor'. Dette er den standard __init__ metode, som du allerede
    har set. """

class Me:

    def __init__(self, eyes, height, weight, name, age, newName):

        self.eyes = eyes
        self.height = height
        self. weight = weight
        self.name = name
        self.age = age

    def getEyes(self): # getter
        return self.eyes

    def getHeight(self): # getter
        return self.height

    def getWeight(self): #getter
        return self.weight

    def getName(self): # getter
        return self.name

    def getAge(self): # getter
        return self.age

    def setNewName(self, newName): # setter
        self.name = newName

# Opg.2 - Done

"Importer din klasse i en seperat fil, som du kalder main.py."

# Opg. 3 - Done

"Lav en funktion i bunden af din main.py - sådan her:"

if __name__ == "__main__":
    print("hej")

# Opg. 4 --------- Nope

"""
    Forsøg at kør filen main.py
    Prøv at forklar (google) hvorfor resultatet er, som det er."""

# Opg. 5

# Erstat print("hej") og opret et object af den klasse, som du lavede 
# i opg.1 

# Opg. 6

# Lav din __init__ funktion om til at tage 2 argumenter:
# - En key 
# - Og en value 

# Opg. 7

# Opret getters og setters for de to argumenter, som er gemt som 
# variabler.

# Opg. 8

# Tilføj et dictionarie til self. - så du kan kalde self.mitDict, når 
# du senere skal bruge det.

# Opg. 9

# Lav en funktion i klassen, som gemmer de to variabler i det 
# dictionarie, som er oprettet under self.

# Lav herefter en funktion i klassen, som tager værdierne fra det dict,
# som er gemt under self. og printer dem sådan her:

# Key: "NØGLEVÆRDIEN" - Value: "VALUEVÆRDIEN"

# Opg. 10

# I main.py - opret objektet for klassen, som du har lavet.

# Brug metoden i klassen, som kan tilføje key:value pairs til det 
# dictionarie, der er i klassen, til at tilføje 5 forskellige key:vlaue
# par.

# Brug print metoden fra klassen til at printe alle de 5 tilføje par.