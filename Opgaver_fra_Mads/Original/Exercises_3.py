# -*- coding: utf-8 -*-

import random
from random import randrange
from random import randint
import math
from math import factorial


#######################################################
#                                                     #
#           Og vi fortsætter på samme måde.           #
#                                                     #
#######################################################

"""
    Her er en række beskrivelser af nogle funktioner, som skal færdiggøres. 
    Nogle af dem er nemme. Nogle af dem er svære. 

    Pointen er, at vi skal have banket idéen om, at man kan give funktioner 
    variabler, som man kan bruge inde i funktioerne, på p Excerlads.

    Hvis der står, at du skal return, så skal du return.
    Hvis der står, at du skal printe, så skal du printe fra funktionen.

    Så det her:

    - Write a function that takes two int variables, multiplies them
    - and returns them.

    - Print out the result of using the function

    Betyder at du skal returnere fra funktionen og printet _UDENFOR_ funktionen.

    Og det her:
    
    - Write a function that sorts a given array in descending order.
    - Print every other element from the array.

    Betyder at du skal printe til terminalen inde fra funktionen.
"""
# Opg. 1

""" 
    Using enumerate - write a function that takes two strings as 
    arguments, loop over the strings at the same time using the index,
    and print every single element of the strings simultaneously, one 
    by one.  
"""
def loopTwoStrings(str1, str2):

    for i in (str1, str2):
        for idx, j in enumerate(i):
            print(idx, j)

loopTwoStrings("Merry", "Christmas")

print("")
# Opg. 2

"""
    Using enumerate - write a function that creates a list of random
    length with random ints in it. Loop over the list and print every
    7'th element.
"""
# def loopRandomListPrintEvery7thElement():

#     x = []
#     for idx, i in enumerate(range(random.randrange(10))):
#         x[idx].append(random.randint(0, 10))

#     print(x)
    
#     for idx, i in enumerate(x):
#         print(x[::7])

# loopRandomListPrintEvery7thElement()

print("")
# Opg. 3

"""
    Using enumerate - write a function that creates a list of random 
    length filled with lists. So it is a nested list of random length
    with lists in it of random length. Fill it with ints. Print every
    third element in the nested lists.
"""
def printFromNestedLists():
   
    x = []
    for idx, i in enumerate(range(random.randrange(10))):
        x.append([])
        for y in range(random.randrange(10)):
            x[idx].append(random.randint(0, 10))
    
    print(x)

    for idx, i in enumerate(x):
        print(x[idx][::3])

printFromNestedLists()

print("")
# Opg. 4

"""
    Using enumerate - write a function that creates a triple nested
    list filled with lists of random ints. Loop and print every 
    single element.
"""
def tripleNestedPrint():

    x = []
    for idx, i in enumerate(range(random.randrange(10))):
        x.append([])
        for ydx, y in enumerate(range(random.randrange(10))):
            x[idx].append([])
            for jdx, j in enumerate(range(random.randrange(10))):
                x[idx][ydx].append([])
                for k in range(random.randrange(10)):
                    x[idx][ydx][jdx].append(random.randint(0, 10))

    print(x)

    for i in (x):
        for y in i:
            for j in y:
                for k in j:
                    print(k)
                
tripleNestedPrint()

print("")
# Opg. 5

"""
    Using enumerate - write a function that prints every other element 
    of a triple nested list filled with lists of random ints.  
"""
def tripleNestedPrintEveryOther():

    x = []
    for idx, i in enumerate(range(random.randrange(10))):
        x.append([])
        for ydx, y in enumerate(range(random.randrange(10))):
            x[idx].append([])
            for jdx, j in enumerate(range(random.randrange(10))):
                x[idx][ydx].append([])
                for k in range(random.randrange(10)):
                    x[idx][ydx][jdx].append(random.randint(0, 10))

    print(x)

    for idx, i in enumerate(x):
        for ydx, y in enumerate(i):
            for jdx, j in enumerate(y):
                print(x[idx][ydx][jdx][::2])

    # Hjælp fra Mads til modulus

tripleNestedPrintEveryOther()

"""
    OG .... nu stepper vi gamet lidt op. Så vi skal altså til at tænke 
    lidt selv igen - dvs. vi skal arbejde med programbeskrivelser og
    dermed løse opgaver selv.
"""

print("")
# Opg. 6

"""
    Wite a function that finds all numbers that are divisible by 7 
    but are not a multiple of 5 between 1000 and 2000. Print each
    of the numbers to the terminal.

    # Hint : range()
"""
def printDivsAndMults():

    for i in range(1000, 2000):
        if (i % 7 == 0) and (i % 5 != 0):
            print(i)

printDivsAndMults()
    
print("")
# Opg. 7 - Hjælp fra Mads

"""
    Write a function that computes the factorial of any number given.
    The factorial is defined as n = 5 , 5 * 4 * 3 * 2 * 1
    Print the result.
"""
def printFact(n):

    if n == 1:
        return 1
    
    else:
        return n * printFact(n - 1)
    
    #return n! = n * (n-1)!

print(printFact(5))

print("")
# Opg. 8

"""
    Write a function that reads n numbers from the terminal and 
    puts each of the numbers into a list. Use enumerate to loop
    over each index and print the numbers out to the terminal one by one.
"""
def readFromTerminalAndPrint(n):

    arr = []

    for i in range(n):
        arr.append(input("Write a number:"))
        for idx, i in enumerate(arr):
            print(i)

    print(arr)
    #print(n)

readFromTerminalAndPrint(5)

print("")
# Opg. 9

"""
    Write a function that uses the formula [2.90x + y*y] on each of 
    the members of a list. Use enumerate to loop over the list that 
    is given as an argument and return a new list appended with the 
    new values that is calculated.

    So.. 

    [(1,2),(2,3),(3,4)]

    ends up being ..

    [2.90 + 2*2, 2.90*2 + 3*3, 2.90*3 + 4*4]

    Print it to the terminal outside the function.
"""

print("")
# Opg. 10

"""
    Write a function that takes two integer arguments (x,y). Create a 
    two-dimensional list filled with 1's that has the size x*y.

    Print the result.
"""
