######################################################
#                                                    #
#           Vi prøver lige noget nyt her.            #
#                                                    #
######################################################

"""
    Her er en række beskrivelser af nogle funktioner, som skal færdiggøres. 
    Nogle af dem er nemme. Nogle af dem er svære. 

    Pointen er, at vi skal have banket idéen om, at man kan give funktioner 
    variabler, som man kan bruge inde i funktioerne, på plads.

    Hvis der står, at du skal return, så skal du return.
    Hvis der står, at du skal printe, så skal du printe fra funktionen.

    Så det her:

    - Write a function that takes two int variables, multiplies them
    - and returns them.

    - Print out the result of using the function

    Betyder at du skal returnere fra funktionen og printet _UDENFOR_ funktionen.

    Og det her:
    
    - Write a function that sorts a given array in descending order.
    - Print every other element from the array.

    Betyder at du skal printe til terminalen inde fra funktionen.
"""

# Jeg løser lige den første.. 

#def sumVars(a, b):
    #"""
    #    Write a function that takes two variables, sums them
    #    and returns them.

    #    Print out the result like this:

    #    x = sumVars(1,2)
    #    print(x)
    #    ....
    #    3
    #"""
#    return a+b

# # Opg.1 

"Write a function that takes two int variables, multiplies them"
"and returns them."
"Print out the result of using the function."

def multVars(a, b):

    x = a * b
    return x

result = multVars(2, 3)
print(result)

# Opg.2 

"Write a function that takes an int and an array. Multiply" 
"every single element of the array and return it."

"Print out the result of using the function."

def multArrayWithInt(a, arr):

    x = []

    for i in arr:
        x.append(i * a)

    return x
    
print(multArrayWithInt(2, [10, 20, 30]))

def multArrayWithInt2(a, arr):

    for idx, _ in enumerate(arr):
        arr[idx] = arr[idx] * a
        
    return arr

print(multArrayWithInt2(3, [10, 20, 30]))

# Opg.3

"Write a function that concatenates two strings and return"
"the resulting string."

"Print out the result of using the function."

def combineStrings(str1, str2):

    x = str1 + str2

    return x

print(combineStrings("He", "llo"))  

    # Hint : Does this function only work for strings? Why/why not? Test it.

def add(x, y):

    result = x + y

    return result

print(add(10, 15))

# Opg.4

# def multCombineString(str, arr): - Hjælp fra Mads :D

"Use the previous written function combineStrings(str1, str2) to"
"create a string from the two arguments str1 and str2. Pass the "
"resulting string to this function, multCombineString(str, arr), "
"as the first argument. "
        
"The second argument is an array of ints [1,2,3,4,5]. "

"Pass the first and the second argument to the function and multiply "
"every single element in the array and put the result in a new array."

"Return the new array."

"Print out the result of using the function."

def multCombineString(str1, arr):

    x = []

    for i in arr:
        x.append((i * str1))

    return x

x = combineStrings("he", "llo")
print(multCombineString(x, [1, 2, 3]))

# Opg.5

"Return the greatest number of the two ints. "
"You are not allowed to use the built-in function max()."
"Print out the result of using the function."

def greater(a, b):

    if a > b:
        print(a)

    else:
        print(b)

greater(2, 6)

def greater2(a, b):

    x = True

    if a < b:
        x = False

    return x

print(greater2(10, 20))

# Opg. 6

"Return the smallest number of the two ints."
"You are not allowed to use the built-in function min()."
"Print out the result of using the function."

def smaller(a, b):

    if a < b:
        print(a)

    else:
        print(b)

x = smaller(15, 10)

# Opg. 7

"Return True if a is greater than b otherwise return "
"False."
"Print out the result of using the function."

def isGreater(a, b):

    x = True

    if a > b:
        x = True
    
    else:
        x = False

    return x

print(isGreater(2, 3))

# Opg. 8

"Return True if a is smaller than b otherwise return "
"False."
"Print out the result of using the function."

def isSmaller(a, b):

    x = True

    if a < b:
        x = True

    else:
        x = False

    return x

print(isSmaller(2, 3))

# Opg. 9

"Return True if n is smaller than the length of the array."
"Otherwise return False."
"Print out the result of using the function."

def isNSmallerThanArray(n, arr):

    #x = True

    #if n < len(arr):
     #   x = True

    #else:
    # x = False

    return n < len(arr)
#    return x 

print(isNSmallerThanArray(5, [1, 2, 3]))

# Opg. 10

"Return True if int n is smaller than all the elements in the" 
"int array - else return False."

"Function is called like this:"

"nSmaller(10, [10,12,5,2,4,5])"

"Print out the result of using the function."    

def nSmaller(n, arr):

    x = True

    for i in arr:
        if n < i in arr:
            x = True
    
        else:
            x = False
    
    return x

print(nSmaller(1, [10, 12, 5, 2, 4, 5]))

# Opg. 10

"Write a function that replaces all the elements in the array arr"
"with the element a and return the array."

"Print out the result of using the function."

def replaceElements(a, arr): # Inplace

    x = []

    for i in arr:
        if i != a:
            x.append(a)

    return x

print(replaceElements(10, [1, 2, 3, 4, 5]))

# Hint : Does this function only work for strings? Why/why not? Test it.

def replaceInString(a, string):

    x = []

    for i in string:
        if i != a:
            x.append(a)

    return x

print(replaceInString("T", "Hello"))

# Opg. 11

"Write a function that replaces the elements in the int array that is"
"less than the int a and return the array."

"Print out the result of using the function."

def replaceIfGreater(a, arr):

    x = []

    for i in arr:
        if i < a:
            x.append(a)

        else:
            x.append(i)

    return x

print(replaceIfGreater(5, [2, 4, 6, 8]))

# Opg. 12

"Write a function that replaces the elements in the int array that is"
"greater than the int a and return the array."

"Print out the result of using the function."

def replaceIfLess(a, arr): # Google til at finde "else", resten blev løst selv.

    x = []

    for i in arr:
        if i > a:
            x.append(a)
            
        else:
            x.append(i)

    return x

print(replaceIfLess(5, [2, 4, 6, 8]))

# Opg. 13

"Write a function that takes the square (multiplies the number with itself)"
"of each element in the given int array and return the resulting array."

"Print out the result of using the function."

def squareList(arr):

    x = []

    for i in arr:
        x.append(pow(i, 2))

    return x

print(squareList([1, 2, 3, 4, 5]))

# Opg. 14

"Write a function that takes the square (multiplies the number with itself)"
"of each element in the given int array and adds a number a to each of"
"the squares. Return the resulting array."

"Print out the result of using the function."

def squareListAdd(a, arr):
    
    x = []

    for i in arr:
       x.append((i * i) + a) 

    return x

print(squareListAdd(1, [1, 2, 3, 4, 5]))

# Opg. 15

"Write a function that removes all the empty strings from a given list of"
"strings."

"Is called like this:"

"removeIfEmptyString(['hej', '', 'dav', '', 'whatever'])"
"result : ['hej', 'dav', 'whatever'']"

"Return the result."

"Print out the result of using the function."

def removeIfEmptyString(arr):

    #x = []

    #for i in arr:

     #   if i != '':
     #      x.append(i)

    #return x

    for idx, i in enumerate(arr):
        if i == '':
            arr.pop(idx)

    return arr

print(removeIfEmptyString(['hej', '', 'dav', '', 'whatever']))

# Opg. 16

"Write a function that removes all occurrences of arg1 in the given list."
"Return the result."

"removeArg1('hej', ['hej', 'dav'])"
"result : ['dav']"

"Print out the result of using the function."

def removeArg1(arg1, arr):

    x = []

    for i in arr:
        if i != arg1:
            x.append(i)

    return x

print(removeArg1('hej', ['hej', 'dav']))

# Opg. 17
"1 - sorteret array."
"2 - sorteret array for herefter at loope over det."

"Write a function that sorts a given array in descending order."
"Print every other element from the array."

def sortDescAndPrint1(arr):

    arr.sort(reverse=True)
    # arr.sort()
    # arr.reverse()
    print(arr)

sortDescAndPrint1([8, 9, 7, 4, 5])

def sortDescAndPrint2(arr):

    arr.sort()
    arr.reverse()

    for x in arr:
        print(x)

x = sortDescAndPrint2([3, 8, 1, 2, 5])

# Opg. 18

"Write a function that takes an arr that holds both strings and ints."

"Make two new arrays. One the holds all the strings from the original "
"array and another one that holds all the ints."

"Print both arrays from the function."

"And return them afterwards like this:"

"return [stringArray, intArray] "

def divideThem(arr):

    stringArray = []
    intArray = []

    for i in arr:

        if isinstance(i, int):
            intArray.append(i)

        if isinstance(i, str):
            stringArray.append(i)

    return [stringArray, intArray]

print(divideThem([10, "Hello", 20, "There"]))

        # [stringArray, intArray] er et array i et andet array, dvs. en 'nested' 
        # datastruktur. Dem skal vi arbejde mere med. Jeg vil gerne ha', at du 
        # reflekterer lidt om, hvordan man kan tilgå et array i et array.
        # ps. google er din ven.

        # Tilgå som ville du tilgå en dictionary INDE i en dictionay =
        # array = [10, [1, 2, 3]]
        # print(array[1][1])

# Opg. 19 - Nogenlunde

"Write a function that takes an arr that holds a number of ints."
"Find the greatest and the second greatest element and return them."

"You are not allowed to loop more than once over the array - i.e. only "
"one loop."

"return like an array -> [greatest, nextGreates"

"Print out the result of using the function."

def greatestAndNext(arr):

   # x = []


    # for (int i = 0; i < 10; i++)
    #    arr[i]

    greatest = 0
    nextGreatest = 0

    for idx, _ in enumerate(arr):
        if greatest < arr[idx]:
            greatest = arr[idx]
        elif nextGreatest < arr[idx]:
            nextGreatest = arr[idx]

    return [greatest, nextGreatest]

print(greatestAndNext([18, 54, 69, 24, 51, 35]))

# Opg. 20

"Write a function that counts the number of occurrences of a given int a."
"The function takes an int a and an int array."

"Return the result."

"Print out the result of using the function."

def whatever(a, arr):

    count = 0
    for i in arr:
        if i == a:
            count = count + 1

    return count

    # return arr.count(a)

print(whatever(3, [2, 3, 4, 3, 2]))

#____________________________________

# DOBBELT LOOP

print('')

darr = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

for x in darr:
    print(x)

for x in darr:
    for y in x:
        print(y)

print(darr[0][1])

for x in darr[1]:
    print(x)

print(darr[2])

for idx, x in enumerate(darr):
    for ydx, y in enumerate(x):
        print(idx, ydx)
        print(darr[idx][ydx])
        print('')