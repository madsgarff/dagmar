# -*- coding: utf-8 -*-

# Skriv en funktion, som..

# Tager et array som argument og printer alle arrayets 
# argumenter fra funktionen.

def arrayArgument(arg1):
    for x in arg1:
        print(x)

arrayArgument([1, 2, 3, 4])

            # - Tager et array som argument og returnerer det samme array.

def returnArr(arr):

    return arr

x = returnArr([1, 2, 3, 4])
print(x)
#__________________________________________________________________________

def hej():

    arr1 = [1,2,3]
    arr2 = [4,5,6]

    return arr1 + arr2

x = hej()

print(x) 
#__________________________________________________________________________

def farvel(arg1, arg2):

    return arg1 + arg2

y = farvel([1,2,3], [4,5,6])

print(y)

z = farvel("dav", "hej")

print(z)

#__________________________________________________________________________

            # - Tager et int array som argument og sorterer dets elementer 
            #   og printer hele arrayet til sidst fra funktionen.

def sortedArray(argument):

    argument.sort()

    print(argument)

sortedArray([9, 8, 7])

            # - Tager et int array som argument og sorterer dets elementer 
            #   og returnerer hele arrayet til sidst.

def sortedArr(argument1):

    argument1.sort()

    return argument1

x = sortedArr([3, 2, 1])
print(x)

            # - Tager et int array som argument og sorterer dets elementer
            #   og printer herefter hvert enkelt element i det sorterede 
            #   array fra funktionen. 

def elementsInSortedArray(argument2):

    argument2.sort()

    for i in argument2:
        print(i)

elementsInSortedArray([7,9,3,1])

            # - Tager et int array som argument og sorterer dets elementer
            #   og returnerer herefter det sorterede array. 

def intSort(argument):

    argument.sort()

    return argument

x = intSort([9, 8, 7, 6])
print(x)

            # - Tager et int array som argument og summerer alle dets 
            #   elementer og printer resultatet til sidst fra funktionen.

def sumArray(arr):

    x = 0

    for i in arr:
        x += i

    print(x)

sumArray([6, 5, 4, 3])
    
            # - Tager et int array som argument og summerer alle dets 
            #   elementer og returnerer resultatet til sidst.

def sumElements(arr):

    x = 0

    for i in arr:
        x += i

    return x

print(sumElements([9, 8, 7, 6, 5]))

            # - Finder på flere, hvis der er brug for det ... 
