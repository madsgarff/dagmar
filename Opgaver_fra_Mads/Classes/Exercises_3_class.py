import random
from random import randrange

class exer2:

    def __init__(self):
        pass

    def _1_multVar(self, a, b):

        """ 
            Write a function that takes two int variables, multiplies them
            and returns them.

            Print out the result of using the function.
        """

        print(a * b)

    def _2_multInt_1(self, element, arr):

        """
            Write a function that takes an int and an array. Multiply 
            every single element of the array and return it.

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            x.append(element * i)

        print(x)

    def _2_multInt_2(self, element, arr):

        # With Enumerate

        for idx, _ in enumerate(arr):
            arr[idx] = arr[idx] * element

        print(arr)

    def _3_combineStrings(self, str1, str2):

        """
            Write a function that concatenates two strings and return
            the resulting string.

            Print out the result of using the function.
        """ 

        print(str1 + str2)

    def _4_multCombineString(self, str1, arr):

        """
            Use the previous written function combineStrings(str1, str2) to
            create a string from the two arguments str1 and str2. Pass the 
            resulting string to this function, multCombineString(str, arr), 
            as the first argument. 
    
            The second argument is an array of ints [1,2,3,4,5]. 

            Pass the first and the second argument to the function and multiply 
            every single element in the array and put the result in a new array.

            Return the new array.

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            x.append((i * str1))
        
        print(x)

    def _5_greatest_1(self, a, b):

        """
            Return the greatest number of the two ints. 
            You are not allowed to use the built-in function max().
            Print out the result of using the function.
        """        

        x = True

        if a < b:
            x = False

        print(x)

    def _5_greatest_2(self, a, b):

        if a > b:
            print(a)

        if a < b:
            print(b)

    def _5_greatest_3(self, a, b):

        if a > b:
            print(b)

        else:
            print(a)

    def _5_greatest_4(self, a, b):
        
        print(a > b)

    def _6_isElementInArray(self, arr, element):

        """
            Returns True if the element can be found in the list. Otherwise
            return false.
            Print the result to the terminal outside of the function.
        """

        x = True

        for i in arr:
            if element not in arr:
                x = False

        print(x)

    def _7_isElementFound(self, arr, element):

        """
            Remove the element from the list if it can be found and return it.
            Otherwise return False. 

            Print the result to the terminal outside of the function.
        """

        if element in arr:
            arr.remove(element)

        else:
            return False

        print(element)

    def _8_printNestedList(self, arr):

        """
            Print each list from the nested array to the terminal:

            arr = [[1,2,3], [4,5,6], [7,8,9]]
            Like this:

            [1,2,3]
            [4,5,6]
            [7,8,9]
        """

        for i in arr:
            print(i)

    def _9_printFirstItem(self, arr):

        """
            Print the first item of each of the arrays in the nested array
            to the terminal.

            arr = [[1,2,3], [4,5,6], [7,8,9]]

            Like this:

            1
            4
            7
        """

        for i in arr:
            print(i[0])

    def _10_removeEleAndPrintArray(self, arr):

        """
            Remove the second array of the nested arrays.

            arr = [[1,2,3], [4,5,6], [7,8,9]]

            Print the result to the terminal outside of the function.

            Like this:

            [[1,2,3], [7,8,9]] 
        """

        del arr[1]

        print(arr)

    def _11_removeFirstEleAndReturnArray(self, arr):

        """
            Remove each of the first items of the nested arrays.

            arr = [[1,2,3], [4,5,6], [7,8,9]]

            Print the result to the terminal outside of the function.

            Like this:

            [[2,3], [5,6], [8,9]]
        """

        for i in arr:
            del i[0]

        print(arr)

    def _12_multiEachElementInArray(self, arr, ele):

        """
            Multi each of the elements of the nested array with an interger
            i.e. the element given.

            Print the result to the terminal outside of the function.
        """

        for i in arr:
            for idx, j in enumerate(i):
                i[idx] = j * ele

        print(arr)

    def _13_printPattern_1(self):

        """
            Print this pattern to the terminal.

            #   *
            #   **
            #   ***
            #   ****
        """

        mango = ""

        for i in range(4):
            for j in range(i + 1):
                mango = mango + "*"
            mango = mango + "\n"   
        
        print(mango)

    def _13_printPattern_2(self):

        for i in range(1, 5):
            print("*" * i)

    def _14_printOddPattern(self):

        """
            Print this pattern to the terminal.

            *
            ***
            *****
            *******

            Hint: Print only odd numbers.
        """

        for i in range(7):
            print("")
            for j in range((i * 2) + 1):
                print("*"),

    def _15_printLetterPattern(self):

        """
            Print this pattern to the terminal.

            A
            B C
            D E F
            G H I J
            K L M N O

            Hint: Look through pythons standard library (yes you can use it)
                and find a function that changes integers into strings.

            (maybe hard)
        """

        count = 65

        for i in range(1,5):
            mango = ""
            for j in range(i):
                mango = mango + " " + chr(count)
                count += 1

            print(mango)

    def _17_makeNested5s(self, arg1):

        """
            Create a nested array and fill each of the 5 inner arrays with 5's.
    
            Print the result to the terminal outside of the function.

            [[5,5,5,5,5], [5,5,5,5,5], [5,5,5,5,5], [5,5,5,5,5], [5,5,5,5,5]]
        """

        arr = []

        for i in arg1:
            for idx, j in enumerate(i):
                if j != 5:
                    i[idx] = 5
        
            arr.append(i)
        
        print(arr)

    def _18_makeNestedWithRandom(self):

        """
            Create a nested array and fill each of the 5 inner arrays with 
            random numbers created using pythons build-in library 'random'.

            Hint 1: You can get a random integer between 1-1000 with 
                randomNumber = randrange(1,1000)

            Hint 2: from random import randrange
        """

        arr = []

        for _ in range(5):
            arr.append([random.randint(0,1000)])

        array = []
        count = 0

        while count < 5:
            count += 1
            array.append([random.randint(0,1000)])

        print(array)

    def _19_mango(self, ele):

        """
            Create a nested array and fill each of the 5 inner arrays with 
            random numbers created using pythons build-in library 'random'.

            Check if any of the created element is divisible (can be divided)
            by the element that is given as the argument to the function.

            If so return the element otherwise return False.
        """

        arr = []

        for _ in range(100):
            arr.append([random.randint(0,1000)])

        for i in arr:
            for j in i:
                if j % ele == 0:
                    print(ele)

        print(False)

    def _20_checkEleAgainstRandom(self, arg1, arg2):

        """
            Create a nested array and fill each of the 5 inner arrays with 
            random numbers created using pythons build-in library 'random'.

            Check if any of the created element is divisible (can be divided)
            by both the elements that is given as the argument to the function.

            If so return the smallest of the arguments otherwise return False.
        """ 

        arr = []

        for _ in range(5):
            arr.append([random.randint(0,1000)])
        
        print(arr)

        for i in arr:
            for j in i:
                if (j % arg1 == 0) and (j % arg2 == 0):
                    print(arg1, arg2)


        print(False)

if __name__ == "__main__":

    runExercises = exer2()
    print(40*"-")
    runExercises._1_multVar(2, 3)
    print(40*"-")
    runExercises._2_multInt_1(5, [1, 2, 3])
    print(40*"-")
    runExercises._2_multInt_2(2, [10, 11, 12])
    print(40*"-")
    runExercises._3_combineStrings("Win", "ter")
    print(40*"-")
    runExercises._4_multCombineString("Winter", [1, 2, 3, 4, 5])
    print(40*"-")
    runExercises._5_greatest_1(10, 20)
    print(40*"-")
    runExercises._5_greatest_2(10, 15)
    print(40*"-")
    runExercises._5_greatest_3(2, 3)
    print(40*"-")
    runExercises._5_greatest_4(3, 2)
    print(40*"-")
    runExercises._6_isElementInArray([1, 2, 3], 4)
    print(40*"-")
    runExercises._7_isElementFound([10, 20, 30], 10)
    print(40*"-")
    runExercises._8_printNestedList([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    print(40*"-")
    runExercises._9_printFirstItem([[1,2,3], [4,5,6], [7,8,9]])
    print(40*"-")
    runExercises._10_removeEleAndPrintArray([[1,2,3], [4,5,6], [7,8,9]])
    print(40*"-")
    runExercises._11_removeFirstEleAndReturnArray([[1,2,3], [4,5,6], [7,8,9]])
    print(40*"-")
    runExercises._12_multiEachElementInArray([[1, 2], [3, 4], [5, 6]], 2)
    print(40*"-")
    runExercises._13_printPattern_1()
    print(40*"-")
    runExercises._13_printPattern_2()
    print(40*"-")
    # runExercises._14_printOddPattern()
    print(40*"-")
    runExercises._15_printLetterPattern()
    print(40*"-")
    runExercises._17_makeNested5s([[1, 2, 3, 4, 5], [2, 3, 4, 5, 6], [3, 4, 5, 6, 7], [4, 5, 6, 7, 8], [5, 6, 7, 8, 9]])
    print(40*"-")
    runExercises._18_makeNestedWithRandom()
    print(40*"-")
    runExercises._19_mango(97)
    print(40*"-")
    runExercises._20_checkEleAgainstRandom(2, 17)
    print(40*"-")