class exer1:

    def __init__(self):
        pass

    def _1_multVars(self, a, b):

        """
            Write a function that takes two int variables, multiplies them
            and returns them.
            Print out the result of using the function.
        """

        x = a * b
        print(x)

    def _2_multArrayWithInt_1(self, a, arr):

        """
            Write a function that takes an int and an array. Multiply
            every single element of the array and return it.
            Print out the result of using the function.
        """

        x = []

        for i in arr:
            x.append(i * a)

        print(x)

    def _2_multArrayWithInt_2(self, a, arr):

        """
            Write a function that takes an int and an array. Multiply
            every single element of the array and return it.
            Print out the result of using the function.
        """

        for idx, i in enumerate(arr):
            arr[idx] = arr[idx] * a

        print(arr)

    def _3_combineStrings(self, str1, str2):

        """
            Write a function that concatenates two strings and return
            the resulting string.
            Print out the result of using the function.
        """

        x = str1 + str2
        print(x)

    def _3_add(self, x, y):
        
        """
            Write a function that concatenates two ints and return
            the result.
            Print out the result of using the function.
        """

        result = x + y
        print(result)

    def _4_multCombineString(self, str1, arr):

        """
            Use the previous written function combineStrings(str1, str2) to
            create a string from the two arguments str1 and str2. Pass the 
            resulting string to this function, multCombineString(str, arr), 
            as the first argument.
        
            The second argument is an array of ints [1,2,3,4,5].

            Pass the first and the second argument to the function and multiply 
            every single element in the array and put the result in a new array.

            Return the new array.

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            x.append(i * str1)

        print(x)     

    def _5_greater_1(self, a, b):

        """
            Return the greatest number of the two ints.
            You are not allowed to use the built-in function max().
            Print out the result of using the function.
        """

        if a > b:
            print(a)

        else:
            print(b)

    def _5_greater_2(self, a, b):

        """
            Return True, if a is smaller than b. Otherwise, return False.
        """

        x = True

        if a < b:
            x = False

        print(x)

    def _6_smaller(self, a, b):
        
        """
            Return the smallest number of the two ints.
            You are not allowed to use the built-in function min().
            Print out the result of using the function.
        """

        if a < b:
            print(a)

        else:
            print(b)

    def _7_isGreater(self, a, b):

        """
            Return True if a is greater than b otherwise return 
            False.
            Print out the result of using the function.
        """

        x = True

        if a > b:
            x = True
    
        else:
            x = False

        print(x)

    def _8_isSmaller(self, a, b):
      
        """
            Return True if a is smaller than b otherwise return
            False.
            Print out the result of using the function.
        """  
        
        x = True

        if a < b:
            x = True

        else:
            x = False

        print(x)

    def _9_isNSmallerThanArray(self, n, arr):

        """
            Return True if n is smaller than the length of the array.
            Otherwise return False.
            Print out the result of using the function.
        """

        print(n < len(arr))

    def _10_nSmaller(self, n, arr):

        """
            Return True if int n is smaller than all the elements in the"
            int array - else return False.

            Function is called like this:

            nSmaller(10, [10,12,5,2,4,5])

            Print out the result of using the function.
        """  
        x = True

        for i in arr:
            if n < i in arr:
                x = True
    
            else:
                x = False
    
        print(x)

    def _11_replaceElements_1(self, a, arr):

        """
            Write a function that replaces all the elements in the array arr
            with the element a and return the array.

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            if i != a:
                x.append(a)

        print(x)

    def _11_replaceElements_2(self, a, string):

        """
            Write a function that replaces all the elements in the array arr
            with the element a and return the array.

            Print out the result of using the function.

            # Hint: Does this function only work for strings? Why/why not? Test it.
        """

        x = []

        for i in string:
            if i != a:
                x.append(a)

        print(x)        

    def _12_replaceIfGreater(self, a, arr):

        """
            Write a function that replaces the elements in the int array that is
            less than the int a and return the array.

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            if i < a:
                x.append(a)

            else:
                x.append(i)

        print(x)

    def _13_replaceIfLess(self, a, arr):

        """
            Write a function that replaces the elements in the int array that is
            greater than the int a and return the array.

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            if i > a:
                x.append(a)
            
            else:
                x.append(i)

        print(x)

    def _14_squareList(self, arr):

        """
            Write a function that takes the square (multiplies the number 
            with itself) of each element in the given int array and return 
            the resulting array."

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            x.append(pow(i, 2))

        print(x)

    def _15_squareListAdd(self, a, arr):

        """
            Write a function that takes the square (multiplies the number with 
            itself) of each element in the given int array and adds a number a 
            to each of the squares. Return the resulting array."

            Print out the result of using the function.
        """

        x = []

        for i in arr:
            x.append((i * i) + a) 

        print(x)

    def _16_removeIfEmptyString(self, arr):

        """
            Write a function that removes all the empty strings from a given list 
            of strings.

            Is called like this:

            removeIfEmptyString(['hej', '', 'dav', '', 'whatever'])
            result : ['hej', 'dav', 'whatever'']

            Return the result.

            Print out the result of using the function.
        """

        for idx, i in enumerate(arr):
            if i == '':
                arr.pop(idx)

        print(arr)

    def _17_removeArg1(self, arg1, arr):

        """
            Write a function that removes all occurrences of arg1 in the given list.
            Return the result.

            removeArg1('hej', ['hej', 'dav'])
            result : ['dav']

            Print out the result of using the function.
        """
        x = []

        for i in arr:
            if i != arg1:
                x.append(i)                    

        print(x)

    def _18_sortDescAndPrint1(self, arr):
    
        """
            Write a function that sorts a given array in descending order."
            Print every other element from the array.
        """
     
        arr.sort(reverse=True)   
        print(arr)

    def _18_sortDescAndPrint2(self, arr):

        # Loopet:

        arr.sort()
        arr.reverse()

        for x in arr:
            print(x)

    def _18_sortDescAndPrint3(self, arr):

        # Loopet med enumerate - printet hver andet element loopet.

        arr.sort(reverse=True)

        for idx, i in enumerate(arr):
            if idx % 2 == 0:   
                print(i)
            
    def _18_sortDescAndPrint4(self, arr):

        # Loopet med enumerate - printet hver andet element i array.

        x = []

        for idx, i in enumerate(arr):
            if idx % 2 == 0:
                x.append(i)

        x.reverse()
        print(x)

    def _18_sortDescAndPrint5(self, arr):

        # Delt simpelt.

        arr.sort()
        arr.reverse()

        print(arr)

    def _19_divideThem(self, arr):

        """
            Write a function that takes an arr that holds both strings and ints.

            Make two new arrays. One the holds all the strings from the original
            array and another one that holds all the ints.

            Print both arrays from the function.

            And return them afterwards like this:

            return [stringArray, intArray].
        """

        stringArray = []
        intArray = []

        for i in arr:

            if isinstance(i, int):
                intArray.append(i)

            if isinstance(i, str):
                stringArray.append(i)

        print([stringArray, intArray])

        # [stringArray, intArray] er et array i et andet array, dvs. en 'nested' 
        # datastruktur. Dem skal vi arbejde mere med. Jeg vil gerne ha', at du 
        # reflekterer lidt om, hvordan man kan tilgå et array i et array.
        # ps. google er din ven.

        # Tilgå som ville du tilgå en dictionary INDE i en dictionay =
        # array = [10, [1, 2, 3]]
        # print(array[1][1])

    def _20_greatestAndNext(self, arr):

        """
            Write a function that takes an arr that holds a number of ints."
            Find the greatest and the second greatest element and return them."

            You are not allowed to loop more than once over the array - i.e. only "
            one loop."

            return like an array -> [greatest, nextGreates"

            Print out the result of using the function.
        """

        greatest = 0
        nextGreatest = 0

        for idx, _ in enumerate(arr):
            if greatest < arr[idx]:
                greatest = arr[idx]
            elif nextGreatest < arr[idx]:
                nextGreatest = arr[idx]

        print([greatest, nextGreatest])

    def _21_occurrence(self, a, arr):

        """
            Write a function that counts the number of occurrences of a given 
            int a. The function takes an int a and an int array.

            Return the result.

            Print out the result of using the function.
        """

        count = 0
        
        for i in arr:
            if i == a:
                count = count + 1

        print(count)        

if __name__ == "__main__":

    runExercises = exer1()
    print(40*"-")
    runExercises._1_multVars(2, 3)
    print(40*"-")
    runExercises._2_multArrayWithInt_1(2, [10, 20, 30])
    print(40*"-")
    runExercises._2_multArrayWithInt_2(3, [10, 20, 30])
    print(40*"-")
    runExercises._3_combineStrings("He", "llo")
    print(40*"-")
    runExercises._3_add(10, 15)
    print(40*"-")
    runExercises._4_multCombineString("hello", [1, 2, 3])
    print(40*"-")
    runExercises._5_greater_1(2, 6)
    print(40*"-")
    runExercises._5_greater_2(10, 20)
    print(40*"-")
    runExercises._6_smaller(15, 10)
    print(40*"-")
    runExercises._7_isGreater(2, 3)
    print(40*"-")
    runExercises._8_isSmaller(2, 3)
    print(40*"-")
    runExercises._9_isNSmallerThanArray(5, [1, 2, 3])
    print(40*"-")
    runExercises._10_nSmaller(1, [10, 12, 5, 2, 4, 5])
    print(40*"-")
    runExercises._11_replaceElements_1(10, [1, 2, 3, 4, 5])
    print(40*"-")
    runExercises._11_replaceElements_2("T", "Hello")
    print(40*"-")
    runExercises._12_replaceIfGreater(5, [2, 4, 6, 8])
    print(40*"-")
    runExercises._13_replaceIfLess(5, [2, 4, 6, 8])
    print(40*"-")
    runExercises._14_squareList([1, 2, 3, 4, 5])
    print(40*"-")
    runExercises._15_squareListAdd(1, [1, 2, 3, 4, 5])
    print(40*"-")
    runExercises._16_removeIfEmptyString(['hej', '', 'dav', '', 'whatever'])
    print(40*"-")
    runExercises._17_removeArg1('hej', ['hej', 'dav'])
    print(40*"-")
    runExercises._18_sortDescAndPrint1([8, 9, 7, 4, 5])
    print(40*"-")
    runExercises._18_sortDescAndPrint2([3, 7, 8, 1, 4, 6])
    print(40*"-")
    runExercises._18_sortDescAndPrint3([6, 11, 42, 99, 63, 2])
    print(40*"-")
    runExercises._18_sortDescAndPrint4([6, 11, 42, 99, 63, 2])
    print(40*"-")
    runExercises._18_sortDescAndPrint5([9, 8, 7, 6, 5, 4, 3, 2, 1])
    print(40*"-")
    runExercises._19_divideThem([10, "Hello", 20, "There"])
    print(40*"-")
    runExercises._20_greatestAndNext([18, 54, 69, 24, 51, 35])
    print(40*"-")
    runExercises._21_occurrence(3, [2, 3, 4, 3, 2])

