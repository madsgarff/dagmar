class exer6:
    
    def __init__(self):
        pass

    def ex1(self, arr):

        """
            Lav en funktion, som printer alt fra en liste - husk enumerate !
            Brug idx til at index in i listen.
        """

        for idx, i in enumerate(arr):
            print(idx, i)

    def ex1_eksperiment(self, arr):

        for idx, i in enumerate(arr, start = 42):
            print(idx, i)

    def ex2(self, string):

        """
            Lav en funktion, som printer alt fra en liste (string, right?) - husk enumerate !
            Brug idx til at index in i stringen.
        """

        for idx, i in enumerate(string):
            print(idx, i)

    def ex3(self, string):

        """
            Lav en funktion, som tager en string som input og printer 
            hvert andet element til terminalen.
        """

        for idx, i in enumerate(string):
            if idx % 2 == 0:
                print(idx, i)

        # Har også printet indexplads.

    def ex4(self, array): # her

        """
            Lav en funktion, som tager en liste som input, hvor listens elementer 
            består af strings - print hvert andet bogstav i alle strings.
        """

        for i in array:
            for j in i:
                print(i[::2])
                break

        print("")

        for i in array:
            for idx, j in enumerate(i):
                print(i[::2])
                break

        print("")

        # Har også printet indexplads for det enkelte element i hver liste.

        for i in array:
            for idx, j in enumerate(i):
                if idx % 2 == 0:
                    print(idx, j)

    def ex5(self, string): # Fik hjælp

        """
            Lav en funktion, som tager en string som input.
            Funktionen skal dele stringen i midten og indsætte et mellemrum, fx;

            str1 = "Mads" 

            str2 = "Ma ds"

            Herefter skal de to dele omvendes, så "Mads" i sidste ende bliver til;

            str3 = "ds Ma"

            Og den sidste returneres.
        """

        halfString = len(string)//2

        empty1 = ""
        empty2 = ""

        for idx, i in enumerate(string):
            if idx < halfString:
                empty2 += i

            if idx >= halfString:
                empty1 += i

        result = empty1 + " " + empty2

        print(result)

        # Ovenstående = hjælp fra Simon. Denne vil gælde for alle strings.
        # Nedestående = lavet selv. Denne gælder for "Mads".

        print("")

        firstChars = ""
        secondChars = ""

        for idx, i in enumerate(string):
            if idx >= 2:
                firstChars += i

            if idx < 2:
                secondChars += i

        final = firstChars + " " + secondChars
        print(final)

    def ex6(self, string):

        """
            Lav en funktion, som tager en string som input.
            Funktionen skal fjerne alle elementer, som står på et ulige input, dvs;

            str1 = "HejDagmar"

            Bliver til

            str2 = "Hjamr"

            Og skal returneres.
        """

        for idx, i in enumerate(string):
            if idx % 2 == 0:
                print(i)

        print("")

        empty = ""

        for idx, i in enumerate(string):
            if idx % 2 == 0:
                empty += string[idx]

        print(empty)

    def ex7(self, array):

        """
            Lav en funktion, som tager en liste af ints og summer dem.. huske at du skal bruge
            enumerate og idx til at summere med!
        """

        for idx, i in enumerate(array):
            result1 = i + i

        print(result1)

        count = 0

        for idx, i in enumerate(array):
            count += i

        print(count)

        count2 = 0

        for idx, i in enumerate(array):
            count2 += array[idx]

        print(count2)

    def ex8(self, array):

        """
            Lav en funktion, som tager en liste af tuples, dvs.

            w = [(a,b), (c,d)]

            af strings..

            print alle strings i alle tuples vha. enumerate og tilhørende idx. 
        """

        for i in array:
            print(i)

        print("")

        for idx, i in enumerate(array):
            for ydx, y in enumerate(i):
                print(y)

        print("")

        for idx, i in enumerate(array):
            print(idx, i)

        print("")
        print("Hvad resultatet legit skulle være:")
        print("")

        for idx, i in enumerate(array):
            for ydx, y in enumerate(i):
                print(idx, ydx, y)

    def ex9(self, strings): 

        """
            Lav en funktion, som tager en liste af strings - loop over de strings
            med enumerate - hvis en string indeholder bogstavet "A" eller "a", så 
            skal der fjernes.

            Printen den færdige liste til sidst.
        """

        # Dobbelt loop:

        for idx, i in enumerate(strings):
            for y in i:
                if y is "A":
                    strings[idx] = i.replace("A", "")
                elif y is "a":
                    strings[idx] = i.replace("a", "")

        print(strings)

        # Enkelt loop:

        for idx, i in enumerate(strings):
            if "A" in i:
                strings[idx] = i.replace("A", "")
            elif "a" in i:
                strings[idx] = i.replace("a", "")

        print(strings)

    def ex10(self, array):

        """
            Lav en funktion, som tager en liste af lists af strings, dvs.

            w = [["hej", "med", "dig"], ["hej", "med", "dig"], ["hej", "med", "dig"]]

            Loop over dem med enumerate og print alle elementerne.

            ## Hint: Double loops 
        """

        for idx, i in enumerate(array):
            for ydx, y in enumerate(i):
                print(idx, ydx, y)

    def ex11(self, array): # her

        """
            Lav en funktion, som tager en liste af lists af strings, dvs.

            w = [["hej", "med", "dig"], ["hej", "med", "dig"], ["hej", "med", "dig"]]

            Erstat alle "h" med "a"
        """

        # Trippel loop:

        for idx, i in enumerate(array):
            for ydx, y in enumerate(i):
                for jdx, j in enumerate(y):
                    if j is "h":
                        i[ydx] = y.replace("h", "a")

        print(array)

        # Dobbelt loop:

        for idx, i in enumerate(array):
            for ydx, y in enumerate(i):
                if "h" in y:
                    i[ydx] = y.replace("h", "a")

        print(array)

    def ex12(self, array): # her



        """
            Lav en funktion, som tager en liste af strings. 

            Sammensæt en ny string af hver tredje element i listen.

            Brug enumerate!!!!!!!!!!
        """

        empty1 = ""

        for i in array:
            for ydx, y in enumerate(i):
                if ydx == 2:
                    empty1 += y

        print(empty1)

        empty2 = ""

        for idx, i in enumerate(array):
            if idx % 3 == 0:
                empty2 += i

        print(empty2)

        empty3 = ""

        for idx, i in enumerate(array, start = 4):
            if idx % 3 == 0:
                empty3 += i
        
        print(empty3)

        empty4 = ""

        for idx, i in enumerate(array, start= 1):
            if idx % 3 == 0:
                empty4 += i

        print(empty4)

        empty5 = ""

        for idx, i in enumerate(array, start= 7):
            if idx % 3 == 0:
                empty5 += i

        print(empty5)

        empty6 = ""

        for idx, i in enumerate(array, start= 10):
            if idx % 3 == 0:
                empty6 += i

        print(empty6)

        empty7 = ""

        for idx, i in enumerate(array):
            if (idx + 1) % 3 == 0:
                empty7 += i

        print(empty7)

        empty8 = ""

        for idx, i in enumerate(array):
            if (idx + 4) % 3 == 0:
                empty8 += i

        print(empty8)
        
if __name__ == "__main__":

    runExercises = exer6()
    print(20*"-")
    runExercises.ex1([1, 2, 3, 4])
    print(20*"-")
    runExercises.ex1_eksperiment([1, 2, 3, 4])
    print(20*"-")
    runExercises.ex2("Exercise")
    print(20*"-")
    runExercises.ex3("Exercise")
    print(20*"-")
    runExercises.ex4(["Archer", "Lana", "Cheryl", "Krieger"])
    print(20*"-")
    runExercises.ex5("Mads")
    print(20*"-")
    runExercises.ex6("HejDagmar")
    print(20*"-")
    runExercises.ex7([1, 2, 3])
    print(20*"-")
    runExercises.ex8([("Archer", "Lana"), ("Pam", "Carol"), ("Cyril", "Ray")])
    print(20*"-")
    runExercises.ex9(["Archer", "Lana", "Cheryl"])
    print(20*"-")
    runExercises.ex10([["Malory", "Slater", "Lana"], ["Ray", "Cyril", "Pam"], ["Carol", "Sterling", "Krieger"]])
    print(20*"-")
    runExercises.ex11([["hej", "med", "dig"], ["hej", "med", "dig"], ["hej", "med", "dig"]])
    print(20*"-")
    runExercises.ex12(["Lana", "Archer", "Malory", "Cheryl", "Pam", "Cyril"])
    print(20*"-")