class exer7_1:

    def __init__(self):
        pass

    def ex1(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og 
            returner (med et print for at bekræfte at der faktisk sker noget) 
            antallet af linjer i filen.
        """

        with open('text.txt', 'r') as f:
            print(f.read())

        print(40*"-")

        # Linjemæssigt er der noget, som ikke stemmer overens med, hvad der 
        # står i filen "text.txt", terminalen og termialens svar på, hvor mange 
        # linjer der er.

        f = open("text.txt", "r")
        line_count = 0
        for line in f:
            if line != "\n":
                line_count += 1

        print("In the story of Moby dick, there are", line_count, "lines.")

        f.close()

    def ex2(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            ord i filen.
        """

        f = open("text.txt", "r")

        word_count = 0

        for i in f:
            word_count += 1

        print("There are", word_count, "words in the story of Moby Dick.")

        f.close()

    def ex3(self): #Tjek

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            bogstaver i filen.
        """

        f = open("text.txt", "r")

        letter_count1 = 0

        for i in f:
            for j in i:
                letter_count1 += 1

        print("There are", letter_count1, "letters in the story of Moby Dick.")

        f.close()

    def ex4(self): # Tjek

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            mellemrum i filen.
        """ 

        f = open("text.txt", "r")

        contents = f.read()
        counter = contents.count(" ")

        # count = 0

        # for i in f:
        #     for j in i:
        #         for k in j:
        #             if k == " "
        #             count += 1

        # print(count)
        

        print("A space accours", counter, "times in the story of Moby Dick.")

        f.close()

    def ex5(self):

        # Det skal nævnes, at denne opgave også er lavet i opgave 11 (en anden class. Her er
        # resultatet anderledes.)

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            vokaler i filen.
        """

        vowels = ["a", "e", "i", "o", "u"]
        found = {}

        found["a"] = 0
        found["e"] = 0
        found["i"] = 0
        found["o"] = 0
        found["u"] = 0

        f = open("text.txt", "r")

        for letter in f:
            for vowel in letter:
                if vowel in vowels:
                    found[vowel] += 1

        print("In total, there are", sum(found.values()), "vowels in the story"
        "of Moby Dick")

        f.close()

    def ex6(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            linjer i filen, som ikke begynder med 'I'.
        """

        f = open("text.txt", "r")

        count = 0

        for i in f.readlines():
            if not i.startswith("I"):
                count += 1

        print(count, "lines does not start with 'I' in the story of Moby Dick.")

        f.close() 

    def ex7(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            ordet 'the' i filen.
        """

        f = open("text.txt", "r")

        contents = f.read()
        counter = contents.count("the")

        print("The letters 'the' accours", counter, "times in the story of Moby Dick.")

        f.close()

        f = open("text.txt", "r")

        count = 0
        word = "the"
        for i in f:
            if word in i.split():
                count += 1

        print("The word 'the' accours", count, "times in the story of Moby Dick.")
        f.close() 

        # Forskellen på de to ovenstående er, at den øverste finder hvor mange gange
        # "the" står i teksten. Metoden .split() skaber en liste, og finder herefter
        # ud af, hvor mange gange "the" står i listen. Af den grund er det kode nummer
        # 2 der er korrekt for denne opgave.

    def ex8(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Print de ord fra 
            filen, som har < 5 bogstaver, dvs. 'yes' skal printes 'whatever' skal ikke. 
        """

        f = open("text.txt", "r")

        k = str(f.read(500))
        v = k.split()
        x = []

        for i in v:
            if len(i) < 5:
                x.append(i)

        print(x)

        f.close() 

    def ex9(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            ord, som ender på bogstavet 'e' i filen.
        """

        f = open("text.txt", "r")

        countForE = {'e': 0}

        for i in f:
            for j in i:
                if j.endswith('e'):
                    countForE['e'] += 1

        for key, value in countForE.items():
            print("Words in the story of Moby Dick that ends with", key, "occurs", 
            value, "times.")

        f.close()

    def ex10(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Print de sidste 10
            linjer af filen.
        """

        f = open("text.txt", "r")

        lines = f.readlines()
        last_10_lines = lines[-10:]

        print(last_10_lines)

        f.close() 

if __name__ == "__main__":

    runExercises = exer7_1()
    print(40*"-")
    runExercises.ex1()
    print(40*"-")
    runExercises.ex2()
    print(40*"-")
    runExercises.ex3()
    print(40*"-")
    runExercises.ex4()
    print(40*"-")
    runExercises.ex5()
    print(40*"-")
    runExercises.ex6()
    print(40*"-")
    runExercises.ex7()
    print(40*"-")
    runExercises.ex8()
    print(40*"-")
    runExercises.ex9()
    print(40*"-")
    runExercises.ex10()

txt = "For only {price:.2f} dollars!"
print(txt.format(price = 49))