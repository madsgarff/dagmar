import random
from collections import Counter
import json
import ast

class exer7_2:

    def __init__(self):
        pass

    def ex11_1(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og 
            returner (med et print for at bekræfte at der faktisk sker noget) 
            antallet af linjer i filen.
        """

        with open("text.txt", "r") as f:

            line_count = 0
            for line in f:
                if line != "\n":
                    line_count += 1

            print("There are", line_count, "lines in the story of Moby Dick.")

    def ex11_2(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            ord i filen.
        """

        word_count = 0

        with open("text.txt", "r") as f:
            
            for i in f:
                word_count += 1

            print("There are", word_count, "words in the story of Moby Dick.")        

    def ex11_3(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            bogstaver i filen.
        """

        letter_count2 = 0

        with open("text.txt", "r") as f:
            for i in f:
                for j in i:
                    letter_count2 += 1

            print("There are", letter_count2, "letters in the story of Moby Dick.")

    def ex11_5(self):

        # Det skal nævnes, at denne opgave også er lavet i opgave 11 (en anden class. Her er
        # resultatet anderledes.)

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            vokaler i filen.
        """

        vowels = ["a", "e", "i", "o", "u"]
        found = {}

        found["a"] = 0
        found["e"] = 0
        found["i"] = 0
        found["o"] = 0
        found["u"] = 0

        with open("text.txt", "r") as f:

            for letter in f:
                for vowel in letter:
                    if vowel in vowels:
                        found[vowel] += 1

            for k, v in sorted(found.items()):
                print(k, "was found", v, "times")

            # for lettert in fo:
            #     for vowel in letter:
            #         if vowel in found.keys():
            #             found[vowel] += 1

            # Vi finder i denne kode antallet af alle vokaler på to måder.
            # 1) Forloop

            total = 0

            for k in vowels:
                total += found[k]

            print("")
            print("In total, there are", total, "vowels in the story of Moby Dick. "
            "We've used a forloop to solve this problem.")
            print("")

            # 2) Indbygget function "sum" og ".values()"

            total = sum(found.values())

            print("In total, there are", sum(found.values()), "vowels in the story of"
            " Moby dick. This code uses built-in methods.")

    def ex11_6(self):

        """
            Lav en funktion, som åbner filen 'text.txt' med 'open'. Tæl og returner 
            (med et print for at bekræfte at der faktisk sker noget) antallet af 
            linjer i filen, som ikke begynder med 'I'.
        """

        # Resultatet er anderledes end i den originale opgave 5, da jeg dengang tjekkede
        # for "I", og denne gang tjekker jeg for "i". Jeg gør præcist det samme, og får de
        # samme resultater begge steder.  

        with open("text.txt", "r") as f:

            count = 0

            for i in f.readlines():
                if not i.startswith("i") or not i.startswith("I"):
                    count += 1

            print(count, "lines does not start with 'i' in the story of Moby Dick.")

    def ex12(self):

        """
            Lav en funktion, som opretter en ny fil kaldet 'ex12.txt'. Læs de første 
            10 linjer fra 'text.txt' og skriv dem til 'ex12.txt'. 

            Hint: Åben begge filer, dvs. to fil objekter. Det her er desuden også 
            et eksempel på, at det kan give mening at blande de to syntax'er - 
            'with open'/'open'.
        """        

        # f = open("text.txt", "r") # Vi skal huske, at denne fil skal være åben, før
        #                           # vi kan arbejde med den i forhold til en anden fil.

        # #print(f.readlines()[:9]) # Her printer vi de første 10 linjer til terminalen.

        # with open("ex12.txt", "w+") as f1: 
        #     f1.writelines(f.readlines()[:10])

        # Når jeg kører denne, så bliver der lavet en ny txt. fil, som hedder "ex12". 
        # Den ligger til venstre sammen med de andre mapper/filer.
        # Vi skriver "w+" for at "write and read". Hver gang vi skaber en ny txt. fil,
        # skriver vi "w" eller "w+". Findes filen ikke allerede, så skaber python 
        # automatisk en ny.

        # f.close()

        with open("ex12.txt", "w+") as f1:
            f = open("text.txt", "r")

            f1.writelines(f.readlines()[:10])

            f.close()

    def ex13(self):

        """ 
            Lav en funktion, som læser de 10 linjer, med 'open with', fra 
            'ex12.txt'. Erstat alle 'i' med 'j' og skriv resultatet til filen. 
            Print indholdet af filen.
        """

        # lines = []
        # with open("ex12.txt", "r") as f1:
        #     for line in f1.readlines():
        #         lines.append(line)

        # for idx, line in enumerate(lines):
        #     lines[idx] = line.replace("i", "j")

        # with open("test.txt", "w") as f2:
        #     for line in lines:
        #         f2.write(line)

        with open("ex12.txt", "r") as f1: 
            with open("test.txt", "w") as f2: 
                for line in f1.readlines():
                    f2.write(line.replace("i", "j"))

    def ex14(self):

        """
            Lav en funktion, som læser en 'random linje' fra filen 'ex12.txt'. Opret 
            en ny fil 'ex14.txt' og skriv den 'random linje' til den nye fil. 
            Print indholdet af filen.
        """

        import random

        random = random.choice(list(open("ex12.txt")))
        print(random)

        # print(list(open("ex12.txt")))

        with open("ex14.txt", "w") as f2:
            f2.writelines(random)

    def ex15(self):

        """
            Lav en funktion, som læser den første linje i 'text.txt' og den første 
            linje i 'ex12.txt'. Sæt herefter de to linjer sammen. Brug herefter 
            enumerate til at loope over stringen. Fjern hvert fjerde bogstav og alle 
            mellemrum. Gem resultat i en ny fil -> 'ex16.txt'.
        """

        joinedLines = ""

        with open("text.txt", "r") as f:
            joinedLines = joinedLines + str(f.readline().rstrip())
        
            with open("ex12.txt", "r") as f1:
                joinedLines = joinedLines + str(f1.readline())
                print(joinedLines)

                joinedLines = joinedLines.replace(" ", "")
                joinedLines2 = ""

                for idx, i in enumerate(joinedLines):
                    if idx % 4 != 0:
                        joinedLines2 += i
                    
                print(joinedLines2)

        with open("ex16.txt", "w") as f3:
            f3.write(joinedLines2)        

    def ex17(self, filnavn, tekst):

        """
            Lav en generel funktion, som tager to input - (self, filnavn, tekst) - 
            og 'append', dvs. tilføj til sidst, teksten til den givne fil. 
        """

        with open(filnavn, "a+") as f1:
            f1.writelines(tekst)

        print(f1)

    def ex18(self, filnavn, bogstav):

        """
            Lav en funktion, som tager to input - (self, filnavn, bogstav) - 
            og åben filen, tæl antallet af de bogstav, returner dette. 
        """

        with open(filnavn, "r") as f2:

            count = 0

            for i in f2:
                for j in i:
                    if j == bogstav:
                        count += 1
            print(count)

    def ex19(self, filnavn, liste): # løst med Mads

        """
            Lav en funktion, som tager to input - (self, filnavn, liste) -
            og åben filen, skriv inputtet fra listen til filen en ad gangen, og
            print filens input. 

            (brug en lille fil, så den ikke skal printe en hel bog)
        """       

        with open(filnavn, 'a') as f1:

            for i in liste:
                f1.writelines("\n" + i)

        # with open(filnavn, "r") as f10:

        #     whatever = f10.read()
        #     print(whatever)

    def ex21(self):

        """
            Lav en funktion, som opretter et nyt dictionary. Tilføj 10 forskellige 
            elementer. Slet 3 af dem. Opdater 3 af dem med nye værdier. 
            Skriv resultatet, dvs. indholdet af dictionariet, til en ny fil kaldet 
            'ex21.txt'. 
        """

        Me = {}

        Me["Name"] = "Dagmar Ree"
        Me["Age"] = 23
        Me["Eyes"] = "Green"
        Me["Height"] = 164
        Me["Adress"] = "Vanløse Allé, 11"
        Me["Phone Number"] = "2616 3369"
        Me["Occupation"] = "Service employee"
        Me["High School"] = "Sankt Annæ Gymnasium"
        Me["Instrument"] = "Piano"
        Me["Credit Card info"] = "Fuck you"

        print(Me)

        del Me["Credit Card info"]
        del Me["High School"]
        del Me["Phone Number"]

        print(Me)

        Me.update({"Name": "Dagmar Krestence Ree", "Instrument": "Guitar", "Eyes": "brown-ish"})

        print(Me)

        with open("ex21.txt", "w") as f4:
            f4.write(json.dumps(Me))

    def ex22(self):

        """
            Lav en funktion, som læser indholdet af 'ex21.txt' ind som et 
            dictionarie. Slet et 'random' 'key/value' pair. Print resultatet, dvs. 
            det nye dict, på en pænt måde. 
        """

        with open("ex21.txt", "r") as f4:

            f4 = f4.read()
            Me = ast.literal_eval(f4)

            # This can be used for safely evaluating strings containing 
            # Python expressions from untrusted sources without the need to 
            # parse the values oneself.

            print(type(Me))
            print(Me)

            for key in random.sample(Me.keys(), 1):
                del Me[key]

            for key, value in Me.items():
                print(key, value)

    def ex25(self):

        """
            Lav en funtion, som sletter 'duplicate values' i et dict, dvs. 

            dict = {'a':10, 'b':10, 'c':20}

            bliver til..

            dict = {'a':10, 'c':20}

            Print resultatet på en pæn måde. 
        """

        emptyList = []
        emptyDict = {}

        original = {"a": 10, "b": 10, "c": 20, "d": 25, "e": 20, "f": 10, "g": 25}

        for key, val in original.items():
            if val not in emptyList:
                emptyList.append(val)
                emptyDict[key] = val

        for key, value in emptyDict.items():
            print(key, value)

if __name__ == "__main__":

    runExercises = exer7_2()
    print(40*"-")
    runExercises.ex11_1()
    print(40*"-")
    runExercises.ex11_2()
    print(40*"-")
    runExercises.ex11_3()
    print(40*"-")
    runExercises.ex11_5()
    print(40*"-")
    runExercises.ex11_6()
    print(40*"-")
    runExercises.ex12()
    print(40*"-")
    runExercises.ex13()
    print(40*"-")
    runExercises.ex14()
    print(40*"-")
    runExercises.ex15()
    print(40*"-")
    runExercises.ex17("ex12.txt", "jeg skal snart have tomatsuppe juhuuuuiiii")
    print(40*"-")
    runExercises.ex18("ex14.txt", "e")
    print(40*"-")
    runExercises.ex19("ex12.txt", ["a", "b", "c", "d", "e"])
    print(40*"-")
    runExercises.ex21()
    print(40*"-")
    runExercises.ex22()
    print(40*"-")
    runExercises.ex25()

# Opgave 23 - ville bruge en af disse til at lave listen om til en dict.
# Jeg ville arbejder derfra, men kan ikke få disse til at fungere - heller ikke
# uden for classen. 

a = ["a", "b", "c", "d"]
a_dict = dict.fromkeys(a, "whatever")
print(a)
a1_dict = dict(enumerate(a))
print(a)

""" 
    Lav en funktion, som læser de 10 linjer, med 'open with', fra 'ex12.txt'.
    Erstat alle 'i' med 'j' og skriv resultatet til filen. Print indholdet 
    af filen.
"""

# with open("ex12", "r") as f1:
#     print(f1.read())

#     for idx, i in enumerate(f1.read()):
#         for ydx, y in enumerate(i):
#             if "i" in y:
#                 i[ydx] = y.replace("i", "j")

#     print(f1.read())