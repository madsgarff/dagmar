class exer5:

    def __init__(self):
        pass

    def ex1(self, str1): # her

        """
            Lav en funktion, som tager en string som input. Hvis denne string 
            har en længde, som er større end 7, så skal du returnere de tre 
            midterste chars ("a" er en char, "aa" er en string, dvs. en string
            er en samling af chars - så bare de tre midterste bogstaver.). 
            Hvis stringens længde er mindre end 7, så skal du returnere end 
            passende tekst, som fortæller, at den er for kort. 
        """

        Str1 = str(str1)
        # length = len(str1)
        result1 = Str1[(len(Str1)-2)//2:(len(Str1)+3)//2]
        result2 = Str1[(len(Str1)-2)//2]

        if len(Str1) > 7:
            print(Str1)
            print(result1)
            print("If the word has an even amount of chars, only two will be\n"
            "returned. If the word has an odd amount of chars, three chars will be\n"
            "returned.")
            print("")

        else: 
            print("The phrashe is shorter than 7 chars. \nTherefor, the three "
                   "middle chars will not be returned.")

        res = ""

        for idx, i in enumerate(Str1):
            if idx == len(Str1)/2:
                res = Str1[idx-1] + Str1[idx] + Str1[idx+1]

        print(res)

    def ex2(self, str1, str2):

        """
            Lav en funktion, som tager to strings som input. Sammensæt disse 
            strings på denne måde;

            str1 = "Mads"
            str2 = "Dagmar"

            Så bliver string tre:

            str3 = "MaDagmards

            Og denne skal returneres.
        """

        str1 = str(str1)
        str2 = str(str2)

        str3 = str1[:2] + str2 + str1[-2:]
        print(str3)

    def ex3(self, str1):

        """
            Lav en funktion, som tager en string som input. Returner den samme string i modsatte rækkefølge.

            str1 = "Mads"

            Så skal "sdaM" returneres. 
        """
        Str1 = str(str1)

        Str1 = Str1[::-1] # når man slicer på denne måde, så vender man
                             # stringen om.
        print(Str1)

    def ex4(self, str1): # Med inspiration fra ex1

        """
            Lav en funktion, som tager en string som input. Funktionen skal returnere samme string, men skal
            sørge for, at den sidste halvdel af stringen er upper case, dvs.

            str1 = "Mads"

            Så skal "maDS" returneres.
        """
        Str1 = str(str1)

        print(Str1.upper()) # her bliver hele stringen uppercases.

        firstHalfOfStr1 = Str1[:(len(Str1))//2]
        secondHalfOfStr1 = Str1[(len(Str1))//2:]

        print(firstHalfOfStr1)
        print(secondHalfOfStr1)

        print(firstHalfOfStr1 + secondHalfOfStr1.upper())

    def ex5(self, str1):
        
        """
            Lav en funktion, som tæller konsonanter i en string og 
            returnerer antallet.
        """

        str1 = str(str1)
        print("The following applies to the word 'Exercise': ")

        consonants1 = 0
        vowels1 = 0

        for i in str1.lower():
            if (i == "a" or i == "e" or i == "i" or i == "o" or i == "u"):
                vowels1 = vowels1 + 1

            else:
                consonants1 = consonants1 + 1

        print("The amount of vowels is: ")
        print(vowels1)
        print("The amount of consonants is: ")
        print(consonants1)

        print("")

        str2 = input("Note that spaces will be considered as a consonant. \n" 
        "Please type a word or phrashe: ")
        print("")

        consonants2 = 0
        vowels2 = 0

        for i in str2.lower():
            if (i == "a" or i == "e" or i == "i" or i == "o" or i == "u"):
                vowels2 = vowels2 + 1

            else:
                consonants2 = consonants2 + 1

        print("The amount of vowels is: ")
        print(vowels2)
        print("The amount of consonants is: ")
        print(consonants2)

    def ex6(self, string): # Hjælp fra Simon - spørgsmål til selve funktionen.

        """
            Lav en funktion, som finder det bogstav med højest frekvens, dvs. 
            "aaaee" returnerer "a"

            Hint: Brug et dict til at tælle bogstaverne og find herefter det 
            højeste antal i det dict.
        """

        dict1 = {}

        for i in string.lower():
            if i not in dict1.keys():
                dict1[i] = 1
            
            else:
                dict1[i] += 1

        print(dict1)

        max_key = max(dict1, key = dict1.get)
        print(max_key)

        print(dict1.keys())

        maxNo = 0
        whateverMAYN = ""
        for x in dict1.keys():
            if maxNo < dict1[x]:
                maxNo = dict1[x]
                whateverMAYN = x 

        print(whateverMAYN)

    def ex7(self, str1): # Spørgsmål
        
        """
            Lav en funktion, som tager en string som input.
            Funktionen skal fjerne alle elementer, som står på et ulige input, 
            dvs;

            str1 = "HejDagmar"

            Bliver til

            str2 = "Hjamr"

            Og skal returneres.
        """
        str1 = str(str1)
        result = "" 

        for i in range(len(str1)):
            if i % 2 == 0:
                result = result + str1[i]

        print(result)
        print("")

        """
            Lav en funktion, tager en string og en int som input. 
            Funktionen skal fjerne det i'th bogstav i en string, dvs.
    
            Hvis string er; "abc"
            Og int er; 1

            Returneres "ac"
        """
        
        # Siden strings er immutabel, 
        # så kan vi hverken bruge .pop(), .remove(), del eller lign.
        # Vi laver derfor en ny string, som arbejder med den originale.
        # Først fjerne vi idx[1] med et forloop.

        str1 = str(str1)
        int1 = 1
        new_str1 = ""

        for i in range(len(str1)): # <- Hver er the deal med range(len())?
            if i != 1:
                new_str1 = new_str1 + str1[i]

        print("The new string, after removing i[1] in loop is:")
        print(new_str1)

        # Vi fjerner nu idx[1] igen, nu ved brug af slicing.

        print("The new string, after removing i[1] with use of slicing is:")
        new_str2 = str1[:1] + str1[2:]
        print(new_str2)

    def ex8(self, str1): # Spørgsmål

        """
            Lav en funktion, som tager en string som input.
            Funktionen skal returnere de to første og de to sidste elementer, som en ny string.

            Dvs.

            str1 = "HejMedDig"

            Returnerer "Heig"
        """

        str1 = str(str1)
        print(str1[0:2] + str1[-3:-1])
        # print(str1[:2::-3:]) <-- Hvorfor fungerer det ikke?

    def ex9(self, string): # Hjælp fra Simon

        """
            Lav en funktion, som tager en string som input.
            Funktionen skal returnere antallet af chars (et enkelt element i 
            en string), som er magen til det første element i den string, dvs.

            str1 = "HejHejHej"

            Returnerer 3, fordi der er 3 * "H"
        """

        str1 = str(string)
        result = 0

        for i in str1:
            if i == str1[0]:
                result += 1
        print(result)

    def ex10(self, str1): # lavet rimelig fucking besværligt... Men √ alligevel 

        """
            Lav en funktion, som tager en string som input.
            Funktionen skal dele stringen i midten og indsætte et mellemrum, fx;

            str1 = "Mads" 

            str2 = "Ma ds"

            Herefter skal de to dele omvendes, så "Mads" i sidste ende bliver til;

            str3 = "ds Ma"

            Og den sidste returneres.
        """

        str1 = str(str1)

        str2 = str1[:2] + " " + str1[2:]
        print(str2)
        str3 = str2[3:] + str2[2] + str2[:3]
        print(str3)

    def ex11(self, str1): # Spørgsmål
        
        """
            Lav en funktion, som tager en string som input.
            Funktionen skal fjerne alle elementer, som står på et ulige input, 
            dvs;

            str1 = "HejDagmar"

            Bliver til

            str2 = "Hjamr"

            Og skal returneres.
        """
        str1 = str(str1)
        result = "" 

        for i in range(len(str1)): # <- Hvad er the deal med "range(len()"?
            if i % 2 == 0:
                result = result + str1[i]

        print(result)
            
    def ex12(self, str1):

        """
            Lav en funktion, som tager en string som input.
            Denne string har flere ord i sig, dvs;

            str1 = "Hej med dig"

            Tæl antallet af ord og returner dette.
        """

        str1 = str(str1)
        print(str1)
        
        result = len(str1.split()) # metoden .split() (når man arbejder med
                                   # strings) returnerer antallet af
                                   # ord i stringen. 
        print(result)

    def ex13(self): # uden parametere - hjælp - her

        """
            Lav en funktion, som tager en string som input og en char, dvs.

            str1 = "Hej"
            char = "H"

            Her skal "ej" returneres, dvs. alt det som kommer efter 
            char i stringen, skal returneres. 
        """

        str1 = "Exercises"
        char = "c"

        # Nedenfor laver vi en ny string med den originale string som
        # udgangspunkt.

        str2 = str1.replace("E", "")
        print(str1)

        print("With use of the built-in method .replace():")
        print(str2)

        # Nedenfor slicer vi blot stringen og udelukker den første char.

        print("With use of slicing:")
        print(str1[1:])

        count = 0
        str10 = ""

        for i in str1:
            if char == i:
                str10 = str1[count+1:]

            count += 1

        print(str10)
            
        for idx, i in enumerate(str1):
            if char == i:
                str10 = str1[idx + 1:]

        print(str10)

    def ex14(self, str1, str2):

        """
            Lav en funktion, som tager to strings som input.

            Tjek om str2 er en substring af str1, dvs;

            str1 = "Mads"
            str2 = "ds"

            Her returnerer funktionen True.

            str1 = "Mads"
            str2 = "xxx"

            Her returnerer den False.
        """

        str1 = str(str1)
        str2 = str(str2)

        for i in str1:
            if str2 in str1:
                print(True)
                break

            else:
                print(False)
                break

    def ex15(self, str1):

        """
            Lav en funktion, som tager en string som input.
            Sorter denne string lesikografisk, dvs.

            str1 = "cba" 

            Bliver til;

            tr2 = "abc" 
        """

        # For arrays gælder built-in method .sort(). Når vi arbejder med
        # strings foregår det en smule anderledes. Vi bruger 
        # "".join(sorted()).

        arr = [3, 6, 12, 45, 90, 622, 562, 112, 67, 51]
        arr.sort()
        print(arr)

        str1 = str(str1)
        print("".join(sorted(str1))) # well, "".join(sorted()) sorterer
                                     # alfabetisk

if __name__ == "__main__":

    runExercises = exer5()
    print(40*"-")
    runExercises.ex1("Exercise")
    print(40*"-")
    runExercises.ex2("Mads", "Dagmar")
    print(40*"-")
    runExercises.ex3("Mads")
    print(40*"-")
    runExercises.ex4("Python")
    print(40*"-")
    runExercises.ex5("Exercise")
    print(40*"-")
    runExercises.ex6("aaBBbbc")
    print(40*"-")
    runExercises.ex7("Copenhagen")
    print(40*"-")
    runExercises.ex8("Exercise")
    print(40*"-")
    runExercises.ex9("HejHejHej")
    print(40*"-")
    runExercises.ex10("Mads")
    print(40*"-")
    runExercises.ex11("Copenhagen")
    print(40*"-")
    runExercises.ex12("Run this exercise")
    print(40*"-")
    runExercises.ex13()
    print(40*"-")
    runExercises.ex14("Copenhagen", "Denmark")
    print(40*"-")
    runExercises.ex15("Exercise")
    print(40*"-")

# 4.33 .count()