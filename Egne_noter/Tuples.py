# -*- coding: utf-8 -*-

# A tuple is like a list that cannot be changed once it’s 
# created (and populated with data). Tuples are immutable: they cannot change.
# As tuples are closely related to lists, it’s no surprise that they look 
# similar (and behave in a similar way. Tuples are surrounded by parentheses, 
# whereas lists use square brackets. A quick visit to the >>> prompt lets us 
# compare tuples with lists. Note how we’re using the type built-in function 
# to confirm the type of each object created:

vowels = ["a", "e", "i", "o", "u"] # vi har her at gøre med en list.
vowels2 = ("a", "e", "i", "o", "u") # vi har her at gøre med en tuple.

print(type(vowels))
print(type(vowels2))

# Ovenstående funktion (type) bruges til at finde ud af, hvilken class vi har med
# at gøre. Svaret bliver printet til terminalen. 
# Tænk på en tuple som var det en en liste, som ikke kan ændres. F.eks:

# vowels[2] = "I"
# vowels2[2] = "I"

# Havde vi printet til terminalen, ville vi få en fejl, da vi kun kan ændre et
# element på en index plads på denne måde i en liste. 

# Pas på med en tuple, som kun indeholder et enkelt element.

t = ("python") # klassificeres som en string, hvorimod
t2 = ("python",) # klassificeres som en tuple. Det er kommaet til sidst, 
# som gør den store forskel. Alle tuples SKAL indeholde et komme inde for
# paranteserne. 

print(type(t))
print(type(t2))