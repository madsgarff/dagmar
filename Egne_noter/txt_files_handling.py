# Encoding? - UTF-8 encoding?

# Hvornår er det praktisk at bruge følgende:
# 'rb' or 'wb' - Open in binary mode (read/write using byte data)

# Raw file types - open('abc.txt', 'rb', buffering=0)???

# Jeg har set "seq" flere gange - jeg har brug for uddybning.

# Hvorfor bliver jeg præsenteret for "\r\n\", når "\n" definerer, at der er
# en ny linje, og "\r\n\" gør det samme? Hvornår/hvordan er "\r\n\" relevant?

#____________________________________________

# Følgende er et uddrag af en kode fra teksten, som var lektier:

# class PngReader():
#     # Every .png file contains this in the header.  Use it to verify
#     # the file is indeed a .png.
#     _expected_magic = b'\x89PNG\r\n\x1a\n'

#     def __init__(self, file_path):
#         # Ensure the file has the right extension
#         if not file_path.endswith('.png'):
#             raise NameError("File must be a '.png' extension")
#         self.__path = file_path
#         self.__file_object = None

#     def __enter__(self):
#         self.__file_object = open(self.__path, 'rb')

#         magic = self.__file_object.read(8)
#         if magic != self._expected_magic:
#             raise TypeError("The File is not a properly formatted .png file!")

#         return self

# Der blev tidligere i opgaven præsenteret et billede, som der her bliver
# arbejdet med. Returnerer "return self" selve billedet?

#____________________________________________

# JEG ER VIRKELIG I TVIVL HER!

# .read(size=-1)
# This reads from the file based on the number of size bytes. 
# If no argument is passed or None or -1 is passed, then the entire file 
# is read.
# .readline(size=-1)
# This reads at most size number of characters from the line. 
# his continues to the end of the line and then wraps back around. 
# If no argument is passed or None or -1 is passed, then the entire 
# line (or rest of the line) is read.

# NOTER:

# # ASCII, stands for American Standard Code for Information Interchange. 
# It's a 7-bit character code where every single bit represents a unique 
# character. 

# Iterating for hver linje:

# Vi iterater for hver linje mens vi læser en fil. Se følgende eksempel:

# with open('dog_breeds.txt', 'r') as reader:
#     # Read and print the entire file line by line
#     line = reader.readline()
#     while line != '':  # The EOF char is an empty string
#         print(line, end='')
#         line = reader.readline()

# Herefter bliver hele filen læst.

# Vi kan også bruge den indbyggede funktion .readlines() (som 
# returnerer en liste):

# with open('dog_breeds.txt', 'r') as reader:
#     for line in reader.readlines():
#         print(line, end='')

# Og til sidst: vi kan iterate over filen for sig selv:

# with open('dog_breeds.txt', 'r') as reader:
#     # Read and print the entire file line by line
#     for line in reader:
#         print(line, end='')
#____________________________________________________


# d_path = 'dog_breeds.txt'
# d_r_path = 'dog_breeds_reversed.txt'
# with open(d_path, 'r') as reader, open(d_r_path, 'w') as writer:
#     dog_breeds = reader.readlines()
#     writer.writelines(reversed(dog_breeds))

# I en forrig opgave arbejde jeg på en lignende måde - jeg havde altså gang
# i to filer på en gang. Dog vidste jeg ikke, at man kunne bruge denne 
# strukter. Af den grund, så min kode således ud;

# with open("ex12.txt", "w+") as f1:
#     f = open("text.txt", "r")

#     f1.writelines(f.readlines()[:10])
#     f.close()

#____________________________________________________

# "end='" bruges for at forhindre Python i at lægge ydeligere nye linjer
# til teksten som bliver printet og KUN printe, hvad der faktisk bliver
# læst fra filen. 

#____________________________________________________

# Eksempel på hvordan man arbejder med "context manager":

# I classer kan man ikke bruge "with open" statementet. Af den grund benytter
# man __enter__ og __exit__:

# class my_file_reader():
#     def __init__(self, file_path):
#         self.__path = file_path
#         self.__file_object = None

#     def __enter__(self):
#         self.__file_object = open(self.__path)
#         return self

#     def __exit__(self, type, val, tb):
#         self.__file_object.close()

# Ovenstående er et eksempel fra den tekst, som var lektier. Jeg er godt
# og vel med, men er ikke helt sikker på, hvad parameterne "type", "val"
# og "tb" laver i classen.