# -*- coding: utf-8 -*-

# Vi holder lige vores fokus på, hvordan vi har defineret "person".
# For det første, så er hele vores dictionary omringet af {}. Hver "key"
# er skrevet med "" - som hvis det bare var strings - og det samme gælder
# for vores "values". Keys og values, BEHØVER dog ikke at være strings. Hver key
# er sepereret fra det tilshørende value med et kolon (:), og hver key/value par
# (også kaldet "row") er sepereret fra det næste key/value par med et komma:

person = {"Name": "Ford prefect", 
"Gender": "Male", 
"Occupation": "Researcher", 
"Home planet": "Betelgeuse Seven"}

print(person)
print("")

# Modsat arrays, så er dictionaries form ikke statisk.  
# Så gør det nemt for dig selv, og brug ovenstående struktur.
# Én lang smøre bliver besværlig at læse for sig selv og for andre, og
# rækkefølgen af de forskellige keys (til venstre) og deres tilhørende
# values (til højre), ændres.
# Vi kan opdatere vores disctionary ved brug af funktionen .update.
# F.eks.;

person.update({"Name": "Cartman", "Phone Number": "Super Secret"})
print(person)
print("")

# "A key is used to look up a value".

print(person["Home planet"]) # kaldes "person", så vil "Betelgeuse Seven"
# blive printet til terminalen.
print("")

# Lad os sige, at Cartman nu vil have endnu en kontakt - telefonnummer.
# Han har altså "Super secret" og vil nu tilføje "I hate Kyle".

person["Phone Number"] = "I hate Kyle", "Super Secret"
print(person["Phone Number"])
print("")

# Vi kan tilføje keys til en allerede eksisterende dictionary således;

person["Age"] = 33
print(person)
print("")

# Nedestående eksempel viser, hvordan man leder efter UDELUKKENDE keys, UDELUKKENDE
# values, eller hver key/value par samelt i en list.

print(person.keys())
print("")

print(person.values())
print("")

print(person.items())
print("")

#    for key, value in person.items:
#        print(key, value)
# Burde loope over de forskellige par???

# Vi kan tilføje hvor mange af den samme value en key indeholder.

found = {}

found["a"] = 0
found["e"] = 0
found["i"] = 0
found["o"] = 0
found["u"] = 0
print(found)

found["e"] = 1 
print(found)
found["e"] += 1
print(found)
# Vi har nu 2 "e" values.
found["e"] += 3
print(found) # Vi har tilføjet 3 til og har nu 5.

# Når vi looper over en dictionary foregår det lidt anderledes, end når
# vi looper over et array eller en liste.

# Vi bruger "kv" for key/value.

print("")
for kv in found:
    print(kv)

print("")
# Problemet med ovenstående loop er, at vi kun får returneret vores values.
# Skal vi have returneret hele vores dictionary lyder loopet:

for k in found:
    print(k, "was found", found[k], "times(s).")

print("")

# k repræsenterer key, og found[k] repræsenterer value.
# I tilfælde af, at en dictionary skal sorteres, foregår det således med
# et forloop;

for k in sorted(found):
    print(k, "was found", found[k], "times(s).")
# _________________________________________________________________________

print("")
vowels = ["a", "e", "i", "o", "u"]
word = input("Provide a word to search for vowels: ")

found = {}

found["a"] = 0
found["e"] = 0
found["i"] = 0
found["o"] = 0
found["u"] = 0

print("")
for letter in word:
    if letter in vowels:
        found[letter] += 1

for k, v in sorted(found.items()):
    print(k, "was found", v, "time(s):")

#__________________________________________________________________________

# BULLET POINTS:

# 1. Think of a dictionary as a collection of rows.

# 2. Each row is known as a key/value pair, and a dictionary can grow to 
# contain any number of key/value pairs. 

# 3. Like lists, dictionaries grow and shrink on demand.

# 4. A dictionary is easy to spot: it’s enclosed in curly braces,  
# with each key/value pair separated from the next by a comma, and 
# each key separated from its value by a colon.

# 5. Insertion order is not maintained by a dictionary. The order in which 
# rows are inserted has nothing to do with how they are stored.

# 6. Accessing data in a dictionary uses the square bracket notation. 
# Put a key inside square brackets to access its associated value.

# 7. Python’s for loop can be used to iterate over a dictionary. On each 
# iteration, the key is assigned to the loop variable, which is used 
# to access the data value.
#___________________________________________________________________________

print("")
# Lad os lave en helt ny dictionary.

Cartman = {"Name": "Eric Cartman", 
"Catchphrase": "Screw you guy, I'm going home", 
"Age": 8, "He hates": []}

print(Cartman)
print("")

print(Cartman["He hates"])
print("")

# Cartman["He hates"] = "Hippies", "Jews"
# print(Cartman["He hates"])
# print("")

print(Cartman.keys())
print(Cartman.values())
print(Cartman.items())
print("")

# Nedestående burde tilføje valuen "Jews" til keyen "He hates".
# Efterfølgende tilføjes "hippies".

#Cartman.setdefault("He hates", [])
Cartman["He hates"].append("Jews")
print(Cartman["He hates"])
print("")
Cartman["He hates"].append("Hippies")
print(Cartman["He hates"])

#___________________________________________________________________________

# Vi kan loope over en dictionary (eller legit lave et array om til en dict, for 
# så at loope over den) således.

whatever = ["dog", "sun", "Copenhagen", "exercise"]

whatever = dict(enumerate(whatever))
print(whatever)