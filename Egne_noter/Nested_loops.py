# -*- coding: utf-8 -*-

# Når vi har at gøre med "nesting", så arbejder vi med "funktioner"
# inde i "den samme funktion". F.eks:

# if a < 5:
#     if b < 10:
#         print("Davs")

# Det samme gælder for forloops;

for i in range(0, 3):
    for j in range(0, 3):
        print(i, j)

# I ovenstående eksempel bør vi få 9 ting printet til terminalen (eller 3 * 3).
# Der bliver loopet for i fra 0 - 2, hvilket vil sige, at vi får printet 0 3 gange.
# Første gang der bliver printet 0, hopper vi ned i det andet forloop, hvor j også er 0.
# Anden gang, vil j være 1, og tredje gang vil j være 2.
# Der er nu blevet printet 3 gange (range(0,3)), og vi hopper tilbage til det første
# forloop, hvor i nu vil være lig med 1. Same goes. Vores sidste print til terminalen lyder
# (2, 2).

# Lad os tage et lidt mere abstrakt eksempel. Vi vil gerne printe nedestående figur
# til terminalen;

# # # #
# # # #
# # # #
# # # #

# For at få 4 #'s vertikalt, ville vi kunne sige
print("# ")
print("# ")
print("# ")
print("# ")
# men vi vil have dem vertikalt. Det er her hvor , end="" kommer i spil.

print("# ", end = "")
print("# ", end = "")
print("# ", end = "")
print("# ", end = "")

# Når vi bruger , end="" fortæller vi python, at vi ikke vil videre til den næste linje.
# I stedet for at kode det samme 4 gange, kan vi lave et forloop.

for j in range(4):
   print("# ",end="")

# Problemet her er, at for at få vores ønskede mønster, så skal vi lige nu printe den 
# samme kode 4 gange. Vi bruger derfor et nested loop.

for i in range(4):
    for j in range(4):
        print("#",end="")

# Lad os tage et andet mønster.

# 
# #
# # #
# # # #

for i in range(4):
   for j in range(i + 1):
       print("# ", end="")

# Vi skriver range(i + 1) da range altid starter fra 0 af. Havde vi bare
# skrevet i, så havde vi kun fået de første tre rækker printet til terminalen.
# Vi vil nu printe den modsatte figur;

# # # # 
# # #
# #
#

for i in range(4):
   for j in range(4 - i):
       print("# ", end="")