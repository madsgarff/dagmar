# -*- coding: utf-8 -*-

# Lad os antage, at vi har en lang liste med en masse navne i en stor organisation,
# men at vi kun er interesseret i en meget lille og bestemt del af navnene. Her er
# et set er god måde at løse dette problem. Vi laver listen af navne om til et set.

# Et set er nemt at spotte i en kode! Det er en samling af nogle objekter, som
# er "skilt fra hinanden" via kommaer og omkring af disse: {}. F.eks:

vowels = {"a", "e", "e", "i", "o", "u", "u"}

# {} resulterer ofte i, at man forveksler et set med en dictionary, som også
# er omringet af {}. Den hovedsagelige forskel på et set og en dictionary er
# brugen af kolon (:) i dictionaries. Det er disse, som adskiller objekterne
# fra hinanden, modsat sets, hvor der bruges kommaer (,).
# Hvad et set og en dictionary har tilfælles er, at begge kan gro og formindskes, 
# som man nu har brug for det. 

#________________________________________________________________________________

vowels = set("aeiou")
word = "hello"

u = vowels.union(set(word))

# "union" kombinerer et sæt med et andet, hvilket så bliver lig med en ny
# variabel - "u". (Hvilket er et nyt set.)
# Pythons hukommelse ser således ud nu;

# vowels = a, e, i, o, u
# word = h, e, l, o
# u = a, e, i, o, u, h, l

# Vi sorterer alfabetisk nu således:

u_list = sorted(list(u))
print(u_list)

print(10*"-")

# En anden metode vi ser nærmere på er "difference". Den sammenligner to
# sets og fortæller dig, hvad der er i den ene, som IKKE er i den anden. 

d1 = vowels.difference(set(word))
print(d1)

# Alle bogstaverne som er med i "hello" bliver trukket fra u, og vi står nu tilbage med
# a, i og u.
# Det vi skal have fokus på her er, at vi undgår at appende under et forloop - og i det
# hele taget at loope.

# Den tredje metode ang. sets er "intersection".
# Her bliver der sammenlignet to sets, og tilbagemldt hvilket elementer der er ens. 

i = vowels.intersection(set(word))
print(i)

# Vi får nu printet "set(["e", "o"])" til terminalen. Metoden bekræfter, at vokalerne
# e og o er i variablen "word".
#________________________________________________________________________________

# BULLET POINTS:

# 1. Sets in Python do not allow duplicates.

# 2. Like dictionaries, sets are enclosed in curly braces, but sets do not identify
# key/value pairs. Instead, each unique object in the set is separated from 
# the next by a comma.

# 3. Also like dictionaries, sets do not maintain insertion order (but can be 
# ordered with the sorted function).

# 4. ou can pass any sequence to the set function to create a set of elements 
# from the objects in the sequence (minus any duplicates).

# 5. Sets come pre-packaged with lots of built-in functionality, including methods 
# to perform union, difference, and intersection.