# -*- coding: utf-8 -*-

# 4.4 break and continue Statements, and else Clauses on Loops

for n in range(2, 10):

    for x in range(2, n):

        if n % x == 0:
            print(n, "equals", x, "*", n//x)
            break

    else:
        # loop fell through without finding a factor
        print(n, "is a prime number") 

for num in range(2, 10):

    if num % 2 == 0:

        print("Found an even number", num)
        continue
    print("Found an odd number", num)

    # Nedestående fungerer ikke - kan ikke selv se fejl, men den skulle
# tilsyneladende være på linje 34

def fib(n):

    a, b = 0, 1

    while a < n:

        print(a, end = "")
        a, b = b, a + b
    
    print()

fib(2000)