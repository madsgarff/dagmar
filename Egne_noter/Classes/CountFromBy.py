
# # Noter findes i "Classes.py". Herfra arbejder vi kun med selve classen.

# class CountFromBy: 
#     pass

# # Vi erstatter "pass" med "c.increase()". Det ligner måske, at der ikke er
# # nogle argumenter her, men hvad python faktisk læser er;
# # CountFromBy.increase(c)

# class CountFromBy: 

#     def increase(self) -> None:

# # Lad os sige, at en class har to attributes: 
# # val, som indeholder den nuværende value af det nuværende object, og
# # incr, som indeholder mængden vi lægger til val hver gang. 
# # Umiddelbart ville det virke naturligt, at skrive denne UKORREKTE linje kode;

# # val += incr

# # Her er hvad vi bør skrive i stedet.

# class CountFromBy:
#     def increase(self) -> None:
#         self.val += self.incr

# # Når vi arbejder med classer, skal vi huske at "referere" til selve classen.
# # Vi skal derfor bruge "self".
# # Hvis det ikke rigtig giver mening/er forstyrrende, at der hele tiden
# # står self, så tænk på det som "this object's". Så "self.val" = 
# # "this object's val".

# # Vi tager nu __init__ i brug. Husk at det første argument ALTID
# # skal være "self". Vores class ser nu således ud;

# class CountFromBy:

#     def __init__(self) -> None:
#         pass

#     def increase(self) -> None:
#         self.val += self.incr

# # Vi tilføjer de to variabler, og giver dem navne inde i classen.

class CountFromBy:

    def __init__(self, v: int=0, i: int=1) -> None:
        self.val = v
        self.incr = i

    def increase(self) -> None:
        self.val += self.incr

h = CountFromBy(100, 10)
print(h.val) # 100 (Når kun den sidste, og indtil videre, færdigudviklede
             # class, ikke står som kommentar)
print(h.incr) # 10 (Når kun den sidste, og indtil videre, færdigudviklede
             # class, ikke står som kommentar)
