
from Class_me import Me
print(10*"-")

# Spørgsmål til video 9 - __str__?

# __len__ 

# car1 = car(3, "honda")

# print(len(car1))

# Object Oriented Programming (OOP)

# Undersøg __str__ statement. Den går vist lidt hånd i hånd med
# __init__ - 8. Object Oriented Programming, MIT 28:40 ca.

# Hvad er classes og objects;

# Classes kan anskues som en form for blueprint. Forstil dig, at en person 
# beder dig om at bygge en robot. Herefter spørger man så, "okay, men 
# hvilke egenskaber skal denne robot have?" En class indeholder altså de 
# samme ting, men der er som sådan ikke givet værdier. 
# Udover dette, så har classen også en function. 
# Lad os sige, at vi har to robotter. Det gælder for dem begge, at de har 
# et navn, en farve, en bestemt vægt, og at de begge kan introdocuere sig 
# selv. Det er dog ikke ens betydende med, at de er ens. Det vil altså 
# sige, at classen i dette tilfælde IKKE repræsenterer en bestemt variabel 
# af en robot, men blot objektet robot - det er et form for blueprint. 
# Classen har selvfølgelig også et navn, så lad os kalde denne for "robot". 

# I Python, Java, Javascript, mm. et stort set alt er et object. 
# F.eks. en built-in fucntion, en list, en float, whatever. Det kan også 
# være en kollektion af andre objekter. Altså en masse forskellige ting, 
# som så bliver samlet som et enkelt objekt, som vi kan sætte til en variabel. 
# Denne kan vi så senere hen bruge i forskellige funktioner. 

# Lige hurtigt ang. terminologi - variablerne inde i objektet (som kan sættes
# til en ny variabel) kaldes enten for instance variabels eller attributes.
# (Tænk klassens variabler).

# ATTRIBUTE = A value associated with an object which is referenced by
#             name using dotted expressions. For exaple, if an object o 
#             has an attribute a it would be referenced as o.a

# Den bestemte funktion som objektet har (den specifikke ting, som den 
# kan udføre), kaldes for methods. (Mads' Calculator) 
# (Tænk classens function)
# "De ligger i self"

# METHOD = A function which is defined inside a class body. If called as an 
#          attribute of an instance of that class, the method will get the
#          instance object as its first argument (which is usually called self).

# class Me:

    # def __init__(self, eyes, height, weight, name, age):

    #     self.eyes = eyes      <-- Attribute
    #     self.height = height  <-- Attribute
    #     self. weight = weight <-- Attribute
    #     self.name = name      <-- Attribute
    #     self.age = age        <-- Attribute

    # def getEyes(self):        <-- Method
    #     return self.eyes

    # def getHeight(self):      <-- Method
    #     return self.height

    # def getWeight(self):      <-- Method
    #     return self.weight

# GETTERS = Returnerer the value af en given attribue.
#           def getEyes(self):
# HER!!!        return self.eyes

# SETTERS = Sætter data attributes til hvad end der bliver passed in.

#           def getName(self): <- Getter
#               return self.name

#           def setName(self, newName): <- Setter
#               self.name = newName

# Lad os sige, at "name" = Dagmar Krestence Ree. (Husk, at "Me" er importeret).

D = Me("Green", "164 cm", "50 kg", "Dagmar Krestence Ree", "22")
print(D.getName()) # Dagmar Kerstence Ree

D.setName("Dagmar")
print(D.getName()) # Dagmar

D.setName("Dagmar Krestence Ree")
print(D.getName()) # Dagmar Krestence Ree

print(10*"-")

# __str__ = Denne metode fortæller python, hvordan et object af en 
#           bestemt type skal printes. 
#           Kalder man bare, så kan der ske det, at terminalen kun foræller
#           en, hvor et bestemt object befinder sig.  

# HHIERARCHIES = Her er der tale om parent classes og child classes, som kan
#                kan have child classes, som kan have child classes and so on.
#                Lad os tage et eksempel:

# Først har vi vores parent class(superclass):

class Animal(object):

    def __init__(self, age):
        self.age = age
        self.name = None
    def getAge(self):
        return self.age

# Vi har for oven vores getters - og ser i denne class bort fra setters.
# Lad os lave en child class (subclass) af ovenstående parent class:

class Cat(Animal):
    def speak(self):
        print("Meow")


# I vores child class er der ingen __init__ metode. Den er allerede kodet ind
# i parent classen. Kalder man en bestemt function som ikke findes i en 
# child class, så vil python lede efter funktionen i dens parent class.
# Findes den heller ikke der, vil den lede efter DENS parent class and so on.
# Findes den ikke i superclassen, spytter python en error ud.

#___________________________________________________________________________

# Jeg har importeret classen "Me". "22" kan tilgås på følgende måder;

D = Me("Green", "164 cm", "50 kg", "Dagmar Krestence Ree", "22")

print(D.getAge())
print(D.age)

print(10*"-")

# "D" SKAL defineres i denne fil, ellers kan python ikke finde den,
# heller ikke selvom den er defineret i filen, hvor classen er kodet i.
#___________________________________________________________________________

# Husk, at ved at bruge kommandoen "type", kan vi finde ud af, 
# hvilken class et objekt hører til. (Husk 'print') Således;

x = 1
print(type(x)) # <class 'int'>

print(type("hello")) # <class 'str'>

def hello():
    print("hello")

print(type(hello)) # <class 'function'>

# Lad os tage et hurtigt og simpelt eksempel på en class:

class Dog:

    def __init__(self, name, age):
        self.name = name
        self.age = age
       
    def get_name(self):
        return self.name
    
    def get_age(self):
        return self.age

    # def bark(self):
    #     print("bark")

    # def add_one(self, x):
    #     return x + 1

print(10*"-")
#__________________________________________________________________________

                        # Nedestående eksempler hører til de
                        # to ovenstående markerede funktioner.
                        # Der bliver altså ikke printet 
                        # noget til terminalen.

# Vi har lavet en simpelt class, og har under den defineret, hvad
# hvad en hund kan. F.eks. kan den gø.

                                # d = Dog()

# # Vi har ovenfor en variabel, d. Vi har defineret den til en 'instance'
# # af classes Dog. Det som står inde i kroppen pt. er vores method.

# # Instance − Et individuelt object af en bestemt class. Et object obj som 
# # tilhører class Circle, f.eks. er en instance af classen Circle.

                            # d.bark() # bark
# # Her kalder vi metoden bark og python finder nu classen Dog.

# print(type(d)) # <class '__main__.Dog'>
# # __ fortæller os, hvilken module classen var defineret i.
# # Basically står der bare, at variablen d tilhører classen Dog.

# # Jeg har tilføjet endnu en method i classen Dog, add_one.
# print(d.add_one(5)) # 6

# Grunden til at int 6 bliver printet til terminalen er, at vi har et
# parameter, x, som ikke har en værdi. Men når vi fortæller python, at
# vi skal have fat i attributen add_one, definerer vi x til at være 5.
# Python hiver nu fat i add_one (return x + 1), og lægger 1 til 5 (x).
#______________________________________________________________________

d = Dog("Tim", 34)
d2 = Dog("Bill", 12)

# Jeg har nu tilføjet en tredje method (den øverste __init__).

print(d.get_age()) # her bliver 34 printet til terminalen.

print(10*"-")
#___________________________________________________________________________
# ILLUSTRATION (DOG IKKE KODET KORREKT)

# Objekt 1:
# Variabel: "r1"

# name: "Tom"
# color: "Red"
# weight: "30"

# + introduceSelf()
# Vi når til nedestående senere;

#'r1 = Robot("Tom", "Red", 30)'

# Objekt 2:
# Variabel: "r2"

# name: "Jerry"
# color: "Blue"
# weight: "40"

# + introduceSelf()
# Vi når til nedestående senere;

#'r2 = Robot("Jerry", "Blue, 40")'

# ______________________________________________________________

# Selve classen:
# ______________________________________________________________

# Robot

# name:
# color:
# weight:

# + introduceSelf()

# I python vil classen se således ud:

class Robot:
    def __init__(self, name, color, weight):

        self.name = name
        self.color = color
        self.weight = weight

    def getName(self):
        return self.name

    def getColor(self):
        return self.color

    def getWeight(self):
        return self.weight

# Keywordet self er ish "this in python". Det refererer til det objekt, 
# som kalder funktion med.
# Så hvis vi kalder funktionen med objekt 1, så vil "My name is Tom" blive 
# printet til terminalen. 

r1 = Robot("Tom", "Red", 30)
r2 = Robot("Jerry", "Blue", 40)

r1Name = r1.getName()
r2Name = r2.getName()

print(r1Name)
print(r1.getName()) # samme resultat som for oven

print(r2Name)
print(r2.getName()) # samme resultat som for oven

print(10*"-")
#_________________________________________________________________________

# Using a class lets you bundle behavior and state together in an object. 
# When you hear the word behavior, think function—that is, a chunk of code
# that does something (or implements a behavior, if you prefer).
# When you hear the word state, think variables—that is, a place to store 
# values within a class. When we assert that a class bundles behavior and 
# state together, we’re simply stating that a class packages functions and 
# variables.

# Vi laver en class og følger med, som den langsomt udvikler sig.

#class CountFromBy():
#    pass

# Vi laver nu to objecter, a og b, som hører til ovenstående klasse.
#a = CountFromBy()
#b = CountFromBy()

        # Side note - et objekt af en klasse kan ligne et functioncall.
        # Der er tradiition for, at man bruger _ i functioncalls og
        # store bogstaver i class objects. 
        # count_from_by() - functioncall
        # CountFromBy() - laver et object til en class.

#c = CountFromBy()
#c.increase()
#c.increase()
#c.increase()
#print(c) # "3" burde blive printet til terminalen

#d = CountFromBy(100)
#print(d) # 100 burde blive printet til terminalen
#d.increase
#d.increase
#d.increase
#print(d) # 103 burde blive printet til terminalen

# Vi kan også fortælle python hvor meget vi vil have, at vores value stal
# stige med per gang. Således;

#e = CountFromBy(100, 10)
#print(e) # 100 burde blive printet til terminalen

#for i in range(3):
#    e.increase()

#print(e) # 130 burde blive printet til terminalen. Siden vi har brugt
         # range(3), fortæller vi python, at der skal loopes 3 gange, og
         # vi har tidligere sagt, at 10 skal tilføjes per gang.

# Vi gør brug af et nyt keyword, increment.

#f = CountFromBy(increment=15)
#print(f) # 0 burde blive printet til terminalen

#for i in range(3):
#    f.increase()

#print(f) # 45 burde blive printet til terminalen
# Vi fortælle python, at tallet skal stige med 15, da vi laver objektet f.
# "increase" bliver så brugt 3 gange, 3 * 15 = 45.

# Lige nu ser vores objects således ud;

# >>> c - 3
# >>> d - 103
# >>> e - 130
# >>> f - 45

# Hvad der er vigtigt at nævne her er, at metoden er delt. Der er altså sket
# det samme ved hvert object. Det samme gælder IKKE for attributes - hvilket
# giver sig selv, da hvert object ligger inde med forskellige værdier pt.

# Key point: THE METHOD IS SHARED BUT THE ATTRIBUTE DATA ISN'T.
# Se filen "CountFromBy.py" for mere information om, hvordan classen kan 
# udvikle sig.
#_________________________________________________________________________

# BULLET POINTS

# 1. Python classes let you share behavior (a.k.a. methods) and 
# state (a.k.a. attributes).

# 2. If you remember that methods are functions, and 
# attributes are variables, you won’t go far wrong.

# 3. When an object is created from a class, the object shares the class’s 
# code with every other object created from the class. However, each object 
# maintains its own copy of the attributes.

# 4. You add behaviors to a class by creating methods. A method is a 
# function defined within a class.

# 5. To add an attribute to a class, create a variable.
#__________________________________________________________________________