class Me:

    def __init__(self, eyes, height, weight, name, age):

        self.eyes = eyes
        self.height = height
        self. weight = weight
        self.name = name
        self.age = age

    def getEyes(self):
        return self.eyes

    def getHeight(self):
        return self.height

    def getWeight(self):
        return self.weight

    def getName(self):
        return self.name

    def getAge(self):
        return self.age

    def setName(self, newName):
        self.name = newName

print("")
print(10*"-")

D = Me("green", "163 cm", "50 kg", "Dagmar Krestence Ree", "22")

T = Me("brown", "163 cm", "68 kg", "Theresa Christensen", "22")

print(T.getName() + " " + "has" + " " + T.getEyes() + " " + "eyes")
print("")

print(D.getName() + " " + "weighs" + " " + D.getWeight())

print("")
print(D.getName()) # Dagmar Krestence Ree

D.setName("Flødefisk")

print("")
print(D.getName()) # Flødefisk

print("")
D.setName("Dagmar Krestence Ree")
print(D.getName()) # Dagmar Krestence Ree
print("")

# if __name__ == "__main__":
#     print("hej")

# print(type(D))

# if __name__ == "__name__":
#     print("hej")

# print(type(D)) # <class '__main__.Me'> wtf?