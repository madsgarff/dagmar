# -*- coding: utf-8 -*-

# Hermed en gennemgang af video om arrays. Først defineres en "list", 
# eller et array, som det også kaldes.

integers = [25, 12, 36, 11, 84]
print(integers)

# Vi kan herefter printe det første element på følgende måde:
print(integers[0])

# Vi skriver "0" og ikke "1", da python starter sit array fra 0. Altså vil et array af
# 10 elementer lyde [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]. Ligeledes gælder;

print(integers[1])
# 12
print(integers[2])
# 36
print(integers[3])
# 11
print(integers[4])
# 84

# Lad os sige, at vi vil printe fra et vis element af, og så til slutningen af arrayet.
# Vi gør således, og vil hermed printe fra det tredje element af;

print(integers[2:])

# Vi kan også bruge negative tal - disse repræseterer også vores elementer i arrayet. Følgende gælder;
#         0   1   2   3   4
#list = [25, 12, 36, 11, 84]
#        -5  -4  -3  -2  -1      
print(integers[-2])
#giver os altså "11", lige så vel som print(list[3]) gør.

# Vi kan også bruge strings i et array. Således;

names = ["Freddy", "Dagmar", "Soffi"]

print(names)

# Vi kan endda have et array, som består af flere arrays. Vi har indtil videre to arrays - names og integers.
# Vi laver en tredje med to elementer, hvor hvert element er lig med vores to tidligere arrays.

arrays = [integers, names]
print(arrays)

# Vi kan tilføje endnu et element i vores arrays ved at bruge "append".
# Tryk blot . efter at have typet navnet på dit array - flere muligheder vil komme frem.

integers.append(45)
print(integers)

# Elementet 45 bliver tilføjet  som det sidste element i arrayet. Funktionen "insert" giver dig mulighed for at 
# bestemme, på hvilken plads det nye element skal tilføjes. Funktionen "remove" fjerner et elemtent, and so on. 

integers.insert(2, 77)
print(integers)
integers.remove(77)
print(integers)

# Lad os sige at vi vil fjerne flere elementer. Vi bruger funktionen "del", og fortæller python fra hvilken
# plads i arrayet vi vil eliminere elementer. F.eks.;

del integers[4:]
print(integers)

# Vi kunne også blot have printet fra det 4. element af. print(integers[4:])
# Omvendt kunne vi også have printet blot de 4 første elementer. print(integers[0:4]).
# Lad os sige, at vi bare vil fjerne et enkelt element. I så tilfælde havde vi sagt;

del integers[2]

print(integers)

# Lad os lave et helt nyt array, og se på funktionen .remove(element).

array = [0, 1, 2, 3, 4, 6, 5, 6]

print(array)

array.remove(6)

print(array)

# Her fjerner vi ikke det 8. element. Vi fjerner elementet "6". Læg mærke til, at der er 2
# 6-taller. Bruger man funktionen .remove(element), er det det første element med den specifikke
# int, float, string osv. der bliver fjernet.

# .pop fjerner det sidste element fra arrayet. Således;

array.pop()

print(array)

# Indsætter man et tal, indikerer det, hvilket index nummer, der skal fjernes fra funktion, 
# hvor .pop() er tilføjet. F.eks. vil .pop(3) fjerne det 4. element.

# Vi kan ændre rækkefølgen, så elementet med den mindste værdi har pladsen 0 i arrayet. Såedes;

integers.sort()
print(integers)

# Funktionen .reverse() ændrer rækkefølgen på arrayet, så det nu har sin originale form. 

# Modsat f.eks. tubles (række forskellige elemter samlet - forskellige i den forstand, at
# nogle er integers, andre er string, der kan være floats, og endda andre tubles), kan man ændre værdier
# i det array, som man allerede har defineret. F.eks:

integers[2] = 100
print(integers)

# Det tredje element i arrayet var før 25 - efter at arrayet var blevet sorteret. 25 er nu
# blevet erstattet med 100.

# Lad os sige at vi har to forskellige arrays, og vil sætte disse to sammen. Vi kalder dem for
# A1 og A2.

A1 = [3, 5, 2]
A2 = [9, 9, 1]

print(A1)
print(A2)

# Ved at lave et helt nyt array, med et helt nyt navn, kan vi lægge A1 of A2 sammen, uden at de
# individuelle arrays ændrer form. Således;

A3 = A1 + A2

print(A3)
print(A1)
print(A2)

# Vi kan laver kloner af vores arrays. Dette er brugbart, hvis vi vil gemme det originale array.
# Vi kalder vores første array for;

cool = ["orange", "blue", "pink"]
print(cool)

chill = cool[:]

# Arrayet er nu blevet klonet, og vi kan tilføje et element til klonen.
# (Det skal understreges, at funktionen .copy vil gøre det samme. Vi har et array;
# First = [a, b, c]. Second = First.copy(). Det er endda præcis samme
# rækkefølge arrayet og kopien bliver skrevet.)

chill.append("black")

print(chill)
print(cool)

# Vi vender tilbage til arrayet A3 og ser på funktionen .extend([]).
# Tallene i [] kommer ikke til at indikere index numre, men blot hvilke elementer, 
# der skal tilføjes til sidst i arrayet. F.eks.

A3.extend([98, 97, 96])
print(A3)

