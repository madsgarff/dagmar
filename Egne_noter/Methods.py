
# Nedestående function tager hvert enkelt element i en given string 
# og gør de små bogstaver store (Er et af bogstaverne allerede store, 
# så vil der ikke ske noget i dette tilfælde, og bogstavet forbliver stort).
string = "Hello out there"
print(string.upper()) # UPPER
print(string.lower())

# På nedestående måde får vi, ved hjælp af slicing vendt stringen om.
print(string[::-1])

# Her får vi sorteret alfabetisk.
print("".join(sorted(string)))

#Her tæller vi antal ord i stringen.
print(len(string.split()))

# Forneden printer vi vores tuple, bare som en legit tuple.
# Ved at bruge .join() kan vi indsætte noget mellem alle elementerne.
myTuple = ("Hello", "out", "there")
print(myTuple)

x = " ".join(myTuple)
print(x)