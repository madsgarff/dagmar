from random import randint
# import opret_spillere
# from opret_spillere import spillere # Er dette korrekt?

class game:

    def __init__(self):
        print(40*"-")

        self.savedDices = []
        self.randomDices = []

        self.dices()

        print("You rolled:")
        print(self.randomDices)

    def dices(self):
        self.randomDices = []

        for _ in range(5 - len(self.savedDices)):
            self.randomDices.append(int(randint(1, 6)))

    def firstRoll(self):

        count = 0
        while count != 5:
            count += 1

            self.keepFromStart = input("Are there any dices, that you want to keep? N for no, Y for yes: ")

            if self.keepFromStart == "N": # Er umiddelbart god nok
                print("")
                self.dices()
                print("You have now rolled: ")
                print(self.randomDices)

            elif self.keepFromStart == "Y":
                self.keep = input("From dice 1-5, which dice/dices would you like to keep? (Use spaces between numbers): ")
                # self.keep.replace(" ", "")

                for i in self.keep.split(" "):
                    self.savedDices.append(self.randomDices[int(i)-1])
        
                print(self.savedDices)
                print("You have now rolled: ")
                self.dices()
                print(self.randomDices)

    def result(self):
        for i in self.savedDices:
            if xx in self.savedDices:
                print("You hit one pair.")    
        
        # 1 par
        # 2 par
        # 3 ens
        # 4 ens
        # Fuldt hus
        # straight
        # YATZY

if __name__ == "__main__":
    runGame = game()
    runGame.firstRoll()