class spillere:

    def __init__(self):
        print(40*"-")
        print("In this game, you will be a minimum of 2 players, and a maximun of 4.")

        # self.player1 = ""
        self.player2 = ""
        self.player3 = ""
        self.player4 = ""

        # Enten "" eller player1 (...).

        self.players = input("Select how many players you want to be - 1-4: ")

    # def players(self):
    #     if self.players == 2:
    #         # (...)

    #     elif self.players == 3:
    #         # (...)

    #     elif self.players == 4:
    #         # (...)

    # self.player2 = ""
    # self.player3 = ""
    # self.player4 = ""

    def names(self): # Branching skal gå i gang her.
        print("Please give every player a name. ")

        if self.players == 2:
            self.first = input("First name is: ")
            self.player1 = str(self.first)

            self.second = input(("Second name is: "))
            self.player2 = str(self.second)

        elif self.players == 3:
            self.first = input("First name is: ")
            self.player1 = str(self.first)

            self.second = input("Second name is: ")
            self.player2 = str(self.second)

            self.third = input("Third name is: ")
            self.player3 = str(self.third)
        
        elif self.players == 4:

            self.first = input("First name is: ")
            self.player1 = str(self.first)

            self.second = input("Second name is: ")
            self.player2 = str(self.second)

            self.third = input("Third name is: ")
            self.player3 = str(self.third)

            self.fourth = input("Fourth name is: ")
            self.player4 = str(self.fourth)

if __name__ == "__main__":
    runPlayers = spillere()
    runPlayers.names()