# -*- coding: utf-8 -*-

from flask import Flask


# BULLET POINT;

# 1. You’ve learned how Python supports single-value data items, such as 
# integers and strings, as well as the booleans True and False.

# 2. You’ve explored use cases for the four built-in data structures: 
# lists, dictionaries, sets, and tuples. You know that you can create complex
# data structures by combining these four built-ins in any number of ways.

# 3. You’ve used a collection of Python statements, including if, elif, else, 
# return, for, from, and import.

# 4. You know that Python provides a rich standard library, and you’ve seen 
# the following modules in action: datetime, random, sys, os, time, 
# html, pprint, setuptools, and pip.

# 5. As well as the standard library, Python comes with a handy collection of 
# built-in functions, known as the BIFs. Here are some of the BIFs 
# you’ve worked with: print, dir, help, range, list, len, input, sorted, dict, 
# set, tuple, and type.

# 6. Python supports all the usual operators, and then some. Those you’ve already 
# seen include: in, not in, +, -, = (assignment), == (equality), +=, and *.

# 7. As well as supporting the square bracket notation for working with items in 
# a sequence (i.e., []) , Python extends the notation to support slices, 
# which allow you to specify start, stop, and step values.

# 8. You’ve learned how to create your own custom functions in Python, using the 
# def statement. Python functions can optionally accept any number of arguments 
# as well as return a value.

# 9. Triple-quoted strings are also supported, and you’ve seen how they are used 
# to add docstrings to your custom functions.

# 10. You learned that you can group related functions into modules. 
# Modules form the basis of the code reuse mechanism in Python, and you’ve seen 
# how the pip module (included in the standard library) lets you consistently 
# manage your module installations.

# 11. Speaking of things working in a consistent manner, you learned that in 
# Python everything is an object, which ensures—as much as possible—that 
# everything works just as you expect it to.
#______________________________________________________________________________

# Uanset hvad du fortager dig på nettet, så handler det altid om "request"
# og "respond". Der bliver sendt et såkaldt "web request" fra en browser
# til en server grundet bruger information. Vi kan dele denne process op i
# 5 steps. 
# 1. Your user enters a web address, clicks a hyperlink, or clicks a button in her chosen
# web browser.
# 2. The web browser converts the user’s action into a web request and sends it 
# to a server over the Internet.
# 3. The web server receives the web request and has to decide what
# to do next..
# 4. The web server sends the response back over the Internet to the waiting web
# browser.
# 5. The web browser receives the web response and displays it on your user’s screen.

# Ved step 3 sker en ud af to følgende ting. Hvis det omtalte web-request er statisk
# inhold - såsom et billede, en HTML fil eller andet som er "stored" på serverens hard disk - 
# så finder web serveren "the resource" og begynder nu at forberede sig på at returnere
# den til browseren, som et "web response". 
# Er indholdet derimod dynamisk - noget som kan blive ved med at forandre sig. F.eks. indkøb
# over nettet - vil web serveren køre noget kode for at producere "the web response". 

