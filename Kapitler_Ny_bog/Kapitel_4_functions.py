# -*- coding: utf-8 -*-

# Vi starter med at følge en vis funktions omdannelse i den forstand, at
# vi hele tiden laver lidt om på den for at gøre den mere effektiv og lettere at læse.

#def search4vowels():
#    vowels = set('aeiou')
#    word = raw_input("Provide at word to search for vowels:")
#    found = vowels.intersection(set(word))
#    for vowels in found:
#        print(vowels)

# The intersection() function returns a set, which has the intersection of all 
# sets(set1, set2, set3…) with set1.
# It returns a copy of set1 only if no parameter is passed.

set1 = {2, 4, 5, 6}  
set2 = {4, 6, 7, 8}  
set3 = {4,6,8} 
  
# union of two sets 
print("set1 intersection set2 : ", set1.intersection(set2)) 

# Ovenstående burde give os;

# set1 intersection set2 :  {4, 6}

# Der bliver altså returneret de elementer, som set 1 og set 2 begge indeholder.

#search4vowels()

# Lad os sige, at vi i terminalen skriver "hitchhiker", når den beder
# om et raw input. Der vil i dette tilfælde blive printet i og e
# til terminalen. Skriver vi "galaxy", bliver der printet et enkelt a.
# Vores "set" indikerer, at vi kun er interesseret i en lille del af
# en bestemt organisation.
# Lad os ændre lidt på koden for første gang. 

#def search4vowels():
#    """Display any vowels found in an asked-for word:"""
#    vowels = set('aeiou')
#    word = raw_input('Provide at word to search for vowels:')
#    found = vowels.intersection(set(word))
#    for vowels in found:
#        print(vowels)

#search4vowels()

# Ovenfor er der blevet tilføjet en såkaldt "docstring". (Øverste linje i koden)
# Den beskriver kort formålet med funktionen. 

# Vi skal lige hurtigt ind på boolean. Et hvert objekt har en enten False eller True
# statement. Noget er False, hvis det har resultatet 0, en value "None", en tom
# string eller en tom data struktur. F.eks.

bool(0)
bool("")
bool([])
bool({})
bool(None)

# Modsat gælder for True statements. 
# Vi kan nu indkorporere booleans i vores funktion. Først ændrer vi dog i vores docstring. 
# Som vi hele tiden laver lidt om på vores funktion, er det vigtigt, at kommentaren
# giver programmøren et godt overblik over, hvad der faktisk skal ske i funktionen.
# Således;

#def search4vowels():
#    """Return a boolean basen on any vowels found."""
#    vowels = set('aeiou')
#    word = raw_input('Provide at word to search for vowels:')
#    found = vowels.intersection(set(word))
    
#    return bool(found)

#print(search4vowels())

# Finder funktionen en vokal i det ord, som terminalen bedte os om, 
# så vil True blive printet. Er det ikke tilældet, bliver False printet.
# Vi ændrer igen i vores docstring og også i vores "return" statement.

#def search4vowels():
#    """Return any vowels found in a supplied word"""
#    vowels = set('aeiou')
#    word = raw_input('Provide at word to search for vowels:')
#    found = vowels.intersection(set(word))
    
#    return found

#print(search4vowels())

# I terminalen har vi nu fået returneret et "set" - i tilfælde af, at orden
# indeholder vokaler. 

# Lists, dictionaries, and sets (being mutable) are always passed into a function by 
# reference— any changes made to the variable’s data structure within the 
# function’s suite are reflected in the calling code. The data is mutable, after all.
# Strings, integers, and tuples (being immutable) are always passed into a 
# function by value— any changes to the variable within the function are private to 
# the function and are not reflected in the calling code. 
# As the data is immutable, it cannot change.

# Function by reference - f.eks;

def double(arg1):
    print('Before:', arg1)
    arg1 = arg1 * 2
    print('After:', arg1)

x = 10
print(double(x))

y = 'Hello '
print(double(y))

z = [42, 256, 16]
print(double(z))

# Function by value - f.eks.;

#def change(arg2):
#    print('Before:', arg2)
#    arg2.append('More data')
#    print('After:', arg2)

#x = 10
#print(change(x))

#y = 'Hello '
#print(change(y))

#z = [42, 256, 16]
#print(change(z))

#___________________________________________________________________________________________________________

# BULLET POINTS;

# 1.  Function annotations can be used to document the type 
# of your function’s arguments, as well as its return type.

# 2. Functions are named chunks of code.

# 3. The def keyword is used to name a function, with the function’s code
# indented under (and relative to) the def keyword.

# 4. Python’s triple-quoted strings can be used to add 
# multiline comments to a function. When they are used in this way, 
# they are known as docstrings.

# 5. Functions can accept any number of named arguments, including none.

# 6. The return statement lets your functions return any number of
# values (including none).