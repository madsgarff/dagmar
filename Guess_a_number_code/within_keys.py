import random
from random import randrange
from random import randint
from random import random

class number_guess:

    def __init__(self):

        self.adjust_hint = ""
        print(40*"-")
        self.guess = int(input("Guess a number between 0 and 100. What is your first guess? "))
        self.correct = int(randint(0, 100))
        self.firstGuess = True
        print("Just to be sure: The correct answer is:")
        print(self.correct)
        print(40*"-")
        self.max1 = 24
        self.max2 = 49
        self.max3 = 74
        self.max4 = 100
        self.min1 = 0
        self.min2 = 25
        self.min3 = 50
        self.min4 = 75

    def guesses(self):

        switcher = {1: range(0, 25),
                    2: range(25, 50),
                    3: range(50, 75),
                    4: range(75, 101)
        }

        count = 0
        while self.guess != self.correct and count < 4:
            count += 1

            if self.guess in switcher[1]:

                if self.correct < self.guess and self.correct > switcher[1][0]: 
                    
                    if self.firstGuess:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[1][0])
                        print(self.adjust_hint)

                        self.max1 = self.guess 
                        self.firstGuess = False 
                    else:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.min1)
                        print(self.adjust_hint)
                        self.max1 = self.guess

                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct > self.guess and self.correct < switcher[1][-1]:

                    if self.firstGuess:

                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[1][-1])
                        print(self.adjust_hint)

                        self.min1 = self.guess
                        self.firstGuess = False
                    else:
                        
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.max1)
                        print(self.adjust_hint) 
                        self.min1 = self.guess                      

                    self.guess = int(input("Guess again babydoll: "))             

                elif self.correct in switcher[2]:
                    self.adjust_hint = "The number lies between " + str(self.min2) + " and " + str(self.max2)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[3]:
                    self.adjust_hint = "The number lies between " + str(self.min3) + " and " + str(self.max3)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[4]:
                    self.adjust_hint = "The number lies between " + str(self.min4) + " and " + str(self.max4)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

            if self.guess in switcher[2]:

                if self.correct < self.guess and self.correct > switcher[2][0]: 
                    
                    if self.firstGuess:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[2][0])
                        print(self.adjust_hint)

                        self.max2 = self.guess 
                        self.firstGuess = False 
                    else:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.min2)
                        print(self.adjust_hint)
                        self.max2 = self.guess

                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct > self.guess and self.correct < switcher[2][-1]:

                    if self.firstGuess:

                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[2][-1])
                        print(self.adjust_hint)

                        self.min2 = self.guess
                        self.firstGuess = False
                    else:
                        
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.max2)
                        print(self.adjust_hint) 
                        self.min2 = self.guess                      

                    self.guess = int(input("Guess again babydoll: "))         

                elif self.correct in switcher[1]:
                    self.adjust_hint = "The number lies between " + str(self.min1) + " and " + str(self.max1)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[3]:
                    self.adjust_hint = "The number lies between " + str(self.min3) + " and " + str(self.max3)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[4]:
                    self.adjust_hint = "The number lies between " + str(self.min4) + " and " + str(self.max4)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

            if self.guess in switcher[3]:

                if self.correct < self.guess and self.correct > switcher[3][0]: 
                    
                    if self.firstGuess:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[3][0])
                        print(self.adjust_hint)

                        self.max3 = self.guess 
                        self.firstGuess = False 
                    else:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.min3)
                        print(self.adjust_hint)
                        self.max3 = self.guess

                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct > self.guess and self.correct < switcher[3][-1]:

                    if self.firstGuess:

                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[3][-1])
                        print(self.adjust_hint)

                        self.min3 = self.guess
                        self.firstGuess = False
                    else:
                        
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.max3)
                        print(self.adjust_hint) 
                        self.min3 = self.guess                      

                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[1]:
                    self.adjust_hint = "The number lies between " + str(self.min1) + " and " + str(self.max1)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[2]:
                    self.adjust_hint = "The number lies between " + str(self.min2) + " and " + str(self.max2)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[4]:
                    self.adjust_hint = "The number lies between " + str(self.min4) + " and " + str(self.max4)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

            if self.guess in switcher[4]:

                if self.correct < self.guess and self.correct > switcher[4][0]: 
                    
                    if self.firstGuess:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[4][0])
                        print(self.adjust_hint)

                        self.max4 = self.guess 
                        self.firstGuess = False 
                    else:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.min4)
                        print(self.adjust_hint)
                        self.max4 = self.guess

                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct > self.guess and self.correct < switcher[4][-1]:

                    if self.firstGuess:

                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[4][-1])
                        print(self.adjust_hint)

                        self.min4 = self.guess
                        self.firstGuess = False
                    else:
                        
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.max4)
                        print(self.adjust_hint) 
                        self.min4 = self.guess                      

                    self.guess = int(input("Guess again babydoll: "))          
                
                elif self.correct in switcher[1]:
                    self.adjust_hint = "The number lies between " + str(self.min1) + " and " + str(self.max1)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[2]:
                    self.adjust_hint = "The number lies between " + str(self.min2) + " and " + str(self.max2)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct in switcher[3]:
                    self.adjust_hint = "The number lies between " + str(self.min3) + " and " + str(self.max3)
                    print(self.adjust_hint)
                    self.guess = int(input("Guess again babydoll: "))

                # else:
                #     self.guess = int(input("Guess again babydoll: "))

        if self.guess == self.correct:
            print("Congratulations,", self.correct, "is correct!")

        elif self.guess != self.correct and count == 4:
            print("The correct answer was", self.correct)

if __name__ == "__main__":
    runExercise = number_guess()
    runExercise.guesses()
