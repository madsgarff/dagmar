# Det hele skal laves i en class.

# Vi lægger ud med et "raw_input". Den giver vi en variabel.

# For hver gang brugeren spiller spillet, skal "det rigtige tal" ændre sig.
# Af den grund skal noget lign. int(randrange(100)) bruges. 

# Herefter skal vi arbejde med branching. 1 if-statement og 5 elif-statements.
# "Else" skal træde i kraft, hvis brugeren gætter på et nummer, som IKKE
# er mellem 0-100.
# Altså; if guess == correct. 
# Elif 1 = 0-25. 
# Elif 2 = 26-50. 
# Elif 3 = 51-75.
# Elif 4 = 76-100.

# Nested braching?

# På den ene eller anden måde, skal denne værdi, som også er tilknyttet
# en variabel, altså være "gemt" i enten "if" eller et "elif" statement.
# Variablen vil ALDRIG gemme sig under "else". Bliver der gettet på en float, 
# et negativt tal, et tal over 100 eller et imaginært tal, så træder "else's"
# print altså i kraft.

# I tilfælde af at vi skal arbejde med et loop, så skal vi bruge et while-loop.
# Dette skal køre, så længe brugeren IKKE har gættet det rigtige tal.

# Hver gang brugeren gætter på et tal, så skal et "hint" vises, hvis der ikke
# bliver gættet på det rigtige tal. 
# Lad os sige, at det tilfældige tal er 42. Hvis man gætter på 10,så skal der i 
# dette elif statement være et hint, som fortæller en, at tallet er større en 20. 
# På den måde leder man brugeren ud af dette statement og videre til et andet.

# Kan jeg gemme variablerne i én class og anvende dem i en anden?

    # 1. Hvordan skal hovedstruktueren være, dvs. skal der fx være et loop,
    #    der kører igen og igen, når der gættes forkert? Hvilket loop? 
    #    Hvordan skal det tage sig ud? Hvad ville en god guard være? Og
    #    hvordan/hvornår skulle sådan et loop slutte?

    # - Som sagt; hovedstrukturen bør være et while-loop. 
    # En potentiel guard ville være et statement som "while guess != correct",
    # hvor "guess" og "correct" er bundet til to variabler. 
    # Om man skulle starte med at have en guard med det udtrykket 
    # "guard = guess != correct", og while-loopet skulle lyde
    # "while guard == True:" (Hvorefter branching ville træde i kraft.)
    # Den ene valgt af brugeren og den valgt tilfældigt i koden. Ved hjælp af 
    # modulet "random" vil man kunne få koden til at ændre "correct" for hver 
    # gang spillet kører. Spillet ville slutte efter 5 gæt (for hvert gæt bliver 
    # der givet et hint) eller når brugeren gætter korrekt.

    # 2. Når den generelle struktur er på plads, hvilke funktioner er der 
    #    så brug for? En til brugerens svar? En til tallene? Etc.

    # Vi skal bruge en starttekst som beder om et tal, og dette tal skal bindes
    # til variablen "guess" (self.guess, for at være mere specifik.)
    # Startteksten skrives helt til start, altså under __init__.
    # Her bliver de to variabler defineret, hvor self.correct er tilfældig
    # for hver gang man kører koden. Ville randint(0, 99) være en idé her?
    # self.guess skal knyttes til et input. Umiddelbart vil jeg gå ud fra, 
    # at disse to kan laves i samme funktion.

    # 3. Hvordan ville man udforme en funktion, der kunne præsentere hints?
    #    De samme hints hvergang? Tilfældigt udvalgt? Etc. 

    # Hvis vi laver en funktion for sig selv - OG HUSKER AT SKRIVE SELF. FORAN
    # HVERT HINT GIVET (ligesom i "udgangspunkt-koden") - så bør man kunne 
    # bruge disse hints i andre funktioner. Mit bud er, at bruge "random" modulet
    # til at printe et tilfældigt hint for hver gang brugeren gætter.
    # Inde i hver branching statement et hint printes - medmindre gættet er
    # korrekt.

# OVERORDNET SPØRGSMÅL! first_guess er den første funktion jeg kører under 
# __main__, men hvis jeg fjerner self.guess = input fra __init__, 
# har jeg en indentationError helt fra start af. Wtf?