import random
from random import randrange
from random import randint
from random import random

class number_guess:

    def __init__(self):

        self.adjust_hint = ""
        self.guess = int(input("What is your first guess? "))
        self.max = 24
        self.min = 0 
        self.correct = int(randint(0, 25))
        self.firstGuess = True
        print(self.guess)
        print(self.correct)

    def guesses(self):

        switcher = {1: range(0, 25),
                    2: range(25, 50),
                    3: range(50, 75),
                    4: range(75, 101)
        }

        count = 0
        while self.guess != self.correct and count < 5:
            count += 1
            
            if self.guess in switcher[1]:

                if self.correct < self.guess and self.correct > switcher[1][0]: 
                    
                    if self.firstGuess:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[1][0])
                        print(self.adjust_hint)

                        self.max = self.guess 
                        self.firstGuess = False 
                    else:
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.min)
                        print(self.adjust_hint)
                        self.max = self.guess

                    self.guess = int(input("Guess again babydoll: "))

                elif self.correct > self.guess and self.correct < switcher[1][-1]:

                    if self.firstGuess:

                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(switcher[1][-1])
                        print(self.adjust_hint)

                        self.min = self.guess
                        self.firstGuess = False
                    else:
                        
                        self.adjust_hint = "The number lies between " + str(self.guess) + " and " + str(self.max)
                        print(self.adjust_hint) 
                        self.min = self.guess                      

                    self.guess = int(input("Guess again babydoll: "))              
                
                # elif self.guess == float:
                #     print("The number should be a real number.")

                # elif self.guess < 0:
                #     print("The number should lie between 1 and 100.")

                # elif self.guess > 100:
                #     print("The number should lie between 1 and 100.")

        if self.guess == self.correct:
            print("Congratulations", self.correct, "is correct!")

        elif self.guess != self.correct and count == 5:
            print("The correct answer was", self.correct)

if __name__ == "__main__":
    runExercise = number_guess()
    runExercise.guesses()