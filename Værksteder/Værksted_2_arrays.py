# -*- coding: utf-8 -*-

vowels = ["a", "e", "i", "o", "u"]

word = "Milliways"

for letter in word:
    if letter in vowels:
        print(letter)

print(len(vowels))

if "y" not in vowels:
    vowels.append("y")

print(vowels)

if "b" not in vowels:
    vowels.remove("a")

print(vowels)

if "a" not in vowels:
    vowels.remove("e")

print(vowels)

if "e" not in vowels:
    vowels.append("e")

print(vowels)

if "e" in vowels:
    del vowels[2]

print(vowels)