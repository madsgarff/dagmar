dict1 = {}

dict1["e"] = 0
dict1["e"] += 1
dict1["e"] += 3

dict1.update({"Name": "Mads", "Age": 34})

# Vi burde have kv: 
# "e": 4
# "Name": "Mads"
# "Age": 34

print(dict1)

del dict1["Age"]

print(dict1)

print(dict1.keys())

print(dict1.values())

for v in dict1:
    print(dict1[v])

with open("words.txt", encoding = 'utf-8') as f:
    # words = f.readlines()
    # print(words)

    dict2 = {}

    for i in f:
        if i not in dict2.keys():
            dict2[i] = 1        
        else:
            dict2[i] += 1

    print(dict2)

    max_key = max(dict2, key = dict2.get)
    print(max_key)

    print(dict2.keys())

__________________________________________________________________________



def almost_pi(N):

    n = range(1, (N + 1))

    for i in n:
        print(i)

    # (k = 0) = 1

    # pi = 1 - (1/3) + (1/5) - (1/7) + (1/9) - (1/11) + ...

    # Your code goes here!
    return 0

print(almost_pi(25))