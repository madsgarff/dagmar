# -*- coding: utf-8 -*-

# 1. Write a Python program to sum all the items in a list.

def add(arg1, arg2, arg3):

    x = arg1 + arg2 + arg3
    y = x + arg2
    z = y + x

    return z

resultOfAdd = add(1, 2, 3)
print(resultOfAdd)

# 2. Write a Python program to multiplies all the items in a list.

def multiplyedElements(arg1, arg2, arg3):

    x = (arg1 * arg2) * arg3

    return x

result = multiplyedElements(6, 2, 3)
print(result)

# 3. Write a Python program to get the largest number from a list.

def findMax(arr):

    maxNumber = 0

    for x in arr:
        print(maxNumber)
        if maxNumber < x:
            maxNumber = x
            print(maxNumber)
    return maxNumber

x = findMax([4, 7, 2, 5, 9])
print(x)
#__________________________________________________________________
    
maxNum1 = [89, 34, 87, 12, 3]

maxNum1.sort()
print(maxNum1[-1])
#__________________________________________________________________
maxNum2 = [98, 45, 67, 2, 11]
print("Largest element is:", max(maxNum2))

# 4. Write a Python program to get the smallest number from a list.

minNum1 = [76, 11, 72, 35]

minNum1.sort()
print(minNum1[0])
#__________________________________________________________________

minNum2 = [8, 56, 33, 41, 25]
print("Smallest element is:", min(minNum2))

def finMax(arr):

    arr.sort()
    return arr([-1])

# 5. Write a Python program to count the number of strings where the 
# string length is 2 or more and the first and last character are 
# same from a given list of strings.

# 8. Write a Python program to check a list is empty or not.

x = []

if not x:
    print("List is empty")

else:
    print("List has elements")

def isEmpty(array):

    x = False

    if len(array) == 0:
        x = True

    return x

x = isEmpty([])
print(x)


# 21. Write a Python program to convert a list of characters into a string.

statement = "Let's convert into string"
print(statement)
convert = list(statement)
print(convert)
print("".join(convert))

statement1 = ["Let's", "convert", "again"]
print(statement1)
print("".join(statement1))

# 7. Write a Python program to remove duplicates from a list.

array = [1, 2, 3, 4, 5]
print(array)

array.pop(3)
print(array)

# 9. Write a Python program to clone or copy a list.

def copy(array):

    array2 = []

    for x in array:
        
        array2.append(x)
        print(array2)

    return array2 

x = copy([1, 2, 3, 4])
# print(x)

# print(copy([1, 2, 3, 4]))

# 10. Write a Python program to find the list of words that are longer than n from a given list of words.

def array(n, arr):

    x = []

    for i in arr:
        if len(i) > n:
            x.append(i)

    return x

print(array(3, ["Fjolleri", "Monkeebizz", "a", "b"]))

print(array(2, ["Fjolleri", "Monkeebizz", "a", "b", "abc"]))

#____________________________________________________________

def arrArgument(arg1):

    arg1.pop(0)
    arg1.pop(3)
    arg1.pop(3)

    return arg1

arg1 = [9, 4, 6, 3, 5, 2]
print(arrArgument(arg1))

#____________________________________________________________

def function(i, arr):
    
    x = arr[i]
    
    return x

print(function(0, [1,2,3,4,5]))
print(function(1, [1,2,3,4,5]))
print(function(2, [1,2,3,4,5]))
print(function(3, [1,2,3,4,5]))
print(function(4, [1,2,3,4,5])) 