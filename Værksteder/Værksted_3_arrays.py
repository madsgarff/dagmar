# -*- coding: utf-8 -*-

first = [1, 2, 3, 4, 5]

print(first)

second = first

second.append(6)

print(second)
print(first)

third = [6, 7, 8, 9]
fourth = third[:]

fourth.append(10)
print(third)
print(fourth)

saying = "Don't panic!"
print(saying)
letters = list(saying)

print(letters)

book = "The hitchhiker's guide to the galaxy"
booklist = list(book)
print(booklist)
print(booklist[0:3])
print("".join(booklist[0:3]))

backwards = booklist[:: -1]
print("".join(backwards))

backwards.reverse()
print("".join(backwards))