# -*- coding: utf-8 -*-

paranoid_android = "Marvin, the Paranoid Android"

letters = list(paranoid_android)

for x in letters[:6]:
    print(x)

for x in letters[12:20]:
    print(x)

for x in letters[-7:]:
    print(x)

Apple = "Apple Does NOT suck!"

for x in Apple[:5]:
    print(x)

for x in Apple[5:10]:
    print(x)

for x in Apple[10:14]:
    print(x)

for x in Apple[-6:]:
    print(x)

Hah = "Så luk røven Simon"

for x in Hah:
    print(x)