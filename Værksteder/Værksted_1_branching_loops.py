# -*- coding: utf-8 -*-

abc = [1, 2, 3, 4, 5]

abc_sq = []

for num in abc:
    new_number = num ** 2
    abc_sq.append(new_number)

print(abc)
print(abc_sq)

is_even = []
is_odd = []

for num in abc_sq:
    if num % 2 == 0:
        print("This is even")
        is_even.append(num)
    else:
        is_odd.append(num)

print(is_even, is_odd)

x = 10
i = 0

while x > i:
    print(i)

    i = i + 1

x = 5
i = 0 

while x > i:
    print(x)

    i = i + 1

# Printer vi direkte i "range", så bliver 0-9 printet til terminalen.

print(range(0, 10))

