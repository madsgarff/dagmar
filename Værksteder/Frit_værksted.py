# # def blah():

# #     for i in range(0, 3):
# #         for j in range(0, 3):
# #             print(i, j)
# # blah()

# # Ingen argumenter. Nested array med værdier
# # loop over arrayet med enumerate.
# # Print værdierne fra arrayet med indexpladserne.

# def noArguments():

#     arr = []

#     for _ in range(5):
#         arr.append([random.randint(0,1000),random.randint(0,1000)])
    
#     print(arr)

#     for ydx, i in enumerate(arr):
#         for idx, j in enumerate(i):
#             print(arr[ydx][idx])

# noArguments()

# def nestedArray():

#     arr = []

#     for _ in range(3):
#         whatever = []
#         for _ in range(3):
#             whatever.append([random.randint(0, 50), random.randint(0, 50), random.randint(0, 50)])
#         arr.append(whatever)

#     print(arr)

#     for idx, i in enumerate(arr):
#         for ydx, j in enumerate(i):
#             for adx, k in enumerate(j):
#                 print(arr[idx][ydx][adx])

# nestedArray()

# for i in range(1, 11, 2):
#     print i


# # print(ord('a'))
# # print(ord('b'))
# # print(ord(''))
# # print(ord('#'))

# # Modsat nedenfor - uniform

# # print(chr(35))
# # # print(chr(76))

# def __init__(self, length):
#     self.length = length 

# def __len__(self):
#     return self.length 

# minBil = car(10) 

# len(minBil)

# print(40*"-")
# with open("ex12.txt", "r") as f:
#     print(f.readlines())
