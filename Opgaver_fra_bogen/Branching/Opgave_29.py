# -*- coding: utf-8 -*-

# Vi arbejder også med booleans. Hvis vores "if" statement er
# true, vil funktionen blive printet til terminalen. 

people = 20
cats = 30
dogs = 15

if people < cats:
    print("Too many cats! The world is doomed!")

if people > cats:
    print("Not many cats! The world is saved!")

if people < dogs:
    print("The world is drooled on!")

if people > dogs:
    print("The world is dry!")

dogs += 5

if people >= dogs:
    print("People are greater than or equal to dogs")

if people <= dogs:
    print("People are less than or equal to dogs")

if people == dogs:
    print("People are dogs :'(")

# 1. What do you think the "if" does to the code under it?           
# 2. Why does the code under "if" need to be intended four spaces?  *****
# 3. What happens if it isn't intended? *****
# 4. Can you put other boolean expressions from exercise 27 in 
# the "if-statement"? Try it. *****
# 5. What happens if you change the initial for people cats and dogs?