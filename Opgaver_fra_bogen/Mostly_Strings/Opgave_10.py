# -*- coding: utf-8 -*-

print("I am 6'2\" tall")
print('I am 6\'2" tall')

TABBY_CAT = "\tI'm tabbed in"
PERSIAN_CAT = "I'm split\non a line"
BACKSLASH_CAT = "I'm\\a\\cat"

FAT_CAT = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip\n\t* Grass
"""

print(TABBY_CAT)
print(PERSIAN_CAT)
print(BACKSLASH_CAT)
print(FAT_CAT)