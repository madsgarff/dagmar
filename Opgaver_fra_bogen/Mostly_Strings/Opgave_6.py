# -*- coding: utf-8 -*-

x = "There are %d types of people. \n" % 10
binary = "binary"
do_not = "don't"
y = "Those who know %s and those who %s. %s \n" % (binary, do_not, "string")

print(x)
print(y)

print("I said %r. \n") % x
print("I also said '%s'. \n") % y 

hilarious = False
joke_evaluation = "Isn't that joke so funny?! %r \n"

print(joke_evaluation % hilarious)

w = "This is the left side of..."
e = "a string with a right side.\n"

print(w + e)