# -*- coding: utf-8 -*-

# Loops and lists

the_count = [1, 2, 3, 4, 5]
fruits = ["apples", "oranges", "pears", "apricots"]
change = [1, "pennies", 2, "dimes", 3, "quarters"] 

for number in the_count:
    print("This is count %d" % number)

for fruit in fruits:
    print("A fruit of type: %s" % fruit)

for i in change:
    print("I've got %r" %i)

# We can also build lists. First, start with an empty one.

elements = []

# Then do the range function to do 0 to 5 counts. 

for i in range(0,6):
    print("Adding %d to the list" %i)

    # Append is a fucntion that lists understand. 
    elements.append(i)    

# Now we can print them out too. 
for i in elements:
    print("Element was: %d" %i)