# -*- coding: utf-8 -*-

# Hermed funktioner og variabler. 
# Til start definerer vi funktionen og har to argumenter. '%d'et indikerer, at denne
# string vil komme til at indeholde en integer. 

def cheese_and_crackers(cheese_count, boxes_of_crackers):
    print("You have %d cheeses!") % cheese_count
    print("You have %d boxes of crackers!") % boxes_of_crackers
    print("Man, that's enough for a party!")
    print("Get a blanket. \n")

# Første gang vi printer funktionen til terminalen, 
# fortæller vi python, at funktionen cheese_and_crackers indeholder to digits som lyder 20 og 30.

print("We can just give the function numbers directly:")
cheese_and_crackers (20, 30)

# 2. gang vi printer til terminalen, fortæller vi python - på linje 26 - at cheese_and_crackers har to variabler:
# "amount of cheese" og "amount of crackers".

print("OR, we can use variables from our script:")
amount_of_cheese = 10
amount_of_crackers = 50

cheese_and_crackers(amount_of_cheese, amount_of_crackers)

# 3. gang har vi at gøre med simpel matematik. Vores argumenter er blot to regnestykker.

print("We can even do math inside too:")
cheese_and_crackers(10 + 20, 5 + 6)

# 4. gang vi printer til terminalen, kombinerer vi regnestykkerne, med to variabler - 
# IKKE de direkte integers, som bliver givet til funktionen. 

print("And we can combine the two, variables and math:")
cheese_and_crackers(amount_of_cheese + 100, amount_of_crackers + 1000)