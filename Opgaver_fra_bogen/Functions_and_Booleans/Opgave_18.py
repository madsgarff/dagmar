# -*- coding: utf-8 -*-
# - Names, variables, code, functions

# Hermed forskellige former for funktioner - i den forstand at de har forskellige antal 
# argumenter. De to første "hænger sammen" (da begge funktioner indeholder to argumenter). For flere 
# eksempler, se de 6 opgaver under afsnittet "funktioner". 


# Script with argument

def print_two(*args):
    arg1, arg2 = args
    print("arg1: %r, arg2: %r") % (arg1, arg2)

# More simple, I guess

def print_two_again(arg1, arg2):
    print("arg1: %r, arg2: %r") % (arg1, arg2)

# ONE argument

def print_one(arg1):
    print("arg1: %r") % arg1

# NO arguments

def print_none():
    print("I got nothin'.")

# ______________________________________________

print_two("Zed", "Shaw")
print_two_again("Zed", "Shaw")
print_one("Frist!")
print_none()