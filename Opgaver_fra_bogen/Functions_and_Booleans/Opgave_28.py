# -*- coding: utf-8 -*-

# Hermed boolean øvelser. Husk om det er "false" eller "true" der passer til
# følgende statements.

True and True
#print("True")
False and True
#print("False")
1 == 1 and 2 == 1
#print("false")
"test" and "test"
#print("true")
1 == 1 or 2 != 1
#print("true")
True and 1 == 1
#print("true")
False and 0 != 0
#print("false")
True or 1 == 1
#print("true")
"test" and "testing"
#print("false")
1 != 0 and 2 == 1
#print("false")
"test" != "testing"
#print("true")
"test" == 1
#print("false")
not (True and False)
#print("true")
not (1 == 1 and 0 != 1)
#print("false")
not (10 == 1 or 1000 == 1000)
#print("false")
not (1 != 10 or 3 == 4)
#print("false")
not ("testing" == "testing" and "Zed" == "Cool Guy")
#print("true")
1 == 1 and not ("testing" == 1 or 1 == 0)
#print("true")
"chunky" == "bacon" and not (3 == 4 or 3 == 3)
#print("false")
3 == 3 and not ("testing" == "testing" or "Python" == "fun")
#print("false")