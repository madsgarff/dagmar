# -*- coding: utf-8 -*-

# Nogle programmører bruger udtrykket 'DRY' - Dont Repeat Yourself.
# Dette er programmørens bread and butter. Det er et virkelig vigtigt 
# koncept og er grundstenen for god kode. 

# Funktioner er et lille stykke kode, som kan genbruges igen og igen. 
# Med disse kan vi undgå at skrive det samme kode igen og igen. 

# Derfor er funktionen 1st tool i programming swiss-army knife og dem skal
# vi self. lære at kende.
w
# I python defineres en metode ved:

def nameOfMethod(argumentOne, argumentTwo, andSoOn): 
    # kroppen for metoden
    argumentOne = argumentOne + 1 
    argumentTwo = argumentOne + 1

    # en returnering af den værdi vi har arbejdet med
    return argumentTwo

# Argumenter og returværdier kan være mange forskellige ting, men for nu, 
# så arbejder vi kun med basale typer som ints, string, etc. 

# De tilsvarer matematiske funktioner. I stedet for at skrive f(a) fx.
# Så er det bare def nameOfMethod(a).

# Og hvis f = x^2 og f(2) = 4, så er returværdien jo bare 4.

# Det er det samme, som sker til sidst i vores metode ovenover. 
# Vi skriver bare eksplicit den værdi, vi gerne vil returnere med
# et 'return' statement.

def power(x):
    return x

hej = power(25)

print(hej)