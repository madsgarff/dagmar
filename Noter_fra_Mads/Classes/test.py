
def numberToNumeral(num):
    numerals = ["I", "V", "X", "L", "C", "D", "M"]

    result = ""
    idx = 0
    divisor = 5

    while(num > 0):
        numAsString = numerals[idx] * num
        count = numAsString.count(numerals[idx])
        result = count % divisor * numerals[idx] + result 

        num = (num // 5) if (idx % 2) == 0 else (num // 2)
        idx += 1
        divisor = 5 if (idx % 2) == 0 else 2

    return result

print(numberToNumeral(384))

result = ""
num = 384 

numAsString = "I" * num
count = numAsString.count("I")
result = count % 5 * "I" + result
num = num // 5

numAsString = "V" * (num)
count = numAsString.count("V")
result = count % 2 * "V" + result 
num = num // 2

numAsString = "X" * (num)
count = numAsString.count("X")
result = count % 5 * "X" + result 
num = num // 5

numAsString = "L" * (num)
count = numAsString.count("L")
result = count % 2 * "L" + result
num = num // 2

numAsString = "C" * (num)
count = numAsString.count("C")
result = count % 5 * "C" + result 
num = num // 5

print(result)