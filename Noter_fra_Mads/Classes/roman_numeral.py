
class romanNumeral:

    def __init__(self):
        self.one = "I"
        self.five = "V"
        self.ten = "X"
        self.fifty = "L"
        self.hundred = "C"
        self.fiveHundred = "D"
        self.thousand = "M"

    def numberToNumeral(self, num):
        numerals = ["I", "V", "X", "L", "C", "D", "M"]

        result = ""
        idx = 0
        divisor = 5

        while(num > 0):
            numAsString = numerals[idx] * num
            count = numAsString.count(numerals[idx])
            result = count % divisor * numerals[idx] + result 

            num = (num // 5) if (idx % 2) == 0 else (num // 2)
            idx += 1
            divisor = 5 if (idx % 2) == 0 else 2

        return result

        

    
