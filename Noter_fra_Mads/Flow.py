# -*- coding: utf-8 -*-

# Flow control er et meget vigtigt koncept i programmering, som lader 
# os vælge en 'branch', hvor vi kan eksekvere noget kode, baseret på 
# et 'boolsk udtryk'. Dette relaterer sig til den ene hjemmeopgave, hvor 
# der var eksempler på sammensatte boolske udtryk. 

# Et eksempel på brug af et boolsk udtryk i et program kan være;

# Hvis længden af koden (som man har indtastet, når man skal oprette en ny 
# konto) > 8 så har den en acceptabel længde og kan derfor
# gemmes i databasen.

# Legetøjseksempel hvor vores database er et array, hvor vi gemmer tubles
# med (username, password)

arrayAsOurDatabase = []

def saveUserToDB(username, password):
    arrayAsOurDatabase.append((username, password))
    return "User saved successfully in DB."

# Et eksempel for koden: 
username = input() # det samme som raw_input(), men er python3 syntax
password = input()  

if (len(password) < 8): 
    print("Error: Code too short - min. value 8 chars.")
else:
    saveUserToDB(username, password)

# Den mindste form er:

if (True): # Her et form for boolsk udtryk. De kan tage mange former.
    print(True)

# Den anden mindste er:

if (3 > 5): 
    print(True)
else:
    print(False)

# Den længste og potentielt uendelige er:

if (5 < 5):
    print(True)
elif (5 > 5):
    print(True)
elif (5 == 5):
    print(True)
else:
    print(False)