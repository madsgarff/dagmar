# -*- coding: utf-8 -*-

# While loops fungerer i princippet ligesom forloops, men er baseret på
# en 'Guard' i stedet for en iterator. Når jeg siger iterator, så mener 
# jeg en mængde af 'objekter/items', hvor man kan iterere over, dvs. fx
# et array. En string er også en mulighed. 

# Basalt set er en guard bare et boolsk udtryk, som vi ved vil afslutte 
# i fremtiden. Dette kan være farligt, fordi vi aldrig kan være sikre på
# at et program faktisk afslutter, før vi har kørt det. Dette er et 
# vigtigt resultat indenfor 'computer science', som er fundet af 
# Alan Turing, der er computer science god. Nobel prisen for computer 
# science er opkaldt efter ham.

# For et par år siden kom denne film frem: 
# https://www.imdb.com/title/tt2084970/?ref_=nm_knf_i2
# Som omgså handler om ham.

# Problemet omtales 'The Halting Problem'. 
# Mere infor her: https://en.wikipedia.org/wiki/Halting_problem

# Derfor skal vi være påpasselige med at bruge whileloops. 

# Det siges, at man kan gøre alt med et for-loop, som du kan gøre med et 
# while-loop. Men der er bare ting, som bliver nemmere, hvis vi benytter
# os af dem engang i mellem. Det er dog meget sjældent, hvis nogensinde, 
# at jeg selv bruger dem. 

# Jeg tænker ikke, at vi går vildt meget ind i dem. Du kan måske se dem,
# som et selvstudie. Der er ikke så meget til det, når først du har 
# set lyset.

# Et kode eksempel:

guard = 0

while (guard < 10):             # check om guard condition er nået 
    print("Dagmar rocks!")      # gør whatever
    guard = guard + 1           # læg en til 

# Når loopet har kørt 11 gange, så er værdien af guard 10 og derfor
# kører loopet ikke igen. Så basically gentager vi bare koden inde i 
# loopet indtil vores guard er nået. Guard kan tage mange former 
# og kan sågar bruges bare som en 'True' fx:

guard = True 
count = 0

while (guard):
    print("Dagmar rocks!")
    
    if (count == 5):
        guard = False
    else:
        count = count + 1

# Det her er 'kinda cheeting', fordi vi egentlig bare gør det samme, som
# i den første, men indirekte. But I pe you get the point.