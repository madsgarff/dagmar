# -*- coding: utf-8 -*-

# Et array (kaldet liste i python) er en datastruktur. Datastrukturer 
# bruges til at holde data sjovt nok). Data kan her forstås som flere ting.
# Den mest basale form for data har vi arbejdet en del med allerede. 
# Her tænker jeg på variablerne String, int, etc. 

# Et eksempel på brug af et array:

# Vi har en skoleklasse, hvor vi gerne vil gemme elevernes højde. 
# Herefter vil vi gerne taget gennemsnittet.

array = [] # dårligt navn, men det fint nu

# Arrays skal have "lagt elementer ind i sig". Dette kan vi gøre ved at 
# appende elementer til arrayet, dvs. at vi putter et element mere på 
# arrayet. Det sidste appendede element er derfor det sidste element 
# i arrayet.

array.append(210)
array.append(180)

# Opgave: Hvilket pladsnummer er elementet 175 på nu?

# Opgave: Lig 10 elementer ind i arrayet.

# Hvis vi skal tilgå elementer, så er syntaxen:

myHeight = array[0] # her tilgår vi plads nummer 0 i arrayet 
yourHeight = array[1] # her tilgår vi plads nummer 1 i arrayet 

print("Min højde: " + str(myHeight))
print("Din højde: " + str(yourHeight))
print("Gennemsnit: " + str((myHeight + yourHeight)/2))

# Hvilket også kan gøres direkte uden at tage elementer ud først:

print("Gennemsnit nr. 2: " + str((array[0] + array[1])/2))