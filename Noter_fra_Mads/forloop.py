# -*- coding: utf-8 -*-

# Forloop'et er en ud af to mulige måder et loope på. Vi har jo talt lidt 
# om det. Der er også while loopet, men vi starter her.

# Syntaxen er:

for x in range(10):
    print(x)

# Her printer vi bare talene 0 -> 9
# Det er 0 -> 9, fordi range returnerer et array med elementerne 0 -> 9
# og herefter looper vi over hvert enkelt element, som et efter et tildeles
# navnet x.

# I første loop er x = 0
# I andet loop er x = 1
# I tredje loop er x = 2

# ....

# I tiende loop er x = 9.

# Vi behøver ikke tænke så meget over formen på loopet lige nu, fordi vi
# skal kun bruge en form, som er:

array = [1,2,3,4,5,6,7,8,9]

for x in array: # selve loop declarationen 
    # kroppen på loopet.
    # følger samme form som en funktion.

    print(x)

# Her kan 'array' erstattet af mange forskellige ting. Forloopet i python
# er meget fleksibelt og kan bruges på mange forskellige måder. Lige nu
# fokuserer vi dog på arrays.

