# Lav en funktion som...
# 1. tager to ints som argumenter, summerer dem og returnerer resultatet.
# Vi bruger her kommandoen "add" for at lægge argumenterne sammen.
# For Mads' specifikke opgaver - se individuelle dybdegående beskrivelse af forskellige former for funktioner.

def add(x, y):
    a = x + y
    print(a)

add(1, 2)

def add(z, q):
    p = z + q

    return p
    
var1 = input("Enter number: ")
var2 = input("Enter number: ")

print(add(var1, var2))

# 2. tager to ints som argumenter, ganger dem og returnerer resultatet.
# Vi bruger her kommandoen "timeit" for at gange.

def timeit(x, y):
    a = x * y
    print(a)

timeit(3, 4)

def timeit(z, q):
    p = z * q

    return p

var1 = int(input())
var2 = int(input())

print(timeit(var1, var2))

# 3. som tager to ints som argumenter, tager a modulo b og returnerer resultatet.
# Vi bruger her kommandoen "modulo" for at finde resten fra de to argumenter.

def modulo(x, y):
    a = x % y
    print(a)

modulo(9, 4)
modulo(10, 4)
modulo(4, 9)

def modulo(z, q):
    p = z % q

    return p

var1 = int(input())
var2 = int(input())

print(modulo(var1, var2))

# 4. som tager to ints som argumenter, trækker dem fra hinanden, og returnerer resultatet.
# Vi bruger her kommandoen "sub".

def sub(x, y):
    a = x - y
    print(a)

sub(9, 3)
sub(3, 9)

def sub(z, q):
    p = z - q

    return p

var1 = int(input())
var2 = int(input())

print(sub(var1, var2))

# 5. som tager to ints som argumenter, dividerer dem med hinanden, og returnerer resultatet.
# Vi bruger her kommandoen "div".

def div(x, y):
    a = x / y
    print(a)

div(8, 2)
div(9, 3)

def div(z, q):
    p = z / q

    return p

var1 = int(input())
var2 = int(input())

print(div(var1, var2))

# 6. som tager et array, der indeholder 5 forskellige tal, og returnerer det tredje element.

list = [4, 7, 19, 11, 5]

print(list[2])