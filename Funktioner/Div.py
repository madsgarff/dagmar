# -*- coding: utf-8 -*-

# Tag to ints som argumenter, divider dem med hinanden, og returner resultatet.
# Vi bruger her kommandoen "div".

# Video

def div(x, y):
    a = x / y
    print(a)

div(8, 2)
div(9, 3)

# Raw_input

def div(z, q):
    p = z / q

    return p

var1 = int(raw_input())
var2 = int(raw_input())

print(div(var1, var2))

# Mads' eksempel

def div(x, y):
    result = x / y
    return result

endresult = div(10, 5)
print(endresult)