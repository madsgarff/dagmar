# -*- coding: utf-8 -*-

# 2. Tag to ints som argumenter, gang dem med hinanden og returner resultatet.
# Vi bruger her kommandoen "timeit" for at gange.

# Video

def timeit(x, y):
    a = x * y
    print(a)

timeit(3, 4)

# Raw_input

def timeit(z, q):
    p = z * q

    return p

var1 = int(raw_input())
var2 = int(raw_input())

print(timeit(var1, var2))

# Mads' eksempel

def timeit(x, y):
    result = x * y
    return result

endresult = timeit(5, 2)
print(endresult)

# Power

def power(x):

    result = x*x 

    return result

calculation = 11
print(power(calculation))

print(power(11))

# Power - funktion i funktion

def power(x):

    result = x*x 

    return result

calculation = 11
print(power(calculation))

print(power(11))

def add(x, y):

    result = x + y

    return result

endresult = add(10, 15)
print(endresult)
print(power(endresult))