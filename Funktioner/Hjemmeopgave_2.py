# -*- coding: utf-8 -*-

def coolCalculation(x, y, z, p):
    result_1 = x + y
    result_2 = result_1 * z
    result_3 = (result_2/y) * p

    return result_3

var1 = int(input())
var2 = int(input())
var3 = int(input())
var4 = int(input())

 print(coolCalculation(var1, var2, var3, var4))

def calculation(a, b, c, d):
      result_1 = coolCalculation(a,b,c,d)
    return result_1

print(calculation(2,3,4,5))

def beregning(q, w, e, r):
    result_1 = q * (q + r)
    result_2 = result_1 * q
    result_3 = result_2/result_1
    result_4 = result_3 + result_2

    return result_4

var1 = int(input())
var2 = int(input())
var3 = int(input())
var4 = int(input())

print(beregning(var1, var2, var3, var4))

