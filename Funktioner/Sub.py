# -*- coding: utf-8 -*-

# 4. Tag to ints som argument, træk dem fra hinanden, og returner resultatet.
# Vi bruger her kommandoen "sub".

# Video

def sub(x, y):
    a = x - y
    print(a)

sub(9, 3)
sub(3, 9)

# Raw_input

def sub(z, q):
    p = z - q

    return p

var1 = int(raw_input())
var2 = int(raw_input())

print(sub(var1, var2))

# Mads' eksempel

def sub(x, y):
    result = x - y
    return result

endresult = sub(4, 2)
print(endresult)