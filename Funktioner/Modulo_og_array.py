# -*- coding: utf-8 -*-

# 3. Tag to ints som argument, tag a modulo b og returner resultatet.
# Vi bruger her kommandoen "modulo" for at finde resten fra de to argumenter.

# Video

def modulo(x, y):
    a = x % y
    print(a)

modulo(9, 4)
modulo(10, 4)
modulo(4, 9)

# Raw_input

def modulo(z, q):
    p = z % q

    return p

var1 = int(raw_input())
var2 = int(raw_input())

print(modulo(var1, var2))

# Mads' eksempel

def modulo(x, y):
    result = x % y
    return result

endresult = modulo(10, 5)
print(endresult)

# 6. Tager et array, der indeholder 5 forskellige tal, og returner det tredje element.

list = [4, 7, 19, 11, 5]
print(list[2])
