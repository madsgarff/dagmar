# -*- coding: utf-8 -*-

# 1. Tag to ints som argumenter, summer dem og returner resultatet.
# Vi bruger her kommandoen "add" for at lægge argumenterne sammen.

# Video

def add(x, y):
    a = x + y
    print(a)

add(1, 2)

# Raw_input

def add(z, q):
    p = z + q

    return p
    
var1 = int(raw_input())
var2 = int(raw_input())

print(add(var1, var2))

# Mads' eksempel

def add(x, y):

    result = x + y

    return result

endresult = add(10, 15)
print(endresult)

# Lille ekstra hygge gejl

def greet():
    print("Hello")
    print("Good morning")

greet()