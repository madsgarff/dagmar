# -*- coding: utf-8 -*-

# Hermed en gennemgang af video om arrays. Først defineres en "list", 
# eller et array, som det også kaldes.

integers = [25, 12, 36, 11, 84]
print(integers)

# Vi kan herefter printe det første element på følgende måde:
print(integers[0])

# Vi skriver "0" og ikke "1", da python starter sit array fra 0. Altså vil et array af
# 10 elementer lyde [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]. Ligeledes gælder;

print(integers[1])
# 12
print(integers[2])
# 36
print(integers[3])
# 95
print(integers[4])
# 14

# Lad os sige, at vi vil printe fra et vis element af, og så til slutningen af arrayet.
# Vi gør således, og vil hermed printe fra det tredje element af;

print(integers[2:])

# Vi kan også bruge negative tal - disse repræseterer også vores elementer i arrayet. Følgende gælder;
#         0   1   2   3   4
#list = [25, 12, 36, 95, 14]
#        -5  -4  -3  -2  -1      
print(integers[-2])
#giver os altså "95", lige så vel som print(list[3]) gør.

# Vi kan også bruge strings i et array. Således;

names = ["Freddy", "Dagmar", "Soffi"]

print(names)

# Vi kan endda have et array, som består af flere arrays. Vi har indtil videre to arrays - names og integers.
# Vi laver en tredje med to elementer, hvor hvert element er lig med vores to tidligere arrays.

arrays = [integers, names]
print(arrays)

# Vi kan tilføje endnu et element i vores arrays ved at bruge "append".
# Tryk blot . efter at have typet navnet på dit array - flere muligheder vil komme frem.

integers.append(45)
print(integers)

# Elementet 45 bliver tilføjet  som det sidste element i arrayet. Funktionen "insert" giver dig mulighed for at 
# bestemme, på hvilken plads det nye element skal tilføjes. Funktionen "remove" fjerner et elemtent, and so on. 

integers.insert(2, 77)
print(integers)
integers.remove(77)
print(integers)

# Lad os sige at vi vil fjerne flere elementer. Vi bruger funktionen "del", og fortæller python fra hvilken
# plads i arrayet vi vil eliminere elementer. F.eks.;

del integers[4:]
print(integers)

# Vi kan ændre rækkefølgen, så elementet med den mindste værdi har pladsen 0 i arrayet. Såedes;

integers.sort()
print(integers)