# -*- coding: utf-8 -*-

print("Du sagde jeg skulle fjolle lidt rundt i python")
print("så jeg har prøvet at smide nogle af de ting du har")
print("gået igennem med mig sammen. Så, jeg synes det kunne være flabet at")
print("fortælle dig hvad jeg faktisk har gjort uden at vise dig min")
print("kode :D")
print("Bare læg ud med og give mig 5 tal :) - på hver sin linje")

var1 = int(raw_input())
var2 = int(raw_input())
var3 = int(raw_input())
var4 = int(raw_input())
var5 = int(raw_input())

elements = [var1, var2, var3, var4, var5, int(5)]

print(elements)

print("Start med at lægge mærke til det 6. og")
print("sidste element - jeg har tilføjet et 5-tal.")
print("Du skylder mig 5 smøger ;) Og hvis du allerede har givet mig dem") 
print("er det her lidt kikset...")
print("Anyways, jeg skal bruge et af de 6 elementer.")

svar = int(raw_input())

if elements.index(svar) == 0: # færdig? (Tilføj noget til sidst?) - et-cifrede?

    print("Okay så lige nu har du 'åbnet' 'if' i mit if/elif/else gejl.")
    print("Og siden du valgte det første element, kommer python ikke til")
    print("at arbejde med 6 andre muligheder - 5 'elif'er og et enkelt 'else'.")
    print("ANYWAYS!")
    print("Alt efter hvilket element du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde plus.")
    print("Giv mig 3 tal til! :D")

    def add(x, y, z):

        a = x + y + z

        return a

    plus1 = int(raw_input())
    plus2 = int(raw_input())
    plus3 = int(raw_input())

    print(add(plus1, plus2, plus3))

    foerste_resultat = plus1 + plus2 + plus3

    print("Vi har nu de første 3 tal lagt sammen. Jeg skal have 4 til. Når de er lagt")
    print("sammen, kan vi lægge de to endelige resultater sammen efterfølgende.")

    def add(x, y, z, p):

        b = x + y + z + p

        return b
    
    plus4 = int(raw_input())
    plus5 = int(raw_input())
    plus6 = int(raw_input())
    plus7 = int(raw_input())

    print(add(plus4, plus5, plus6, plus7))

    andet_resultat = plus4 + plus5 + plus6 + plus7

    print("Fedt mand. For at lægge de to værdier sammen, har jeg defineret de første tre")
    print("tal som en variabel, og de 4 næste som en anden variabel. De to variabler, kan")
    print("kan vi nu lægge sammen. Altså bliver det endelige resultat = ")

    print(foerste_resultat + andet_resultat)

    samlet_resultat = foerste_resultat + andet_resultat

    print("Vi kan nu loope over det endelige resultat, såfremt at x = den sidste værdi")
    print("og i = 0 + 1. ")

    print("Da vi her bruger et forloop, og beder Python om at loope i range(x) (hvor")
    print("x er den samlede værdi) vil den samlede værdi ikke være med i vores loop som")
    print("en integer.")

    x = samlet_resultat

    for i in range(x):
        print(i)
    
elif elements.index(svar) == 1: # færdig? (Tilføj noget til sidst?) - et-cifrede?

    print("Okay så lige nu har du 'åbnet' det første 'elif' i mit if/elif/else gejl.")
    print("Så derfor kommer python ikke til at arbejde med den første mulighed (if),")
    print("den sidste mulighed, (else) og de resterende 4 'elif'.")
    print("Alt efter hvilket element du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde minus.")
    print("Giv mig tre positive tal til! :D")

    def sub(x, y, z):

        a = x - y - z

        return a

    minus1 = int(raw_input())
    minus2 = int(raw_input())
    minus3 = int(raw_input())

    print(sub(minus1, minus2, minus3))

    foerste_resultat = minus1 - minus2 - minus3

    print("Jeg har nu trukket de tre tal fra hinanden i den rækkefølge du gav mig dem.") 
    print("Jeg skal have to positive tal til (de bliver lagt sammen) så jeg kan trække de to værdier") 
    print("fra hinanden til sidst.")

    def add(x, y):

        b = x + y

        return b

    minus4 = int(raw_input())
    minus5 = int(raw_input())

    print(add(minus4, minus5))

    andet_resultat = minus4 + minus5

    print("Fedt mand. For at trække de to værdier fra hinanden, har jeg defineret de første tre")
    print("tal som en variabel, og de næste 2 som en anden variabel. De to variabler")
    print("kan vi nu trække fra hinanden. Altså bliver det endelige resultat = ")

    print(foerste_resultat - andet_resultat)

    samlet_resultat = foerste_resultat - andet_resultat

    print("Vi kan nu loope over det endelige resultat, såfremt at x = den sidste værdi")
    print("og i = 0 + 1. ")

    print("Vi bruger her et forloop. Loopet stopper altså, så snart der ikke er flere")
    print("elementer tilbage.")

    x = samlet_resultat

    for i in range(x, 0):
        print(i)

elif elements.index(svar) == 2:

     print("Okay så lige nu har du 'åbnet' det andet 'elif' i mit if/elif/else gejl.")
     print("Så derfor kommer python ikke til at arbejde med den første mulighed (if),")
     print("den sidste mulighed, (else) og de resterende 4 'elif'.")
     
     # gange/timeit?

elif elements.index(svar) == 3:

    print("Okay så lige nu har du 'åbnet' det tredje 'elif' i mit if/elif/else gejl.")
    print("Så derfor kommer python ikke til at arbejde med den første mulighed (if),")
    print("den sidste mulighed, (else) og de resterende 4 'elif'.")

    #print("division")

elif elements.index(svar) == 4: # færdig? (Tilføj noget til sidst?)
    
    print("Okay så lige nu har du 'åbnet' det fjerde 'elif' i mit if/elif/else gejl.")
    print("Så derfor kommer python ikke til at arbejde med den første mulighed (if),")
    print("den sidste mulighed, (else) og de resterende 4 'elif'.")
    print("ANYWAYS!")
    print("Alt efter hvilket element du ville vælge, ville forskellige funktioner omhandle")
    print("forskellige regnemetoder - i dette tilfælde modulus.")
    print("Giv mig 2 tal til! :D")

    def modulo(x, y):

        a = x % y

        return a

    modulus1 = int(raw_input())
    modulus2 = int(raw_input())

    print(modulo(modulus1, modulus2))

    foerste_resultat = modulus1 % modulus2

    print("Vi har her taget første tal du gav mig modulus anden tal du gav mig.")
    print("Når vi arbejder med denne metode, finder vi ud af, hvor mange gange det andet")
    print("tal går op i det første. Vi ser bagefter på resten, og dette er vores resultat.")
    print("Jeg skal bruge to tal til, hvor vi igen bruger modulus.")

    def modulo(x, y):

        b = x % y

        return b
    
    modulus3 = int(raw_input())
    modulus4 = int(raw_input())

    print(modulo(modulus3, modulus4))

    andet_resultat = modulus3 % modulus4

    print("Fedt mand. Jeg har defineret de første to tal som en variabel, og de to næste") 
    print("som en anden variabel. Vi bruger nu for tredje gang den samme regnemetode og får, at")
    print("vores endelige resultat lyder = ")

    print(foerste_resultat % andet_resultat)

    samlet_resultat = foerste_resultat % andet_resultat

    print("Vi kan nu loope over det endelige resultat, såfremt at x = det endelige resultat")
    print("og i = 0 + 1. ")

    print("Da vi her bruger et forloop, og beder Python om at loope i range(x) (hvor")
    print("x er den samlede værdi) vil den samlede værdi ikke være med i vores loop som")
    print("en integer.")

    x = samlet_resultat

    for i in range(x):
        print(i)

elif elements.index(svar) == 5:
    print("Okay, så i min kode står der egentlig bare, at du har valgt den 6. mulighed,")
    print(" - mere specifikt, den 5. elif mulighed, da if/elif/else lægger ud med 'if.")
    print("Så snart du valgte det 5. element i arrayet, blev mit 'if', mit 'else', og")
    print("de resterende 'elif'er sprunget over.")

   # print("array")

else: # if/elif/else herunder?
    print("Siden du ikke har valgt et element fra arrayet, ved jeg altså ikke")
    print("hvordan jeg kommer videre herfra... Så hardcore er jeg ikke :'(")

    print("(Siden du er flabet, er du endt i 'else' (...))")

    # if
    # elif
    # else:
