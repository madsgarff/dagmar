# -*- coding: utf-8 -*-
import random
# Skriv en funktion som...

# Looper over en given string og printer hvert bogstav.

def printFjolleri():

    x = "Fjolleri"

    for i in x:
        print(i)

printFjolleri()

# Looper over en given string og rekonstruerer den 
# i en mellemregning - print til sidst.

def printStringRec():

    y = ""

    x = "StringRec"

    for i in x:

        y = y + i

    print(y)

printStringRec()

# Looper over en given string og putter alle elementer ind i 
# et array

def printElements():

    array = []
    x = "string"

    for i in x:

        array.append(i)
    
    return array

print(printElements())

# Looper over det samme array og sætter stringen sammen igen - 
# print til sidst.

def plusElements(array):
    y = ""
    
    for i in array:
        y = y + i

    return y 

arr = printElements()

print(arr)

print(plusElements(arr))

# Bruger den indbyggede metode range() til at printe tallene
# 1 - 100.

def printNumbers():

    for i in range(20, 30):
        print(i)

printNumbers()

# Bruger den indbyggede metode pow() til at beregne 10*10

def power(x):

    result = x * x

    return result

calculation = 10
print(power(calculation))

# Bruger den indbyggede metode sum() til at summere et array.

array = [10, 8, 34, 29, 12]

sum(array)

print(sum(array))

# Bruger den indbyggede metode random.randrange() til at generere et array, 
# hvor der er 100 random entries.

def printArray():

    x = []

    for _ in range(100):
        
        z = random.randrange(100)
        x.append(z)
        
    return x

print(printArray())

# Bruger den indbyggede metode sorted() til at sortere det før genererede array.

print(sorted(printArray()))