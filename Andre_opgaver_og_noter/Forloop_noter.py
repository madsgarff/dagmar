# -*- coding: utf-8 -*-

# Vi starter ud med at tage udgangspunkt i arrays. Lad os sige at vi har et array som
# lyder

x = ["Dagmar", 65, 2.5]
print(x)

# Lad os nu sige, at vi vil have hvert element print individuelt. Vi skal her hive fat
# forloopet og bruge variablen "i". "i" vil nu referere til hvert enkelt element i arrayet. 

for i in x:
    print(i)

# Printes ovenstående til terminalen kan vi se, at de tre elemnter nu er "delt op". Vi kan arbejde
# på præcis samme måde, i tilfælde af, at vores "x" ikke er et array men en string;

name = "Dagmar"

for i in name:
    print(i)

# Vi kan, uden problemer, samle de to "led". På den måde definerer vi ikke arrayet/stringen/el.lign. på forhånd. 

for i in [2, 2.9, 3 + 5]:
    print(i)

# Læg mærke til, at det tredje element i terminalen IKKE lyder "3 + 5", men i stedet "8".
# Lad os hive fat i begrebet "range". 

for i in range(10):
    print(i)

# Her bliver der printet fra 0 til 9 - altså 10 gange. Lad os sige, at vi i stedet vil printe fra
# 11 til 20. Nedenfor står der "21" i range, da "20" ellers ikke ville blive inkluderet. Det sidste tal - "1" - i
# ranget indikerer, at der er "1" "mellemrum" for hvert element. Anden gang vi printer, er det sidste tal "2".
# Altså vil printet nu lyde "11, 13, 15 (...)"

for i in range(11, 21, 1):
    print(i)

for i in range(11, 21, 2):
    print(i)

# Vi kan sagtens bevæge os den anden vej, men det kræver, at det sidste tal i vores range er negativt. 
# (Vi huske at skrive 10 i ranget, da vi på den måde inkluderer 11). F.eks;

for i in range(21, 10, -2):
    print(i)

# Lad os sige, at vi vil printe fra 1 til 20, men hver gang vi når et element som 5 går op i, så
# skal det element elimineres. Yikes, den er tricky - check it out

for i in range(1, 21):
    if i % 5 != 0 :
        print(i)

# Her fortæller vi python "print only when it's not equal". I det her tilfælde, "not equal 5".