"""
    Denne gang omhandler lidt om at læse fra filer.

    Og herefter påbegynder vi en lidt større opgave, fordi du skal 
    altså i gang før eller siden, og det er nu. 
"""

"""
    Her er en fremragende guide om filer, paths, læse fra filer, etc. 
    
    https://realpython.com/read-write-files-python/

    Jeg synes, at du skal gennemgå den hele. Og eventuelt skrive
    spørgsmål ned/selv afklare det vha. google søgninger. Hvis du 
    bliver træt af den, så spring til afsnittet:
    
    "Reading and Writing Opened Files"

    Den er rimelig heavy, så jeg lader den gælde for nogle opgaver.
"""

## Ex.1
"""
    Lav en funktion, som læser fra en fil (lav en fil med noget tekst), og 
    gør det både med 'read()', 'readline()' og 'readlines().

    Print resultaterne. Se forskellen. Forklar forskellen. Forstå forskellen.
"""

## Ex.2
"""
    Lav en funktion, som læser fra en fil, hele filen, og tæller hvert 
    eneste bogstav. Gem resultatet i et 'dict'. Pretty print når alt er 
    talt op.
"""
def countLettersWholeFile(self, filename):
    # Åben filen (brug read)

    # Tæl bogstaver i for loop (double loop)

    # Gem de enkelte bogstaver i dict, dvs bogstaver[x] += 1 

    # Print smukt

## Ex.3
"""
    Lav en funktion, som læser fra en fil, linje for linje, og tæller hvert
    eneste bogtav. Gem resultatet i et 'dict'. Pretty print når alt er talt 
    op.
"""
def countLettersStreamLines(self, filename):
    # Åben filen (brug readline)

    # Tæl bogstaver

    # Gem dem i dict 

    # Pretty print 

## Ex.4
"""
    Lav en funktion, som læser fra en fil, linje for linje - meeen.. linjerne
    skal gemme i en liste. Tæl herefter hver eneste bogstav. Gem resultatet 
    i et 'dict'. Pretty print når alt er talt op. 
"""
def countLettersAllLines(self, filename):
    # Åben filen med readlines og smid dem i et array

    # Loop over array og tæl bogstaver 

    # Gem dem i dict 

    # Pretty print 


"""
    OG SÅ TIL DEN STOOOORE OPGAVE !!! 

    Vi griber det anderledes and end sidst, fordi det var nok liiidt for 
    stort et spring. Så denne gang tænker jeg, at vi 'designer mere sammen', 
    hvorefter du selvfølgelig skal kode det. 

    Jeg præsenterer dig derfor for opgaven nu. Det jeg gerne vil have,
    at du gør til næste gang, er faktisk "bare", at du sætter dig ind 
    i opgaven. Du danner dig overblik, skriver lidt noter, tænker tanker,
    overvejer hvilke datastrukturer, der kan bruges, finder navne på 
    funktioner, tænker på variabler, osv. 

    Kort sagt - du forsøger analytisk at danne et overblik og ud fra det, 
    så forsøger du at skabe en form for ramme for arbejdet med problemet. 

    Tanken er så, at når vi mødes, så går vi planen i gennem sammen,
    tænker højt sammen, deler ideer og forsøger at gøre planen 
    rigtig skarp. Herefter laver vi en liste over funktioner, etc., 
    som skal indgå, du vælger et par stykker, og det er lektierne 
    til næste gang. 

    Jeg håber, at det giver mening - hvis ikke, så skal du skrive og så
    tager vi den på Discord.
"""

"""
    Og du skal jo også have lidt medbestemmelse, så jeg har fundet 3 
    projekter, som jeg synes giver god mening. Du vælger bare det 
    projekt, som du synes lyder bedst for dig. Du behøver ikke 
    melde tilbage, hvilket projekt du vælger. Du skal bare forberede
    dig på det ene.
"""

"""
    Projekt 1:

    'Number guessing'

    Projektet går ud på at gætte et tal. Brugeren bliver altså præsenteret
    for et spørgsmål/en range af tal - fx 'Guess a number between 1 - 100'.

    Herefter forsøger brugeren at gætte. Chancen for at gætte forkert 
    er jo self. rimelig stor. Gættes der forkert bliver der givet et hint.
    Det kan fx være 'the number is < 50'. Eller noget i den dur.

    Brugeren får fx 5 hints og herefter er spillet slut, hvis ikke tallet 
    er gættet.

    Ting at tænke over i det her projekt:

    1. Hvordan skal hovedstruktueren være, dvs. skal der fx være et loop,
       der kører igen og igen, når der gættes forkert? Hvilket loop? 
       Hvordan skal det tage sig ud? Hvad ville en god guard være? Og
       hvordan/hvornår skulle sådan et loop slutte?

    2. Når den generelle struktur er på plads, hvilke funktioner er der 
       så brug for? En til brugerens svar? En til tallene? Etc.

    3. Hvordan ville man udforme en funktion, der kunne præsentere hints?
       De samme hints hvergang? Tilfældigt udvalgt? Etc. 
"""

"""
    Projekt 2:

    'Text-based Adventure Game'

    Projektet går ud på at skabe et lille spil, som spilles i terminalen.
    Det kunne fx starte ved:

    "You wake up in a dark room and smell something funny.. what do you do?"

    1. Try and find a wall.
    2. Use your nose and try and find the source of the smell.
    3. Etc etc. 

    Og så kan man ellers bygge videre herfra. 

    1. Igen - en form for hovedstruktur og nok meget ala den anden, fordi
       man jo vælger igen og igen.

    2. Hvad skal spillet handle om.

    3. Skal der være monstre? Skal man kunne få lvls? Skal man få våben?
       Skal det foregå i "South Park world" agtigt?

    Der er mange muligheder. Det er et udemærket projekt, men også rimelig 
    simpelt. Ikke at det gør noget.
"""

"""
    Projekt 3:

    'Tic-Tac-Toe'

    Det giver lidt sig selv. Det er en tekstbaseret version af spillet.

    _ _ _
    _ _ _
    _ _ _ 

    _ x _ 
    _ _ _
    _ _ _

    _ x _ 
    o _ _
    _ _ _ 

    OG så videre.. 

    Der er altså to spillere og hver spiller vælger, hvor de gerne vil 
    placere deres brik næste gang. Så indledende printes der for hver 
    runde og herefter bliver der så placeret en brik, yada yada..

    1. Igen - den generelle struktur. Et form for loop til at køre showet.

    2. Hvilke datastruktur kunne være god til at holde spillet?

    3. Hvordan de forskellige felter tilgås af spillerne? Er det koordinater?
"""

"""
    GOOD LUCK! =)
"""